package com.project.centroid.deliveryapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.OrderDetails;
import com.project.centroid.deliveryapp.data.remote.model.OrderItem;
import com.project.centroid.deliveryapp.view.adapter.OrderDetailsAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends AppCompatActivity {

    String orderid="";

    TextView name;

    ImageView imgback;

    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        getSupportActionBar().hide();
        name=findViewById(R.id.name);
        imgback=findViewById(R.id.imgback);
        recyclerView=findViewById(R.id.recyclerView);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        orderid=getIntent().getStringExtra("orderid");
        getOrderDetails();

    }




    public void getOrderDetails()
    {

     SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        RestService.RestApiInterface client = RestService.getClient();

        Call<List<OrderDetails>> callBack = client.getOrderDetails(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),orderid);

        callBack.enqueue(new Callback<List<OrderDetails>>() {
            @Override
            public void onResponse(Call<List<OrderDetails>> call, Response<List<OrderDetails>> response) {

                List<OrderDetails>orderDetails=response.body();

                if(orderDetails.size()>0)
                {

                    OrderDetails orderDetails1=orderDetails.get(0);






                    StringBuilder sb=new StringBuilder();
                    sb.append("Invoice : "+orderDetails1.getSalesInvoice()+"\n");
                    sb.append("Order date : "+orderDetails1.getOrderDate()+"\n");
                    sb.append("Delivery date : "+orderDetails1.getDeliveryDate()+"\n");
                    sb.append("Address : "+orderDetails1.getDeliveryAddress()+"\n");
                    sb.append("Order status : "+orderDetails1.getOrderStatus());

                    name.setText(sb.toString());

                    orderDetails.remove(0);

                    recyclerView.setLayoutManager(new LinearLayoutManager(OrderDetailsActivity.this));
                    recyclerView.setAdapter(new OrderDetailsAdapter(OrderDetailsActivity.this,orderDetails,orderDetails1.getOrderStatus()));




                }



            }

            @Override
            public void onFailure(Call<List<OrderDetails>> call, Throwable t) {

            }
        });

    }
}
