package com.project.centroid.deliveryapp.data.remote;

import android.support.annotation.NonNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.project.centroid.deliveryapp.data.remote.model.CartItems;
import com.project.centroid.deliveryapp.data.remote.model.CartProduct;
import com.project.centroid.deliveryapp.data.remote.model.Category;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.data.remote.model.NewsItem;
import com.project.centroid.deliveryapp.data.remote.model.OrderDetails;
import com.project.centroid.deliveryapp.data.remote.model.OrderItem;
import com.project.centroid.deliveryapp.data.remote.model.Pincode;
import com.project.centroid.deliveryapp.data.remote.model.SearchSuggestions;
import com.project.centroid.deliveryapp.data.remote.model.SubCategory;
import com.project.centroid.deliveryapp.data.remote.model.UserItem;
import com.project.centroid.deliveryapp.data.remote.modelnew.MainCategory;
import com.project.centroid.deliveryapp.data.remote.modelnew.Offer;
import com.project.centroid.deliveryapp.data.remote.modelnew.OfferItems;

import org.json.JSONArray;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


public class
RestService {
    private static RestApiInterface RestApiInterface;


    public static RestApiInterface getClient() {

        if (RestApiInterface == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.interceptors().add(logging);

            OkHttpClient httpClient = builder.build();

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(ApiConstants.BASEURL)
                    .addConverterFactory(new NullOnEmptyConverterFactory())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();

            RestApiInterface = client.create(RestApiInterface.class);
        }
        return RestApiInterface;
    }


   /* public static RestApiInterface getClient(String url) {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(logging);
        OkHttpClient httpClient = builder.build();

        Retrofit client = new Retrofit.Builder()
                .baseUrl(url).client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestApiInterface = client.create(RestApiInterface.class);

        return RestApiInterface;
    }
*/

    public interface RestApiInterface {

        @Headers({"Accept: application/json"})
        @FormUrlEncoded

        @POST("UserLogin.php")
        Call<ResponseBody> loginRequest(@Query("apicall") String callType, @Field("phone") String phone, @Field("password") String password);


        @GET("getOfferinfo.php")
        Call<OfferItems>getOfferlist(@Header("Authorization") String x_auth_token);



        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("UserLogin.php")
        Call<ResponseBody> regUser(@Query("apicall") String callType, @Field("username") String name, @Field("phone") String phone, @Field("email") String email, @Field("password") String password, @Field("gender") String gender,@Field("pincode")String pincode);

        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("add_to_cart.php")
        Call<ResponseBody> addToCart(@Header("Authorization") String x_auth_token, @Field("cd_item") String cdItemcode, @Field("quantity") String quantity);


        @NonNull
        @Headers({"Accept: application/json"})
        @GET("master/featuredprofiles/{type}/{page}")
        Call<List<ResponseBody>> getFeaturedUsers(@Header("Authorization") String x_auth_token, @Path("type") String type, @Path("page") int page);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("get_item_subcategories.php")
        Call<List<Item>> getAllItems(@Header("Authorization") String x_auth_token, @Query("api_key") String call);
        @NonNull
        @Headers({"Accept: application/json"})
        @GET("get_single_item.php")
        Call<List<Item>> getSingleItem(@Header("Authorization") String x_auth_token, @Query("cd_item") String call);

        @Headers({"Accept: application/json"})
        @GET("getAll.php")
        Call<List<Item>> getAll(@Header("Authorization") String x_auth_token, @Query("currentpage") String currentPage);

        @Headers({"Accept: application/json"})
        @GET("get_featured_item.php")
        Call<List<Item>> getFeaturedItems(@Header("Authorization") String x_auth_token, @Query("apicall") String call);

        @Headers({"Accept: application/json"})
        @GET("search_products.php")
        Call<List<Item>> searchProduct(@Header("Authorization") String x_auth_token, @Query("search") String key, @Query("currentpage") String currentPage);


        @Headers({"Accept: application/json"})
        @GET("get_catwise_subcategories.php")
        Call<List<SubCategory>> getSubcategoryList(@Header("Authorization") String x_auth_token, @Query("cd_category") String categoryId);


        @Headers({"Accept: application/json"})
        @GET("get_featured_item.php")
        Call<List<Item>> getCategorySubcategoryList(@Header("Authorization") String x_auth_token, @Query("cd_category") String categoryId, @Query("cd_category") String subCategoryId, @Query("currentpage") String currentPage);


        @Headers({"Accept: application/json"})
        @GET("filter_item.php")
        Call<List<Item>> getFilterCategorySubcategoryList(@Header("Authorization") String x_auth_token, @Query("cd_category") String categoryId, @Query("cd_subcategory") String subCategoryId, @Query("currentpage") String currentPage);

        @Headers({"Accept: application/json"})
        @GET("get_categories.php")
        Call<List<Category>> getAllCategories(@Header("Authorization") String x_auth_token);

        @Headers({"Accept: application/json"})
        @GET("checkDeliveryPincode.php")
        Call<ResponseBody> checkPincode(@Header("Authorization") String x_auth_token, @Query("pincode") String pincode);




        @Headers({"Accept: application/json"})
        @POST("new_sales_order.php")
        @FormUrlEncoded
        Call<ResponseBody> placeOrder(@Header("Authorization") String x_auth_token, @Field("ds_pincode") String pincode, @Field("ds_delivery_address") String address);

        @POST("add_to_wishList.php")
        @FormUrlEncoded
        Call<ResponseBody> addWishList(@Header("Authorization") String x_auth_token, @Field("cd_item") String itemCode, @Field("quantity") String quantity);

        @GET("WishListRemove.php")
        Call<ResponseBody> removeWishList(@Header("Authorization") String x_auth_token, @Query("cd_item") String itemCode);

        @Headers({"Accept: application/json"})
        @POST("cartP.php")
        Call<ResponseBody> addCart(@Header("Authorization") String x_auth_token, @Body CartItems cartItems);

        @Headers({"Accept: application/json"})
        @GET("cancelOrder.php")
        Call<ResponseBody> cancelOrder(@Header("Authorization") String x_auth_token, @Query("cd_sales_order") String id);

        @Headers({"Accept: application/json"})
        @GET("ordersView.php")
        Call<List<OrderItem>> getAllOrder(@Header("Authorization") String x_auth_token, @Query("currentpage")int currentPage);

        @Headers({"Accept: application/json"})
        @GET("WishListCartView.php")
        Call<List<Item>> getWishList(@Header("Authorization") String x_auth_token, @Query("apicall") String callType,@Query("currentpage")int currentpage);

        @Headers({"Accept: application/json"})
        @GET("userProfile.php")
        Call<List<UserItem>> getUser(@Header("Authorization") String x_auth_token);

        @Headers({"Accept: application/json"})
        @FormUrlEncoded
        @POST("editUserProfile.php")
        Call<ResponseBody> updateUser(@Header("Authorization") String x_auth_token, @Field("name") String name, @Field("phone") String phone, @Field("email") String email, @Field("home_address") String home, @Field("delivery_address") String deliveryAddress, @Field("pincode") String pincode);

        @Headers({"Accept: application/json"})
        @GET("newsView.php")
        Call<List<NewsItem>> getNews(@Header("Authorization") String x_auth_token,@Query("currentpage") int currentpage);

        @GET("wishlist_existance.php")
        Call<JsonArray>getWishListExistence(@Header("Authorization") String x_auth_token, @Query("cd_item")String cd_item);


        @Multipart
        @POST("UsersEditImage.php")
        Call<JsonArray> uploadImage(@Header("Authorization") String x_auth_token,@Part MultipartBody.Part file);



        @GET("UsersGetProfileImage.php")
        Call<JsonArray>getImage(@Header("Authorization") String x_auth_token);




        @FormUrlEncoded
        @POST("cartP.php")
        Call<JsonArray> addProductCart(@Header("Authorization") String x_auth_token,@Field("cd_item")String cd_item,@Field("quantity")String quantity,@Field("date")String date);


        @FormUrlEncoded
        @POST("cartP.php")
        Call<JsonArray> updateProductCart(@Header("Authorization") String x_auth_token,@Field("cd_item")String cd_item,@Field("quantity")String quantity,@Field("date")String date);


        @Headers({"Accept: application/json"})
        @GET("CartCount.php")
        Call<JsonObject> getCartCount(@Header("Authorization") String x_auth_token);



        @Headers({"Accept: application/json"})
        @GET("WishListCartView.php?apicall=cart")
        Call<List<CartProduct>> getCartProducts(@Header("Authorization") String x_auth_token,@Query("currentpage")int currentpage);


        @FormUrlEncoded
        @POST("cartRemove.php")
        Call<JsonObject> deleteFromCart(@Header("Authorization") String x_auth_token,@Field("cd_item") String cd_item);

        @GET("get_suggessions_items.php")
        Call<List<SearchSuggestions>> getSearchSuggestios(@Header("Authorization") String x_auth_token, @Query("item") String cd_item);

        @GET("orderDetails.php")
        Call<List<OrderDetails>>getOrderDetails(@Header("Authorization") String x_auth_token, @Query("cd_sales_order")String cd_sales_order);

        @FormUrlEncoded
        @POST("cancelOrderItem.php")
        Call<JsonObject>cancelOrderItem(@Header("Authorization") String x_auth_token,@Field("cd_item")String cd_item);


        @GET("get_avilable_pincode.php")
        Call<List<Pincode>>getPincodeSuggestions(@Header("Authorization") String x_auth_token, @Query("pincode")String pincode);

        @GET("CartTotalView.php")
        Call<JsonArray>getTotalPriceFromcart(@Header("Authorization") String x_auth_token, @Query("currentpage")int currentpage);


        @GET("get_maincategories.php")
        Call<List<MainCategory>>get_maincategories(@Header("Authorization") String x_auth_token);






         /*
        @NonNull
        @Headers({"Accept: application/json"})
        @POST("search/basic/{page}")
        Call<List<FeaturedUser>> getAllSearched(@Body ProfileSearchRequest profileSearchRequest, @Path("page") int page);

        @NonNull
        @Headers({"Accept: application/json"})
        @POST("search/code/{scmid}/{userId}")
        Call<FeaturedUser> getAllSearchedById(@Path("scmid") String scmId, @Path("userId") String userId);

        @NonNull
        @POST("user/resendEmailVerify/{userId}")
        Call<ResponseBody> resendEmailCode(@Path("userId") String userId);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("profile/{uid}")
        Call<UserProfile> getUserProfile(@Header("X-Auth-Token") String x_auth_token, @Path("uid") Integer uid);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("profile/get")
        Call<UserProfile> getUserProfile(@Header("X-Auth-Token") String x_auth_token);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("profile/bookmarksall/{userId}/{page}")
        Call<List<FeaturedUser>> getAllBookmarked(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("page") int page);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("search/matchingprofile/{userId}/{page}")
        Call<List<FeaturedUser>> getAllMatchingProfiles(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String userId, @Path("page") int page);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("search/matchingprofileauto/{userId}/{page}")
        Call<List<FeaturedUser>> getAllMatchingProfilesAuto(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String userId, @Path("page") int page);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("profile/bookmarks/{userId}/{page}")
        Call<BookmarkUserId> getAllBookmarkedUserId(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("page") int page);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("master/countries")
        Call<List<Country>> getAllCountryCode();

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("master/countries")
        Call<List<ParishCountry>> getAllParishCountry();


        @NonNull
        @Headers({"Accept: application/json"})
        @GET("master/communities")
        Call<List<CommunityQuickSignUp>> getAllCommunity();

        @NonNull
        @Headers({"Accept: application/json", "Content-Type: application/json"})
        @POST("user/signup")
        Call<UserResponse> userQuickSignUp(@Header("Authorization") String auth_token, @Body QuickSignUpBody signUp);


        @NonNull
        @GET("profile/getLatestPayment/{userId}")
        Call<PaymentSummaryDTO> getLatestTransaction(@Header("X-Auth-Token") String auth_token, @Path("userId") String userId);


        @NonNull
        @GET("paypal/client_token")
        Call<UserResponse> getBrianTreeToken();

        @NonNull
        @GET("paypal/checkout/{nonceFromTheClient}/{amount}/{orderId}/{userId}")
        Call<ResponseBody> checkOutProcess(@Path("nonceFromTheClient") String nonce, @Path("amount") String amount, @Path("orderId") String orderId, @Path("userId") String userId);


        @NonNull
        @Headers({"Accept: application/json"})
        @GET("profile/image/list/{userId}")
        Call<List<ProfileImageItem>> getAllProfileImages(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String userId);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("profile/image/profilepic/{userId}/{imageId}")
        Call<UserResponse> setProfileImages(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String userId, @Path("imageId") int imageId);


        @NonNull
        @GET("master/dioces/{userId}")
        Call<List<Community>> getAllDiocese(@Path("userId") String userId);

        @NonNull
        @GET("master/parish/{country}/{userId}")
        Call<List<Diocese>> getAllParish(@Path("country") String country, @Path("userId") String userId);


        @NonNull
        @GET("user/checkemail/{userId}/")
        Call<ResponseValid> checkEmailId(@Path("userId") String userId);

        @NonNull
        @GET("user/checkmobile/{country}/{userId}/")
        Call<ResponseValid> checkMobile(@Path("country") String countryId, @Path("userId") String userId);


        @NonNull
        @GET("master/occupation/")
        Call<List<Profession>> getAllOccupation();

        @NonNull
        @GET("master/education/")
        Call<List<Education>> getAllEducation();

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("profile/primary/{userId}")
        Call<PrimaryProfileResponse> getPrimaryProfile(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("profile/family/{userId}")
        Call<FamilyInfoResponse> getFamilyInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("profile/church/{userId}")
        Call<ParishInfoResponse> getParishInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("profile/professional/{userId}")
        Call<ProfessionalInfoResponse> getProfessionalInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);


        @NonNull
        @Headers({"Accept: application/json"})
        @GET("profile/showUserIdDoc/{userId}")
        Call<IdProofResponse> getIdProofResponse(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("profile/partner/{userId}")
        Call<PartnerInfoResponse> getPartnerInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);

        @NonNull
        @GET("master/packages")
        Call<List<PaymentPlan>> getPaymentPlans();

        @NonNull
        @POST("profile/upgrade/{userId}")
        Call<PaymentRequestResponse> getPaymentUserResponse(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Body RequestPaymentBody paymentBody);

        @NonNull
        @FormUrlEncoded
        @POST("resources/partials/payment/GetRSA.jsp")
        Call<ResponseBody> getRSAResponse(@FieldMap Map<String, String> params);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("profile/get/{userId}")
        Call<UserBasicData> getUserBasicInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("profile/activities/{userId}")
        Call<UserActivitiesData> getAllActivities(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type);


        @NonNull
        @Headers({"Accept: application/json"})
        @POST("profile/primary/{userId}")
        Call<UserResponse> updatePrimaryProfile(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Body PrimaryProfileData primaryProfileData);

        @NonNull
        @Headers({"Accept: application/json"})
        @GET("user/validatemail/{email}/{token}")
        Call<UserResponse> validateEmail(@Path("email") String emailId, @Path("token") String token);

        @NonNull
        @Headers({"Accept: application/json"})
        @POST("profile/family/{userId}")
        Call<UserResponse> updateFamilyInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Body FamilyInfoData familyInfoData);

        @NonNull
        @Headers({"Accept: application/json"})
        @POST("profile/church/{userId}")
        Call<UserResponse> updateParishInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Body ChurchInfoData churchInfoData);

        @NonNull
        @Headers({"Accept: application/json"})
        @POST("profile/professional/{userId}")
        Call<UserResponse> updateProfessionalInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Body ProfessionalInfoData churchInfoData);

        @NonNull
        @Headers({"Accept: application/json"})
        @POST("profile/partner/{userId}")
        Call<UserResponse> updatePartnerInfo(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Body PartnerInfoData partnerInfoData);

        @NonNull
        @Headers({"Accept: application/json"})
        @POST("profile/bookmark/{userId}/{currentUserId}")
        Call<UserResponse> bookMarkUser(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("currentUserId") String currentUserId);

        @NonNull
        @Headers({"Accept: application/json"})
        @POST("profile/activity/expressinterest")
        Call<UserResponse> expressInterest(@Header("X-Auth-Token") String x_auth_token, @Body ExpressInterestRequestBody requestBody);

        @NonNull
        @Headers({"Accept: application/json"})
        @POST("profile/interest/{userId}/read/{currentUserId}")
        Call<UserResponse> readInterest(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("currentUserId") String currentUserId);

        @NonNull
        @Headers({"Accept: application/json"})
        @POST("profile/interest/{userId}/accept/{currentUserId}")
        Call<UserResponse> acceptInterest(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("currentUserId") String currentUserId);

        @NonNull
        @Headers({"Accept: application/json"})
        @POST("profile/interest/{userId}/reject/{currentUserId}")
        Call<UserResponse> rejectInterest(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("currentUserId") String currentUserId);

        @NonNull
        @Headers({"Accept: application/json"})
        @POST("profile/forward/{userId}")
        Call<UserResponse> forwardProfile(@Header("X-Auth-Token") String x_auth_token, @Body ForwardProfileRequestBody requestBody, @Path("userId") String userId);

        @NonNull
        @Headers({"Accept: application/json"})
        @POST("user/forgotpassword")
        Call<UserResponse> resetPassword(@Body UserRequest userRequest);


        @NonNull
        @POST("profile/removebookmark/{userId}/{currentUserId}")
        Call<UserResponse> unBookMark(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("currentUserId") String currentUserId);

        @NonNull
        @POST("profile/image/delete/{imageId}/{userId}")
        Call<UserResponse> deleteProfilePicture(@Header("X-Auth-Token") String x_auth_token, @Path("imageId") String imageId, @Path("userId") String currentUserId);



        @NonNull
        @GET("view/contacts/{userId}/{currentUserId}")
        Call<UserResponse> getUserContacts(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String type, @Path("currentUserId") String currentUserId);

        @NonNull
        @GET("user/sms/{userId}")
        Call<ResponseBody> getSMS(@Path("userId") String userId);

        @NonNull
        @GET("user/email/{userId}")
        Call<ResponseBody> getEmail(@Path("userId") String userId);


        @NonNull
        @GET("profile/activities/interest/sent/{userId}/{page}")
        Call<List<InterestResponse>> getInterestSent(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String user, @Path("page") int page);

        @NonNull
        @GET("profile/activities/interest/recieved/{userId}/{page}")
        Call<List<InterestResponse>> getInterestRecieved(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String user, @Path("page") int page);

        @NonNull
        @GET("profile/activities/{type}/{userId}/{page}")
        Call<List<InterestResponse>> getAllRequested(@Header("X-Auth-Token") String x_auth_token, @Path("type") String type, @Path("userId") String user, @Path("page") int page);


        @NonNull
        @GET("profile/activities/{type}/{userId}/{page}")
        Call<List<VisitedResponse>> getAllProfileVisitResponse(@Header("X-Auth-Token") String x_auth_token, @Path("type") String type, @Path("userId") String user, @Path("page") int page);

        @NonNull
        @GET("profile/accountsummary/{userId}")
        Call<AccountSummary> getAccountSummary(@Header("X-Auth-Token") String x_auth_token, @Path("userId") String user);

        @NonNull
        @GET("profile/activities/{type}/{userId}/{page}")
        Call<List<ContactsResponse>> getAllContactsResponse(@Header("X-Auth-Token") String x_auth_token, @Path("type") String type, @Path("userId") String user, @Path("page") int page);

        @NonNull
        @GET("view/profile/{profilecode}/{userId}")
        Call<ResponseBody> sendUserViews(@Header("X-Auth-Token") String x_auth_token, @Path("profilecode") String profileCode, @Path("userId") String userId);


        @NonNull
        @Multipart
        @POST("profile/image/upload/{userId}/false/true/1")
        Call<UserResponse> uploadProfileImage(@Header("X-Auth-Token") String authtoken, @Path("userId") String userId, @Part MultipartBody.Part imageFile);

        @NonNull
        @Multipart
        @POST("profile/image/upload/{userId}/true/false/1")
        Call<UserResponse> uploadCoverImage(@Header("X-Auth-Token") String authtoken, @Path("userId") String userId, @Part MultipartBody.Part imageFile);

        @NonNull
        @Multipart
        @POST("profile/uploadiddoc/{userId}")
        Call<UserResponse> uploadIdDocument(@Header("X-Auth-Token") String authtoken, @Path("userId") String userId, @Part MultipartBody.Part imageFile, @Part("idtype") RequestBody type);
*/

    }

}
