package com.project.centroid.deliveryapp.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchSuggestions {

    @SerializedName("cd_item")
    @Expose
    private String cdItem;
    @SerializedName("ds_item")
    @Expose
    private String dsItem;

    public SearchSuggestions() {
    }

    public String getCdItem() {
        return cdItem;
    }

    public void setCdItem(String cdItem) {
        this.cdItem = cdItem;
    }

    public String getDsItem() {
        return dsItem;
    }

    public void setDsItem(String dsItem) {
        this.dsItem = dsItem;
    }
}
