package com.project.centroid.deliveryapp.data.remote.modelnew;

import com.project.centroid.deliveryapp.data.remote.model.Item;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductWithCategory implements Serializable {

    String categorydata;
    int size;
    String catid;

    String maincatid;
    String maincategory;

    List<Item>items=new ArrayList<>();

    public ProductWithCategory() {
    }


    public String getMaincatid() {
        return maincatid;
    }

    public void setMaincatid(String maincatid) {
        this.maincatid = maincatid;
    }

    public String getMaincategory() {
        return maincategory;
    }

    public void setMaincategory(String maincategory) {
        this.maincategory = maincategory;
    }

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getCategorydata() {
        return categorydata;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public void setCategorydata(String categorydata) {
        this.categorydata = categorydata;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
