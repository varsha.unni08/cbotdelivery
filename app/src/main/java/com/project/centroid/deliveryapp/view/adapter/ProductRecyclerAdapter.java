package com.project.centroid.deliveryapp.view.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.view.interfaces.cartListner;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Sooraj Soman on 11/1/2018
 */
public class ProductRecyclerAdapter extends RecyclerView.Adapter<ProductRecyclerAdapter.ItemViewHolder> {
    private final Context context;
    private List<Item> items;
    private cartListner listner;

    public ProductRecyclerAdapter(Context context, List<Item> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_item_products, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        try {
            Item item = items.get(position);
            holder.set(item);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void setListner(cartListner listner) {
        this.listner = listner;
    }
    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemPic)
        ImageView itemPic;
        @BindView(R.id.itemName)
        TextView itemName;
        @BindView(R.id.itemDesc)
        TextView itemDesc;
        @BindView(R.id.itemPrice)
        TextView itemPrice;
        @BindView(R.id.addbtn)
        LinearLayout addbtn;
        @BindView(R.id.bookmark)
        ImageView bookmark;
        @BindView(R.id.textnewprice)
        TextView textnewprice;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }



        public void set(Item item) {
            if (ApiConstants.isNonEmpty(item.getDsItem())) {
                itemName.setText(item.getDsItem());
            }
            if (ApiConstants.isNonEmpty(item.getDsItemDescription())) {
                itemDesc.setText(item.getDsItemDescription());
            }
            if (ApiConstants.isNonEmpty(item.getVlMrp())) {
                itemPrice.setText(ApiConstants.amountFormatter(item.getVlMrp()));
            }
            if(ApiConstants.isNonEmpty(item.getVl_selling_price())){
                if (ApiConstants.isNonEmpty(item.getVlMrp())) {
                    if(!item.getVlMrp().equalsIgnoreCase(item.getVl_selling_price())){

                        itemPrice.setPaintFlags(itemPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        textnewprice.setText(ApiConstants.amountFormatter(item.getVl_selling_price()));
                    }}
            }
            if (ApiConstants.isNonEmpty(item.getDsItemPicSmall())) {
                Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(item.getDsItemPic()).into(itemPic);
            }
            final Item item1=item;
            addbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listner.OnaddCart(item1.getDsItemCode());
                }
            });
            //UI setting code
        }
    }





}