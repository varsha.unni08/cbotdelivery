package com.project.centroid.deliveryapp.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.MatrixCursor;
import android.os.Build;
import android.os.Bundle;

import android.provider.BaseColumns;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.Constants;
import com.project.centroid.deliveryapp.ViewItemSingle;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.SearchSuggestions;
import com.project.centroid.deliveryapp.view.adapter.LatestItemRecyclerAdapter;
import com.project.centroid.deliveryapp.view.adapter.OfferedItemRecyclerAdapter;
import com.project.centroid.deliveryapp.view.fragments.AccountFragment;
import com.project.centroid.deliveryapp.view.fragments.HomeFragment;
import com.project.centroid.deliveryapp.view.fragments.OrderFragment;
import com.project.centroid.deliveryapp.view.fragments.WishListFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.project.centroid.deliveryapp.Utils.Constants.mCartItemCount;

public class HomeActivity extends AppCompatActivity implements OfferedItemRecyclerAdapter.CallbackInterface, LatestItemRecyclerAdapter.CallbackInterfaceResult, ViewItemSingle.UpdateListner {

    private TextView mTextMessage;
    private TextView textCartItemCount;
    private FragmentManager mFragmentManager;

    List<SearchSuggestions>searchSuggestions=new ArrayList<>();

    CursorAdapter mAdapter;
    Fragment mFragment;

    private FragmentTransaction mFragmentTransaction;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected( MenuItem item) {
            switch (item.getItemId()) {
                case R.id.home:
                    mFragment = HomeFragment.newInstance();
                    break;
                case R.id.wishlist:
                    mFragment = WishListFragment.newInstance();
                    break;
                case R.id.orders:
                    mFragment = OrderFragment.newInstance();
                    break;
                case R.id.accounts:
                    mFragment = AccountFragment.newInstance();
                    break;
            }
            return loadFragment(mFragment);
        }
    };
     private Menu menuV;
    private MenuItem menuItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        loadFragment(new HomeFragment());
        invalidateOptionsMenu();
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);




    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.message, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menuV=menu;

        getMenuInflater().inflate(R.menu.cart_menu, menu);

        menuItem = menu.findItem(R.id.action_cart);

        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        //setupBadge();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        setupBadge();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_search:
                final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(item);

                final String[] from = new String[] {"cityName"};
                final int[] to = new int[] {R.id.txt};
                mAdapter = new SimpleCursorAdapter(HomeActivity.this,
                        R.layout.layout_listitem,
                        null,
                        from,
                        to,
                        CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
                searchViewAndroidActionBar.setSuggestionsAdapter(mAdapter);


                searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        searchViewAndroidActionBar.clearFocus();
//                        Constants.type = "search";
//                        Intent intent = new Intent(getApplicationContext(), ProductActivity.class);
//                        intent.putExtra("key", query);
//                        startActivity(intent);
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {

                        getSearchSuggestions(newText);
                        return false;
                    }
                }

                );

                searchViewAndroidActionBar.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
                    @Override
                    public boolean onSuggestionSelect(int i) {
                        return false;
                    }

                    @Override
                    public boolean onSuggestionClick(int i) {

                        SearchSuggestions searchSuggest=searchSuggestions.get(i);
                        String id=searchSuggest.getCdItem();
                        Intent intent = new Intent(HomeActivity.this, ViewItemSingle.class);
                        intent.putExtra("id", id);
                        startActivity(intent);


                        return false;
                    }
                });
                break;

            case R.id.action_cart:
                // Do something

                startActivityForResult(new Intent(this, CartActivity.class), 203);
                return true;


            case R.id.action_news:
                // Do something

                startActivityForResult(new Intent(this, NewsViewActivity.class), 204);
                return true;


        }
        return super.onOptionsItemSelected(item);
    }


    public void getSearchSuggestions(String txt)
    {
        RestService.RestApiInterface client = RestService.getClient();

        SharedPreferences sharedpreferences =getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        Call<List<SearchSuggestions>>jsonArrayCall=client.getSearchSuggestios(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),txt);

        jsonArrayCall.enqueue(new Callback<List<SearchSuggestions>>() {
            @Override
            public void onResponse(Call<List<SearchSuggestions>> call, Response<List<SearchSuggestions>> response) {

                if(response.isSuccessful())
                {

                    try{

                        //JSONArray jsonArray=new JSONArray(response.body().toString());

                        if(response.body().size()>0)
                        {

                            searchSuggestions.clear();

                            searchSuggestions.addAll(response.body());

                            final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, "cityName" });
                            for (int i=0; i<response.body().size(); i++) {
                               // if (SUGGESTIONS[i].toLowerCase().startsWith(query.toLowerCase()))
                                    c.addRow(new Object[] {i, response.body().get(i).getDsItem()});
                            }
                            mAdapter.changeCursor(c);

                        }







                    }catch (Exception e)
                    {

                    }



                }




            }

            @Override
            public void onFailure(Call<List<SearchSuggestions>> call, Throwable t) {

                t.printStackTrace();
            }
        });

    }


    @Override
    protected void onRestart() {
        super.onRestart();

        setupBadge();
    }

    private void setupBadge() {

        if (textCartItemCount != null) {

          SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
          String token=ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, "");

          Log.e("Token forcount",token);

            RestService.RestApiInterface client = RestService.getClient();
            Call<JsonObject> callback = client.getCartCount(token);

             callback.enqueue(new Callback<JsonObject>() {
                 @Override
                 public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                     if(response.isSuccessful())
                     {
                         try{

                             JSONObject jsonArray=new JSONObject(response.body().toString());


                             if(jsonArray.getInt("count")==0)
                             {
                                 textCartItemCount.setVisibility(View.GONE);
                             }
                             else {

                                // JSONObject jsonObject=jsonArray.getJSONObject(0);

                                 textCartItemCount.setVisibility(View.VISIBLE);

                                 textCartItemCount.setText(""+jsonArray.getInt("count"));
                             }


                             // notifyItemRangeChanged(0,items.size());


                         }catch (Exception e)
                         {

                         }
                     }
                     else {

                         Toast.makeText(HomeActivity.this, "failure", Toast.LENGTH_SHORT).show();
                     }
                 }

                 @Override
                 public void onFailure(Call<JsonObject> call, Throwable t) {
                     t.printStackTrace();

                     Toast.makeText(HomeActivity.this, "failure", Toast.LENGTH_SHORT).show();

                 }
             });


//            if (mCartItemCount != null && Constants.mCartItemCount == 0) {
//                Log.v("TAG", "qy");
//
//                if (textCartItemCount.getVisibility() != View.GONE) {
//                       textCartItemCount.setVisibility(View.GONE);
//                }
//            } else {
//                Log.v("TAG", "ty"+Constants.mCartItemCount);
//                if (Constants.cartArray != null) {
//                    Constants.mCartItemCount = Constants.cartArray.size();
//                }
//
//                textCartItemCount.setText(String.valueOf(Constants.mCartItemCount));
//                if (Constants.cartArray == null) {
//
//                    textCartItemCount.setVisibility(View.GONE);
//                    textCartItemCount.setText("" + 0);
//                }
//                if (textCartItemCount.getVisibility() != View.VISIBLE) {
//                    textCartItemCount.setVisibility(View.VISIBLE);
//
//                }
//
//            }
        }
    }


    public void OnaddCart() {
        setupBadge();

    }

    @Override
    public void onHandleResult(String text) {
        Intent intent = new Intent(this, ViewItemSingle.class);
        intent.putExtra("id", text);
        startActivityForResult(intent, 205);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        Log.v("TAG", "1ds");
        setupBadge();
        // check if the request code is same as what is passed  here it is 2
    }

    public void OnRemoveCart() {

    }

    @Override
    public void onHandleSelection(String text) {
        ViewItemSingle.setListner(this);

        Intent intent = new Intent(this, ViewItemSingle.class);
        intent.putExtra("id", text);

        startActivityForResult(intent, 205);
    }


    @Override
    public void onUpdate() {
        Log.v("TAG", "1zs");
        if (Constants.cartArray != null) {
            Constants.mCartItemCount = Constants.cartArray.size();
            Log.v("TAG", "xzy"+Constants.mCartItemCount);


            setupBadge();

        }

       // setupBadge();

    }

}
