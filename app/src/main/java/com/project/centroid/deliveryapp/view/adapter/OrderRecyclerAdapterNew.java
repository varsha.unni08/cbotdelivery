package com.project.centroid.deliveryapp.view.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.project.centroid.deliveryapp.OrderDetailsActivity;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.ViewItemSingle;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.OrderItem;
import com.project.centroid.deliveryapp.data.remote.model.OrderItem;
import com.project.centroid.deliveryapp.view.interfaces.cartListner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderRecyclerAdapterNew extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;

    private cartListner listner;
    private boolean isSwitchView = true;
    private static final int LIST_ITEM = 0;
    private static final int GRID_ITEM = 1;

    private final int VIEW_PROG = 2;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private List<OrderItem> users;
    private OrderItem item;

    public OrderRecyclerAdapterNew(Context context, List<OrderItem> users, RecyclerView recyclerView) {
        this.users = users;
        this.context = context;

        Log.v("TAG r", "" + recyclerView.getLayoutManager());
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {


            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    totalItemCount = linearLayoutManager.getItemCount();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        } else if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {


            final GridLayoutManager linearLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            Log.v("TAG r1", "" + recyclerView.getLayoutManager());

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    totalItemCount = linearLayoutManager.getItemCount();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = null;
        Log.v("TAG 1r", "er" + i);
        RecyclerView.ViewHolder vh = null;
        switch (i) {
            case LIST_ITEM:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_itemorders, viewGroup, false);
                vh = new ItemViewHolder(v);
                break;
           
            case VIEW_PROG:
                Log.v("TAG 2r", "er");
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_progress_loadmore, viewGroup, false);
                vh = new ProgressViewHolder(v);
                break;
        }

        return vh;

    }

    @Override
    public int getItemViewType(int position) {
        if (isSwitchView) {
            return users.get(position) != null ? LIST_ITEM : VIEW_PROG;
        } else {
            return users.get(position) != null ? GRID_ITEM : VIEW_PROG;
        }

    }

    public boolean toggleItemViewType() {
        isSwitchView = !isSwitchView;
        return isSwitchView;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolderNew, final int position) {


        if (viewHolderNew instanceof OrderRecyclerAdapterNew.ItemViewHolder) {
            Log.v("TAG r", "er");

           ItemViewHolder holder = (ItemViewHolder) viewHolderNew;
            item = users.get(position);

            try {
                holder.set(item);
                if( users.get(position).getCdSalesOrderStatus().equalsIgnoreCase("1")){
                    holder.cancelOrder.setVisibility(View.VISIBLE);
                    holder.cancelOrder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                            alertDialogBuilder.setMessage("Are you sure, You wanted to cancel order");
                            alertDialogBuilder.setPositiveButton("yes",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            cancelorder(users.get(position).getCdSalesOrder(),position);
                                        }
                                    });

                            alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();


                        }
                    });}else{
                    holder.cancelOrder.setVisibility(View.GONE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (viewHolderNew instanceof ProgressViewHolder) {
            ProgressViewHolder viewHolder = (ProgressViewHolder) viewHolderNew;

            viewHolder.progressBar.setIndeterminate(true);
        }

    }


    @Override
    public int getItemCount() {

        return users == null ? 0 : users.size();
        //   return 5;
    }

    private void cancelorder(String orderid, final int pos) {



        final ProgressDialog progressDialog = new ProgressDialog(context,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        SharedPreferences sharedpreferences =context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);



        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();



        Call<ResponseBody> callback = client.cancelOrder(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),orderid);
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject= new JSONObject(response.body().string());
                        if(jsonObject.getString("status").equalsIgnoreCase("1")){
                            // items.remove(i)
                            Toast.makeText(context, "order cancelled successfully", Toast.LENGTH_SHORT).show();


                            users.remove(pos);
                            notifyItemRemoved(pos);





                        }else{

                            Toast.makeText(context, " Please try again after some time!", Toast.LENGTH_SHORT).show();

                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.orderdate)
        TextView orderdate;
        @BindView(R.id.deliverydate)
        TextView deliverydate;
        @BindView(R.id.amount)
        TextView itemPrice;
        @BindView(R.id.btnremove)
        Button  cancelOrder;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(context, OrderDetailsActivity.class);
                    intent.putExtra("orderid",users.get(getAdapterPosition()).getCdSalesOrder());


                    context.startActivity(intent);
                }
            });
        }


        public void set(OrderItem item) {
            //if (ApiConstants.isNonEmpty(item.getDsSalesOrder())) {
            name.setText(" Order : " + item.getCdSalesOrder());
//            }
//            if (ApiConstants.isNonEmpty(item.getDtSalesOrder())) {
            orderdate.setText("Delivery date : " + item.getDtDelivery());
//            }
//            if (ApiConstants.isNonEmpty(item.getDtDelivery())) {
           // deliverydate.setText(" " + item.getCdSalesOrderStatus());
            deliverydate.setVisibility(View.GONE);
//            }
//            if (ApiConstants.isNonEmpty(item.getVlGrandTotal())) {
            itemPrice.setText("Amount :"+ApiConstants.amountFormatter(item.getVlGrandTotal()));
            //UI setting code
        }
    }

    
    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(@NonNull View v) {
            super(v);
            progressBar = v.findViewById(R.id.progress);
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public void setLoaded() {
        loading = false;
    }

    public void setLoadedFalse() {
        loading = true;
    }
    
}
