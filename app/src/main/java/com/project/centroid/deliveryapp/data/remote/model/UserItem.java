package com.project.centroid.deliveryapp.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserItem {

    @SerializedName("ds_customer")
    @Expose
    private String dsCustomer;
    @SerializedName("ds_address")
    @Expose
    private String dsAddress;
    @SerializedName("ds_phoneno")
    @Expose
    private String dsPhoneno;
    @SerializedName("ds_email")
    @Expose
    private String dsEmail;
    @SerializedName("ds_delivery_address")
    @Expose
    private String dsDeliveryAddress;
    @SerializedName("ds_pincode")
    @Expose
    private String dsPincode;

    public String getDsCustomer() {
        return dsCustomer;
    }

    public void setDsCustomer(String dsCustomer) {
        this.dsCustomer = dsCustomer;
    }

    public String getDsAddress() {
        return dsAddress;
    }

    public void setDsAddress(String dsAddress) {
        this.dsAddress = dsAddress;
    }

    public String getDsPhoneno() {
        return dsPhoneno;
    }

    public void setDsPhoneno(String dsPhoneno) {
        this.dsPhoneno = dsPhoneno;
    }

    public String getDsEmail() {
        return dsEmail;
    }

    public void setDsEmail(String dsEmail) {
        this.dsEmail = dsEmail;
    }

    public String getDsDeliveryAddress() {
        return dsDeliveryAddress;
    }

    public void setDsDeliveryAddress(String dsDeliveryAddress) {
        this.dsDeliveryAddress = dsDeliveryAddress;
    }

    public String getDsPincode() {
        return dsPincode;
    }

    public void setDsPincode(String dsPincode) {
        this.dsPincode = dsPincode;
    }

}