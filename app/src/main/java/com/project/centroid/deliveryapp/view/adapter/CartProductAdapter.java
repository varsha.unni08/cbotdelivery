package com.project.centroid.deliveryapp.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.data.remote.model.CartProduct;
import com.project.centroid.deliveryapp.view.CartActivity;
import com.travijuu.numberpicker.library.Enums.ActionEnum;
import com.travijuu.numberpicker.library.Interface.ValueChangedListener;
import com.travijuu.numberpicker.library.NumberPicker;

import java.util.List;

public class CartProductAdapter extends RecyclerView.Adapter<CartProductAdapter.CartProductHolder> {



    Context context;
    List<CartProduct>cartProducts;

    public CartProductAdapter(Context context, List<CartProduct> cartProducts) {
        this.context = context;
        this.cartProducts = cartProducts;
    }

    public class CartProductHolder extends RecyclerView.ViewHolder{

        TextView txtProductDetails;

        ImageView imgproduct;

        NumberPicker number_picker;

        ImageView delete;

        public CartProductHolder(@NonNull View itemView) {
            super(itemView);
            txtProductDetails=itemView.findViewById(R.id.txtProductDetails);
            imgproduct=itemView.findViewById(R.id.imgproduct);
            number_picker=itemView.findViewById(R.id.number_picker);
            delete=itemView.findViewById(R.id.delete);

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((CartActivity)context).deleteFromCart(cartProducts.get(getAdapterPosition()).getCdItem(),getAdapterPosition());
                }
            });


            number_picker.setValueChangedListener(new ValueChangedListener() {
                @Override
                public void valueChanged(int value, ActionEnum action) {

                   // if(value!=0) {

                    //((CartActivity) context).reduceValue();

                        ((CartActivity) context).setCartQuantity(value, cartProducts.get(getAdapterPosition()).getCdItem(),getAdapterPosition());

                  //  }

                }
            });
        }
    }


    @NonNull
    @Override
    public CartProductHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_cartproductadapter,viewGroup,false);



        return new CartProductHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartProductHolder cartProductHolder, int i) {

        if(i==cartProducts.size()-1)
        {
            ((CartActivity)context).loadmore();
        }



        Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(cartProducts.get(i).getDsItemPic()).into(cartProductHolder.imgproduct);

        cartProductHolder.txtProductDetails.setText(cartProducts.get(i).getDsItem()+"\n\nPrice : "+cartProducts.get(i).getVlSellingPrice()+" Rs");


        cartProductHolder.number_picker.setMax(15);
        cartProductHolder.number_picker.setMin(1);
        cartProductHolder.number_picker.setUnit(1);
        //cartProductHolder.number_picker.setValue(1);
        cartProductHolder.number_picker.setValue(Integer.parseInt(cartProducts.get(i).getCartQty()));
}

    @Override
    public int getItemCount() {
        return cartProducts.size();
    }
}
