package com.project.centroid.deliveryapp.data.remote.modelnew;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MainCategory {

    String data;
    int selected=0;

    @SerializedName("Status")
    @Expose
    private String Status="1";
    @SerializedName("Warning")
    @Expose
    private String Warning;
    @SerializedName("cd_main_category")
    @Expose
    private String cdMainCategory;
    @SerializedName("ds_main_category")
    @Expose
    private String dsMainCategory;
    @SerializedName("ds_image")
    @Expose
    private String dsImage;
    @SerializedName("subcategory")
    @Expose
    private List<MainSubcategory> subcategories=new ArrayList<>();



    public MainCategory() {
    }


    public List<MainSubcategory> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<MainSubcategory> subcategories) {
        this.subcategories = subcategories;
    }

    public String getWarning() {
        return Warning;
    }

    public void setWarning(String warning) {
        Warning = warning;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getCdMainCategory() {
        return cdMainCategory;
    }

    public void setCdMainCategory(String cdMainCategory) {
        this.cdMainCategory = cdMainCategory;
    }

    public String getDsMainCategory() {
        return dsMainCategory;
    }

    public void setDsMainCategory(String dsMainCategory) {
        this.dsMainCategory = dsMainCategory;
    }

    public String getDsImage() {
        return dsImage;
    }

    public void setDsImage(String dsImage) {
        this.dsImage = dsImage;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }
}
