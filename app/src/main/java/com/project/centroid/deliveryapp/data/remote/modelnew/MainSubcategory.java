package com.project.centroid.deliveryapp.data.remote.modelnew;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainSubcategory {

    @SerializedName("cd_category")
    @Expose
    private String cd_category;
    @SerializedName("ds_category")
    @Expose
    private String ds_category;

    @SerializedName("Status")
    @Expose
    private String Status="1";
    @SerializedName("Warning")
    @Expose
    private String Warning;

    public MainSubcategory() {
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getWarning() {
        return Warning;
    }

    public void setWarning(String warning) {
        Warning = warning;
    }

    public String getCd_category() {
        return cd_category;
    }

    public void setCd_category(String cd_category) {
        this.cd_category = cd_category;
    }

    public String getDs_category() {
        return ds_category;
    }

    public void setDs_category(String ds_category) {
        this.ds_category = ds_category;
    }
}
