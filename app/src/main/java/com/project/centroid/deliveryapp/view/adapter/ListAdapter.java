package com.project.centroid.deliveryapp.view.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.data.remote.model.SubCategory;

import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends ArrayAdapter<SubCategory> {

    private Context mContext;
    private List<SubCategory> mList = new ArrayList<>();

    public ListAdapter(@NonNull Context context,  List<SubCategory> list) {
        super(context, 0, list);
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.list_itemcat, parent, false);

        SubCategory subCategory = mList.get(position);


        TextView name = (TextView) listItem.findViewById(R.id.name);
        name.setText(subCategory.getDsSubcategory());


        return listItem;
    }
}
