package com.project.centroid.deliveryapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.project.centroid.deliveryapp.Utils.Constants;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Cart;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.view.CartActivity;
import com.project.centroid.deliveryapp.view.HomeActivity;
import com.project.centroid.deliveryapp.view.LoginActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewItemSingle extends AppCompatActivity {
    List<Item> data;
    @BindView(R.id.itemPic)
    ImageView itemPic;
    @BindView(R.id.itemName)
    TextView itemName;
    @BindView(R.id.itemDesc)
    TextView itemDesc;
    @BindView(R.id.itemPrice)
    TextView itemPrice;
    @BindView(R.id.bookmark)
    ImageView bookmark;
    @BindView(R.id.textnewprice)
    TextView textnewprice;
    @BindView(R.id.edit)
    LinearLayout add;
    private SharedPreferences sharedpreferences;
    private Item item;
    static  UpdateListner updateListner;

    boolean isWishlist=false;

    TextView textCartItemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item_single);
        ButterKnife.bind(this);
        sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        try {
            data = new ArrayList<>();
            if(Constants.cartArray!=null){
                data=Constants.cartArray;
            }
            if (getIntent().getStringExtra("id") != null) {
                getSingle(getIntent().getStringExtra("id"));

                checkWishListExistence(getIntent().getStringExtra("id"));
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.app_name);

        } catch (Exception e) {
            e.printStackTrace();
        }

        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!isWishlist) {

                    addToWishList(getIntent().getStringExtra("id"));
                }
                else {

                    removeWishList(getIntent().getStringExtra("id"));

                }
            }
        });

    }


    private void setupBadge() {

        if (textCartItemCount != null) {

            SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
            String token=ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, "");

            Log.e("Token forcount",token);

            RestService.RestApiInterface client = RestService.getClient();
            Call<JsonObject> callback = client.getCartCount(token);

            callback.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if(response.isSuccessful())
                    {
                        try{

                            JSONObject jsonArray=new JSONObject(response.body().toString());


                            if(jsonArray.getInt("count")==0)
                            {
                                textCartItemCount.setVisibility(View.GONE);
                            }
                            else {

                                // JSONObject jsonObject=jsonArray.getJSONObject(0);

                                textCartItemCount.setVisibility(View.VISIBLE);

                                textCartItemCount.setText(""+jsonArray.getInt("count"));
                            }


                            // notifyItemRangeChanged(0,items.size());


                        }catch (Exception e)
                        {

                        }
                    }
                    else {

                        Toast.makeText(ViewItemSingle.this, "failure", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();

                    Toast.makeText(ViewItemSingle.this, "failure", Toast.LENGTH_SHORT).show();

                }
            });


//            if (mCartItemCount != null && Constants.mCartItemCount == 0) {
//                Log.v("TAG", "qy");
//
//                if (textCartItemCount.getVisibility() != View.GONE) {
//                       textCartItemCount.setVisibility(View.GONE);
//                }
//            } else {
//                Log.v("TAG", "ty"+Constants.mCartItemCount);
//                if (Constants.cartArray != null) {
//                    Constants.mCartItemCount = Constants.cartArray.size();
//                }
//
//                textCartItemCount.setText(String.valueOf(Constants.mCartItemCount));
//                if (Constants.cartArray == null) {
//
//                    textCartItemCount.setVisibility(View.GONE);
//                    textCartItemCount.setText("" + 0);
//                }
//                if (textCartItemCount.getVisibility() != View.VISIBLE) {
//                    textCartItemCount.setVisibility(View.VISIBLE);
//
//                }
//
//            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        setupBadge();
    }

    private void removeWishList(String orderid) {



        final ProgressDialog progressDialog = new ProgressDialog(ViewItemSingle.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        SharedPreferences sharedpreferences =getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);



        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();



        Call<ResponseBody> callback = client.removeWishList(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),orderid);
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject= new JSONObject(response.body().string());
                        if(jsonObject.getString("status").equalsIgnoreCase("1")){
                            //items.remove(pos);

                            isWishlist=false;
                            bookmark.setImageResource(R.drawable.ic_favorite_border_red);


                            Toast.makeText(ViewItemSingle.this, "item removed successfully", Toast.LENGTH_SHORT).show();

                        }else{

                            Toast.makeText(ViewItemSingle.this, " ", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }



    public void checkWishListExistence(String cditem)
    {
        SharedPreferences sharedpreferences =getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        RestService.RestApiInterface client = RestService.getClient();
        Call<JsonArray> callback = client.getWishListExistence(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),cditem);
        callback.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                if(response.isSuccessful())
                {
                    try{

                        JSONArray jsonArray=new JSONArray(response.body().toString());
                        JSONObject jsonObject=jsonArray.getJSONObject(0);
                        if(jsonObject.getInt("whishlist_occurance")==1)
                        {

                            //items.get(position).setIsInWishlist(1);

                            isWishlist=true;

                            bookmark.setImageResource(R.drawable.ic_favorite_red);
                        }
                        else {

                           // items.get(position).setIsInWishlist(0);

                            isWishlist=false;

                            bookmark.setImageResource(R.drawable.ic_favorite_border_red);
                        }

                       // notifyItemRangeChanged(0,items.size());


                    }catch (Exception e)
                    {

                    }
                }
                else {

                    Toast.makeText(ViewItemSingle.this, "failure", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                t.printStackTrace();

                Toast.makeText(ViewItemSingle.this, "failure", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void getSingle(String id) {

        final ProgressDialog progressDialog = new ProgressDialog(ViewItemSingle.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Item>> callback = client.getSingleItem(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), id);
        callback.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                progressDialog.dismiss();

                try {
                    if (response.body() != null) {
                         item = response.body().get(0);
                        if (ApiConstants.isNonEmpty(item.getDsItem())) {
                            itemName.setText(item.getDsItem());
                        }
                        if (ApiConstants.isNonEmpty(item.getDsItemDescription())) {
                            itemDesc.setText(item.getDsItemDescription());
                        }
                        if (ApiConstants.isNonEmpty(item.getVlMrp())) {
                            itemPrice.setText(ApiConstants.amountFormatter(item.getVlMrp()));
                        }
                        if (ApiConstants.isNonEmpty(item.getVl_selling_price())) {
                            if (ApiConstants.isNonEmpty(item.getVlMrp())) {
                                if (!item.getVlMrp().equalsIgnoreCase(item.getVl_selling_price())) {

                                    itemPrice.setPaintFlags(itemPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                    textnewprice.setText(ApiConstants.amountFormatter(item.getVl_selling_price()));
                                }
                            }
                        }
                        if (ApiConstants.isNonEmpty(item.getDsItemPicSmall())) {
                            Glide.with(ViewItemSingle.this).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(item.getDsItem()).into(itemPic);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        switch (id) {
//            case android.R.id.home:
//
//                Intent intent1=getIntent();
//                setResult(Activity.RESULT_OK, intent1);
//                finish();
//                if(updateListner!=null){
//                    updateListner.onUpdate();
//                }
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @OnClick(R.id.edit)
    public void onViewClicked() {

        try{
            if(item!=null){
                Gson gson = new Gson();
                final String data = gson.toJson(item);
                addtoCart(item.getCdItem());  }
        } catch (Exception e){
            e.printStackTrace();
        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart_menu_p, menu);

        final MenuItem menuItem = menu.findItem(R.id.action_cart);

        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        setupBadge();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        return true;
    }





    private void addtoCart(String cdItem) {

        final ProgressDialog progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Calendar cldr=Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        month++;

        String date=day+"-"+month+"-"+year;


        Call<JsonArray> callback = client.addProductCart(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),cdItem,"1",date);

        callback.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                progressDialog.dismiss();
                if(response.isSuccessful())
                {


                    try {
                        JSONArray jsonArray= new JSONArray(response.body().toString());

                        if (jsonArray.length()>0)
                        {

                            setupBadge();

//                            JSONObject jsonObject=jsonArray.getJSONObject(0);
//
//
//                        if(jsonObject.getInt("status")==1){
//
//
//                            Toast.makeText(ViewItemSingle.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//
//                        }else{
//
//                            Toast.makeText(ViewItemSingle.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//
//                        }

                            Toast.makeText(ViewItemSingle.this, "item added to cart", Toast.LENGTH_SHORT).show();



                        }else{

                            Toast.makeText(ViewItemSingle.this, "failure", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                }




            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {

                progressDialog.dismiss();
                t.printStackTrace();

            }
        });






    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {


            case R.id.action_cart:
                // Do something
                Intent intent = new Intent(this, CartActivity.class);
                startActivityForResult(intent, 123);
                break;

            case android.R.id.home:


                onBackPressed();

                break;
        }

        return false;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public static void setListner(HomeActivity listner) {
        Log.v("TAGG","dfdfdf");
        updateListner=(UpdateListner)listner;
    }

    public interface UpdateListner{
        void onUpdate();
    }


    private void addToWishList(final String cdItem) {
        SharedPreferences sharedpreferences =getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);

        final ProgressDialog progressDialog = new ProgressDialog(ViewItemSingle.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();



        Call<ResponseBody> callback = client.addWishList(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),cdItem,"1");
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject= new JSONObject(response.body().string());
                        if(jsonObject.getString("status").equalsIgnoreCase("1")){

                            isWishlist=true;


                            Toast.makeText(ViewItemSingle.this, "Item added to wishlist successfully", Toast.LENGTH_SHORT).show();

                            bookmark.setImageResource(R.drawable.ic_favorite_red);



                        }else{

                            Toast.makeText(ViewItemSingle.this, "Some thing went wrong.Please try after some time", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }
}
