package com.project.centroid.deliveryapp.view.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.Constants;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.view.interfaces.cartAmoutListner;
import com.project.centroid.deliveryapp.view.interfaces.cartListner;
import com.travijuu.numberpicker.library.Enums.ActionEnum;
import com.travijuu.numberpicker.library.Interface.ValueChangedListener;
import com.travijuu.numberpicker.library.NumberPicker;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Sooraj Soman on 11/1/2018
 */
public class CartRecyclerAdapter extends RecyclerView.Adapter<CartRecyclerAdapter.ItemViewHolder> {
    private final Context context;

    private List<Item> items;
    private cartListner listner;
    private cartAmoutListner mlistner;

    public CartRecyclerAdapter(Context context, List<Item> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_cart, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        try {
            final Item item = items.get(position);

            Log.v("TAG","V"+position);
            if (ApiConstants.isNonEmpty(item.getDsItem())) {
                holder.itemName.setText(item.getDsItem());
            }
            if (ApiConstants.isNonEmpty(item.getDsItemDescription())) {
                holder.itemDesc.setText(item.getDsItemDescription());
            }
            if (ApiConstants.isNonEmpty(item.getVlMrp())) {
                holder.itemPrice.setText(ApiConstants.amountFormatter(item.getVlMrp()));
            }
            if(ApiConstants.isNonEmpty(item.getVl_selling_price())){
                if (ApiConstants.isNonEmpty(item.getVlMrp())) {
                    if(!item.getVlMrp().equalsIgnoreCase(item.getVl_selling_price())){

                       holder.itemPrice.setPaintFlags(holder.itemPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        holder.textnewprice.setText(ApiConstants.amountFormatter(item.getVl_selling_price()));
                    }}
            }
            if (ApiConstants.isNonEmpty(item.getDsItemPicSmall())) {
                Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(item.getDsItemPic()).into(holder.itemPic);
            }
            final Item item1 = item;

            holder.numberPicker.setValueChangedListener(new ValueChangedListener() {
                @Override
                public void valueChanged(int value, ActionEnum action) {

                item.setItemQuantity(value);
                mlistner.OnQuantity(item.getDsItemCode(), String.valueOf(value));

                }
            });
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //listner.OnaddCart(item1.getDsItemCode());
                    if(Constants.cartArray!=null){
                        items.remove(position);

                        Constants.cartArray=Constants.removeItem(item.getDsItemCode(),Constants.cartArray);
                        mlistner.OnQuantity(null,"0");
                        notifyDataSetChanged();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setListner(cartListner listner) {
        this.listner = listner;
    }
    public void setAmountListner(cartAmoutListner listner) {
        this.mlistner = listner;
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemPic)
        ImageView itemPic;
        @BindView(R.id.itemName)
        TextView itemName;
        @BindView(R.id.itemDesc)
        TextView itemDesc;
        @BindView(R.id.itemPrice)
        TextView itemPrice;
        @BindView(R.id.number_picker)
        NumberPicker numberPicker;
        @BindView(R.id.addbtn)
        LinearLayout addbtn;
        @BindView(R.id.delete)
        ImageView delete;
        @BindView(R.id.textnewprice)
        TextView textnewprice;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }



    }
}