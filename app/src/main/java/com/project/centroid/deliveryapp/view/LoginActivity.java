package com.project.centroid.deliveryapp.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.project.centroid.deliveryapp.MainActivity;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.Constants;
import com.project.centroid.deliveryapp.application.MyApplication;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.reciever.NetworkChangeReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.regex.Pattern;

import butterknife.BindView;

import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class LoginActivity extends AppCompatActivity implements NetworkChangeReceiver.ConnectivityReceiverListener {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    @BindView(R.id.input_email)
    EditText _emailText;
    @BindView(R.id.input_password)
    EditText _passwordText;
    @BindView(R.id.btn_login)
    Button _loginButton;
    @BindView(R.id.link_signup)
    TextView _signupLink;

    @BindView(R.id.imgback)
    ImageView imgback;


    private SharedPreferences sharedpreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        ButterKnife.bind(this);
       sharedpreferences = this.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        if (!TextUtils.isEmpty(sharedpreferences.getString(ApiConstants.LOGINSTATE, ""))) {
            if (NetworkChangeReceiver.isConnected()) {
                startActivity(new Intent(LoginActivity.this, MainHomeActivity.class));
                finish();
            } else {
                Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

            }
        }
            _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
               // finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

       // _loginButton.setEnabled(false);
       if(NetworkChangeReceiver.isConnected()){
          loginCall();
       }else{

           Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

       }

    }

    private void loginCall() {

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<ResponseBody> callback = client.loginRequest("login", email, password);
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject= new JSONObject(response.body().string());
                        if(jsonObject.getString("status").equalsIgnoreCase("1")){

                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString(ApiConstants.USERTOKEN, jsonObject.getString("token"));
                            editor.putString(ApiConstants.LOGINSTATE, "true");
                            editor.apply();
                            editor.commit();
                            onLoginSuccess();

                        }else{
                            onLoginFailed();
                            // Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        //_loginButton.setEnabled(true);
        Constants.cartArray=null;
        Constants.mCartItemCount=0;
        Constants.type=null;
        startActivity(new Intent(this, MainHomeActivity.class));
        finish();


    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        /*if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }*/
        Pattern pattern = Pattern.compile("^[6-9]\\d{9}$");
        if (email.isEmpty() || !pattern.matcher(email).matches()) {
            _emailText.setError("enter a valid mobile number");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 3 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }
    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setConnectivityListener(this);

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
