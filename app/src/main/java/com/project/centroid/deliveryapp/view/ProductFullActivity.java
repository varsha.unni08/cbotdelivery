package com.project.centroid.deliveryapp.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.data.remote.modelnew.ProductWithCategory;
import com.project.centroid.deliveryapp.view.adapter_new.ProductItemAdapter;
import com.project.centroid.deliveryapp.view.progress.ProgressFragment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductFullActivity extends AppCompatActivity {

    TextView txtTitle;
    ImageView imgback;

    RecyclerView recyclerView;

    ProductWithCategory productWithCategory;

    ProductItemAdapter productItemAdapter;

    int lastVisibleItem, totalItemCount;

    int visibleThreshold = 1;

    boolean loading;

    List<Item>items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_full);
        getSupportActionBar().hide();

        imgback=findViewById(R.id.imgback);
        txtTitle=findViewById(R.id.txtTitle);
        recyclerView=findViewById(R.id.recyclerView);
        items=new ArrayList<>();

        productWithCategory=(ProductWithCategory)getIntent().getSerializableExtra("prod");

        txtTitle.setText(productWithCategory.getCategorydata());


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        setupadapter(productWithCategory.getItems());

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                totalItemCount = linearLayoutManager.getItemCount();
                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {

                        onLoadMore();
                   
                    loading = true;
                }
            }
        });
    }








    public void setupadapter(List<Item>items)
    {
        productItemAdapter=new ProductItemAdapter(ProductFullActivity.this,items);
        recyclerView.setLayoutManager(new LinearLayoutManager(ProductFullActivity.this));
        recyclerView.setAdapter(productItemAdapter);

        loading=false;
    }


    public void onLoadMore()
    {



        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"jdfm");
        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        RestService.RestApiInterface client = RestService.getClient();
        Call<List<Item>> callBack = client.getFilterCategorySubcategoryList(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), productWithCategory.getMaincatid(), productWithCategory.getCatid(), "1");

        callBack.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                try {
                    progressFragment.dismiss();

                    if (!response.body().isEmpty()) {
                        if (response.body() != null) {


                            List<Item> posts = response.body();

                            if(posts.size()>0)
                            {


                                items.addAll(posts);


                            }

                            productItemAdapter.notifyDataSetChanged();


                        } else {
                            Log.v("TAG", "loadingend");




                        }
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {

                progressFragment.dismiss();
            }
        });



    }
}
