package com.project.centroid.deliveryapp.view.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.Constants;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.data.remote.model.OrderItem;
import com.project.centroid.deliveryapp.view.CartActivity;
import com.project.centroid.deliveryapp.view.adapter.ListAdapter;
import com.project.centroid.deliveryapp.view.adapter.WishListRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WishListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_OrderItem_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    Unbinder unbinder;
    @BindView(R.id.buynow)
    Button buynow;

    @BindView(R.id.layoutProgress)
    LinearLayout layoutProgress;

    int currentpage=1;

    boolean isend=false;

    WishListRecyclerAdapter adapter;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private SharedPreferences sharedpreferences;
    private List<Item> orderItems1;

    public WishListFragment() {
        // Required empty public constructor
    }

    public static WishListFragment newInstance() {
        WishListFragment fragment = new WishListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wishlist, container, false);
        sharedpreferences = getActivity().getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);

        orderItems1 = new ArrayList<>();
        unbinder = ButterKnife.bind(this, view);
        getLatest();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    public void loadmore()
    {

        if(!isend) {
            layoutProgress.setVisibility(View.VISIBLE);
            currentpage++;
            getLatest();
        }
        else {
            layoutProgress.setVisibility(View.GONE);
        }
    }


//    {cdItem='null', dsItemCode='null', dsItem='null', cdCategory='null', cdSubcategory='null', nrOpeningstock='null', cdUom='null', vlMrp='null', dsHsncode='null', dsItemDescription='null', dsItemPic='null', dsItemPicSmall='null', vlCgst='null', vlSgst='null', dsDelStatus='null', itemQuantity=null}

    private void getLatest() {


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Item>> callback = client.getWishList(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "wishlist",currentpage);
        callback.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                try {

                    if(response.body().size()<10)
                    {
                        layoutProgress.setVisibility(View.GONE);
                        isend=true;
                    }



                    orderItems1.addAll(response.body());
                     adapter = new WishListRecyclerAdapter(WishListFragment.this,getActivity(), orderItems1);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//                    recyclerView.setItemAnimator(new DefaultItemAnimator());
//                    recyclerView.setNestedScrollingEnabled(false);

                    recyclerView.setAdapter(adapter);
                    //recyclerView.setHasFixedSize(false);


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(),e.toString(),Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buynow)
    public void onViewClicked() {
        if(orderItems1!=null) {
            if (orderItems1.size() > 0) {

                if (Constants.cartArray == null) {
                    Log.v("TAG 12", "vt" );
                    Constants.cartArray = orderItems1;
                } else {
                    List<Item> orderItem = new ArrayList<>();
                    for (Item item : orderItems1) {
                        Log.v("TAG", "item"+item.getDsItemCode() );

                        if (Constants.isItemPresent(item.getDsItemCode(), Constants.cartArray)) {

                        } else {
                            orderItem.add(item);
                        }

                    }
                    Constants.cartArray.addAll(orderItem);
                }
                startActivity(new Intent(getActivity(), CartActivity.class));
            }
        }

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name

    }
}
