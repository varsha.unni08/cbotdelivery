package com.project.centroid.deliveryapp.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("cd_category")
    @Expose
    private String cdCategory;
    @SerializedName("ds_category")
    @Expose
    private String dsCategory;
    @SerializedName("ds_image")
    @Expose
    private String dsImage;


    /**
     * No args constructor for use in serialization
     */
    public Category() {
    }

    /**
     * @param dsCategory
     * @param cdCategory
     */
    public Category(String cdCategory, String dsCategory,String dsImage) {
        super();
        this.cdCategory = cdCategory;
        this.dsCategory = dsCategory;
        this.dsImage=dsImage;
    }

    public String getCdCategory() {
        return cdCategory;
    }

    public void setCdCategory(String cdCategory) {
        this.cdCategory = cdCategory;
    }

    public String getDsCategory() {
        return dsCategory;
    }

    public void setDsCategory(String dsCategory) {
        this.dsCategory = dsCategory;
    }

    public String getDsImage() {
        return dsImage;
    }

    public void setDsImage(String dsImage) {
        this.dsImage = dsImage;
    }

    @Override
    public String toString() {
        return ""+this.dsCategory;
    }
}