package com.project.centroid.deliveryapp.view;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.MatrixCursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.Constants;
import com.project.centroid.deliveryapp.ViewItemSingle;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Cart;
import com.project.centroid.deliveryapp.data.remote.model.CartItems;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.data.remote.model.Pincode;
import com.project.centroid.deliveryapp.data.remote.model.SearchSuggestions;
import com.project.centroid.deliveryapp.progress.ProgressFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderPlaceActivity extends AppCompatActivity implements LocationListener {

    @BindView(R.id.pincode)
    SearchView pincode;
    @BindView(R.id.pincodecheck)
    Button pincodecheck;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.address)
    EditText address;
    @BindView(R.id.addaddress)
    Button addaddress;

    @BindView(R.id.txtlocation)
    TextView txtlocation;

    @BindView(R.id.imglocation)
    ImageView imglocation;
    @BindView(R.id.txtmsg)
    TextView txtmsg;

    CursorAdapter mAdapter;

    List<Pincode>pincodes=new ArrayList<>();

   // FusedLocationProviderClient fusedLocationClient;


    Location location;
    LocationManager locationManager ;

    ProgressFragment progressFragment;

    double lat=0;
    double longi=0;



    private SharedPreferences sharedpreferences;
    private boolean pincodeFlag=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_place);
        ButterKnife.bind(this);
        sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        final String[] from = new String[] {"pincode"};
        final int[] to = new int[] {R.id.txt};
        mAdapter = new SimpleCursorAdapter(OrderPlaceActivity.this,
                R.layout.layout_listitem,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        pincode.setSuggestionsAdapter(mAdapter);


        pincode.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                                                              @Override
                                                              public boolean onQueryTextSubmit(String query) {
                                                                  pincode.clearFocus();
//                        Constants.type = "search";
//                        Intent intent = new Intent(getApplicationContext(), ProductActivity.class);
//                        intent.putExtra("key", query);
//                        startActivity(intent);

                                                                 // pincode.setQuery(pincodes.get(i).getDsPincode(),true);
                                                                  return true;
                                                              }

                                                              @Override
                                                              public boolean onQueryTextChange(String newText) {

                                                                  getSearchSuggestions(newText);
                                                                  return false;
                                                              }
                                                          }

        );

        pincode.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int i) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int i) {


                pincode.setQuery(pincodes.get(i).getDsPincode(),true);

                checkPincode();


                return false;
            }
        });

    }

    public void getSearchSuggestions(String txt)
    {
        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Pincode>> callBack = client.getPincodeSuggestions(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),txt)
                ;

        callBack.enqueue(new Callback<List<Pincode>>() {
            @Override
            public void onResponse(Call<List<Pincode>> call, Response<List<Pincode>> response) {

                if(response.isSuccessful())

                if(response.body()!=null)
                {
                {

                    pincodes.clear();

                    pincodes.addAll(response.body());

                    final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, "pincode" });
                    for (int i=0; i<pincodes.size(); i++) {
                        // if (SUGGESTIONS[i].toLowerCase().startsWith(query.toLowerCase()))
                        c.addRow(new Object[] {i, pincodes.get(i).getDsPincode()});
                    }
                    mAdapter.changeCursor(c);



                }}



            }

            @Override
            public void onFailure(Call<List<Pincode>> call, Throwable t) {

                t.printStackTrace();


            }
        });
    }


    @Override
    public void onLocationChanged(Location location) {

        if(location!=null) {

            lat = location.getLatitude();
            longi = location.getLongitude();


            Geocoder geocoder = new Geocoder(OrderPlaceActivity.this, Locale.getDefault());
            String result = null;
            try {
                List<Address> addressList = geocoder.getFromLocation(
                        lat, longi, 1);
                if (addressList != null && addressList.size() > 0) {
                    Address address = addressList.get(0);


                    if(address!=null) {
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {

                            if(address.getAddressLine(i)!=null&&!address.getAddressLine(i).equals("")) {

                                sb.append(address.getAddressLine(i)).append("\n");
                            }
                        }

                        if(address.getLocality()!=null) {
                            sb.append(address.getLocality()).append("\n");
                        }

                        if(address.getCountryName()!=null) {
                            sb.append(address.getCountryName()).append("\n");
                        }
                        //sb.append(address.getCountryName());
                        result = sb.toString();

                        txtlocation.setText(result);




                    }
                } else {


                }
            } catch (Exception e) {

            }
        }
        else{

            Toast.makeText(OrderPlaceActivity.this, "Cannot fetch location", Toast.LENGTH_LONG).show();

        }

        if(progressFragment!=null)
        {

            if(progressFragment.isVisible())
            {
                progressFragment.dismiss();
            }

        }

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @OnClick({R.id.pincodecheck, R.id.addaddress,R.id.imglocation,R.id.txtlocation})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.imglocation:


                checkPermission();

                break;


            case R.id.txtlocation:


                checkPermission();

                break;





            case R.id.pincodecheck:

                if(!TextUtils.isEmpty(pincode.getQuery().toString())){

                    //checkPincode();


                }


                break;
            case R.id.addaddress:

                if(!TextUtils.isEmpty(name.getText().toString())&& !TextUtils.isEmpty(address.getText().toString()))
                {  if(pincodeFlag){


                    placeOrder();


                }else{

                    txtmsg.setText("Items are not deleiverable to this pincode.Please try another");
                    txtmsg.setVisibility(View.VISIBLE);
                    txtmsg.setTextColor(Color.RED);
                    Snackbar.make(findViewById(android.R.id.content), "Items are not deleiverable to this pincode.Please try another", Snackbar.LENGTH_SHORT).show();

                }
                }else{
                    Snackbar.make(findViewById(android.R.id.content), "Please enter valid details.", Snackbar.LENGTH_SHORT).show();

                }
                break;
        }
    }



    public boolean CheckGpsStatus(){

        locationManager = (LocationManager)OrderPlaceActivity.this.getSystemService(Context.LOCATION_SERVICE);

     boolean   GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

     return GpsStatus;

    }



    public void checkPermission()
    {

        if(CheckGpsStatus()) {


            if (ContextCompat.checkSelfPermission(OrderPlaceActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


                ActivityCompat.requestPermissions(OrderPlaceActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 119);

            } else {

                progressFragment=new ProgressFragment();
                progressFragment.setCancelable(false);
                progressFragment.show(getSupportFragmentManager(),"dfgdf");

                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 12000, 7, OrderPlaceActivity.this);


            }
        }
        else{

            Toast.makeText(OrderPlaceActivity.this, "Please Enable GPS First", Toast.LENGTH_LONG).show();


            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }




    }









    private void addCart(CartItems cartItems) {




        final ProgressDialog progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();



        Call<ResponseBody> callback = client.addCart(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),cartItems);
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject= new JSONObject(response.body().string());
                        if(jsonObject.getString("status").equalsIgnoreCase("1")){



                        }else{

                            // Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

                        }
                       // placeOrder();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }

    private void placeOrder() {



        final ProgressDialog progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();



        Call<ResponseBody> callback = client.placeOrder(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),pincode.getQuery().toString(),name.getText().toString()+","+address.getText().toString());
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject= new JSONObject(response.body().string());
                        if(jsonObject.getString("status").equalsIgnoreCase("1")){
                            Snackbar.make(findViewById(android.R.id.content), "Order  placed successfully.", Snackbar.LENGTH_SHORT).show();


                            showDialog();


                        }else{

                             Snackbar.make(findViewById(android.R.id.content), "Order not placed.Please try again after sometime.", Snackbar.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });


    }

    private void showDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Order confirmation");
        alertDialogBuilder.setMessage("Order placed successfully!");
                alertDialogBuilder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                startActivity(new Intent(OrderPlaceActivity.this,HomeActivity.class));
                                finish();
                            }
                        });



        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

    }

    private void checkPincode() {



            final ProgressDialog progressDialog = new ProgressDialog(this,
                    R.style.AppTheme_Dark_Dialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();


            // TODO: Implement your own authentication logic here.
            RestService.RestApiInterface client = RestService.getClient();



            Call<ResponseBody> callback = client.checkPincode(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),pincode.getQuery().toString());
            callback.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    Log.v("TAG", response.body().toString());
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        progressDialog.hide();
                        try {
                            JSONObject jsonObject= new JSONObject(response.body().string());
                            if(jsonObject.getString("status").equalsIgnoreCase("1")){

                                 pincodeFlag = true;
                                Snackbar.make(findViewById(android.R.id.content), "Items deliverable to this pincode.", Snackbar.LENGTH_SHORT).show();


                                txtmsg.setText("Items are  deleiverable to this pincode");
                                txtmsg.setVisibility(View.VISIBLE);
                                txtmsg.setTextColor(Color.GREEN);

                            }else{

                                txtmsg.setText("Items are not deliverable to this pincode.Please try another");
                                txtmsg.setVisibility(View.VISIBLE);
                                txtmsg.setTextColor(Color.RED);

                                 Snackbar.make(findViewById(android.R.id.content), "Items are not deliverable to this pincode.Please try another", Snackbar.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }


                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    t.printStackTrace();
                }
            });
        }

}
