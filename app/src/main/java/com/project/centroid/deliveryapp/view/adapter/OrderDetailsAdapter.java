package com.project.centroid.deliveryapp.view.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.project.centroid.deliveryapp.OrderDetailsActivity;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.OrderDetails;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsAdapter  extends RecyclerView.Adapter<OrderDetailsAdapter.OrderDetailHolder> {


    Context context;
    List<OrderDetails>orderDetails;
    String orderstatus;

    public OrderDetailsAdapter(Context context, List<OrderDetails> orderDetails,String orderstatus) {
        this.context = context;
        this.orderDetails = orderDetails;
        this.orderstatus=orderstatus;
    }

    public class OrderDetailHolder extends RecyclerView.ViewHolder{

        TextView name;
        ImageView imgdelete;

        public OrderDetailHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            imgdelete=itemView.findViewById(R.id.imgdelete);

            imgdelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    final ProgressDialog progressDialog = new ProgressDialog(context,
                            R.style.AppTheme_Dark_Dialog);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Please wait...");
                    progressDialog.show();


                    SharedPreferences sharedpreferences = context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


                    RestService.RestApiInterface client = RestService.getClient();

                    Call<JsonObject> callBack = client.cancelOrderItem(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),orderDetails.get(getAdapterPosition()).getCdItem());


                    callBack.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {


                            progressDialog.dismiss();

                            if (response.isSuccessful()) {


                                try{

                                    JSONObject jsonObject=new JSONObject(response.body().toString());

                                    if(jsonObject.getInt("status")==1)
                                    {

                                        Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();


                                        orderDetails.remove(getAdapterPosition());

                                        if(orderDetails.size()==0)
                                        {

                                            ((OrderDetailsActivity)context).onBackPressed();
                                        }

                                        notifyDataSetChanged();

                                    }
                                    else {

                                        Toast.makeText(context,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                                    }



                                }catch (Exception e)
                                {

                                }





                            }





                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                            progressDialog.dismiss();

                            t.printStackTrace();
                        }
                    });

                }
            });
        }
    }

    @NonNull
    @Override
    public OrderDetailHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_orderdetailsadapter,viewGroup,false);


        return new OrderDetailHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderDetailHolder orderDetailHolder, int i) {

        orderDetailHolder.name.setText("Name : "+orderDetails.get(i).getItemName()+"\n Quantity : "+orderDetails.get(i).getNrQty()+"\n Total price : "+orderDetails.get(i).getVlNetTotal());


        if(!orderstatus.equalsIgnoreCase("initial"))
        {
            orderDetailHolder.imgdelete.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return orderDetails.size();
    }
}
