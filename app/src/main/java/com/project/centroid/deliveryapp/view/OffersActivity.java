package com.project.centroid.deliveryapp.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.modelnew.OfferItems;
import com.project.centroid.deliveryapp.view.adapter_new.OfferAdapter;
import com.project.centroid.deliveryapp.view.progress.ProgressFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersActivity extends AppCompatActivity {

    ImageView imgback;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        recyclerView=findViewById(R.id.recyclerView);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        showOfferSlider();
    }


    public void showOfferSlider() {
        final ProgressFragment progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "jdfm");

        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        RestService.RestApiInterface client = RestService.getClient();
        Call<OfferItems> callBack = client.getOfferlist(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""));

        callBack.enqueue(new Callback<OfferItems>() {
            @Override
            public void onResponse(Call<OfferItems> call, Response<OfferItems> response) {

                progressFragment.dismiss();


                if (response.body() != null) {

                    if (response.body().getData().size() > 0) {


                        //offers.addAll(response.body().getData());


                        OfferAdapter offerAdapter=new OfferAdapter(OffersActivity.this,response.body().getData());
                        recyclerView.setLayoutManager(new LinearLayoutManager(OffersActivity.this));
                        recyclerView.setAdapter(offerAdapter);


                       // setupCategory();
                    }


                }


            }

            @Override
            public void onFailure(Call<OfferItems> call, Throwable t) {

                progressFragment.dismiss();

            }
        });
    }
    }
