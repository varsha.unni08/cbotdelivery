package com.project.centroid.deliveryapp.view.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.Constants;
import com.project.centroid.deliveryapp.ViewItemSingle;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.view.HomeActivity;
import com.project.centroid.deliveryapp.view.ProductActivity;
import com.project.centroid.deliveryapp.view.interfaces.cartListner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Sooraj Soman on 11/1/2018
 */
public class LatestItemRecyclerAdapter extends RecyclerView.Adapter<LatestItemRecyclerAdapter.ItemViewHolder> {
    private final Context context;

    private List<Item> items;
    private cartListner listner;
    private CallbackInterfaceResult mCallback;

    public LatestItemRecyclerAdapter(Context context, List<Item> items) {
        this.items = items;
        this.context = context;
        try{
            mCallback = (CallbackInterfaceResult) context;
        }catch(ClassCastException ex){
            //.. should log the error or throw and exception
            Log.e("MyAdapter","Must implement the CallbackInterface in the Activity", ex);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_item, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        try{
        Item item = items.get(position);
         holder.set(item,position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(items.get(position).getCdItem().equals("0"))
                    {


                        Intent intent = new Intent(context, ProductActivity.class);
                        Constants.type="all";
                        Constants.mCategoryId=null;
                        Constants.mSubCatId=null;
                        ((AppCompatActivity)context).startActivityForResult(intent,205);


                    }
                    else {


                        if (mCallback != null) {


                            mCallback.onHandleResult(items.get(position).getCdItem());
                        }

                    }
                }
            });

            SharedPreferences sharedpreferences =context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
            RestService.RestApiInterface client = RestService.getClient();
            Call<JsonArray> callback = client.getWishListExistence(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),items.get(position).getCdItem());
            callback.enqueue(new Callback<JsonArray>() {
                @Override
                public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                    if(response.isSuccessful())
                    {
                        try{

                            JSONArray jsonArray=new JSONArray(response.body().toString());
                            JSONObject jsonObject=jsonArray.getJSONObject(0);
                            if(jsonObject.getInt("whishlist_occurance")==1)
                            {

                                items.get(position).setIsInWishlist(1);

                                holder.bookmark.setImageResource(R.drawable.ic_favorite_red);
                            }
                            else {

                                items.get(position).setIsInWishlist(0);

                                holder.bookmark.setImageResource(R.drawable.ic_favorite_border_red);
                            }

                            //notifyItemRangeChanged(0,items.size());


                        }catch (Exception e)
                        {

                        }
                    }
                    else {

                        Toast.makeText(context, "failure", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<JsonArray> call, Throwable t) {
                    t.printStackTrace();

                    Toast.makeText(context, "failure", Toast.LENGTH_SHORT).show();

                }
            });



        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public void setListner(cartListner listner) {
        this.listner = listner;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemPic)
        ImageView itemPic;
        @BindView(R.id.itemName)
        TextView itemName;
        @BindView(R.id.itemDesc)
        TextView itemDesc;
        @BindView(R.id.itemPrice)
        TextView itemPrice;
        @BindView(R.id.addbtn)
        LinearLayout addbtn;
        @BindView(R.id.bookmark)
        ImageView bookmark;
        @BindView(R.id.textnewprice)
        TextView textnewprice;
        @BindView(R.id.txtviewall)
        TextView txtviewall;

        @BindView(R.id.frame)
        FrameLayout frame;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void set(final Item item, final int position) {

            if (!item.getCdItem().equals("0")) {


                if (ApiConstants.isNonEmpty(item.getDsItem())) {
                    itemName.setText(item.getDsItem());
                }
                if (ApiConstants.isNonEmpty(item.getDsItemDescription())) {
                    itemDesc.setText(item.getDsItemDescription());
                }
                if (ApiConstants.isNonEmpty(item.getVlMrp())) {
                    itemPrice.setText(ApiConstants.amountFormatter(item.getVlMrp()));
                }
                if (ApiConstants.isNonEmpty(item.getVl_selling_price())) {
                    if (ApiConstants.isNonEmpty(item.getVlMrp())) {
                        if (!item.getVlMrp().equalsIgnoreCase(item.getVl_selling_price())) {

                            itemPrice.setPaintFlags(itemPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            textnewprice.setText(ApiConstants.amountFormatter(item.getVl_selling_price()));
                        }
                    }
                }
                if (ApiConstants.isNonEmpty(item.getDsItemPicSmall())) {
                    Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(item.getDsItemPic()).into(itemPic);
                }
                //UI setting code
                final Item item1 = item;
                Gson gson = new Gson();
                final String data = gson.toJson(item);

                addbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listner.OnaddCart(data);
                    }
                });
                bookmark.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        if (item1.getIsInWishlist() == 0) {
                            addToWishList(item1.getCdItem(), position);
                        } else {

                            removeWishList(item1.getCdItem(), position);
                        }
                    }
                });

                //  checkWishListExistence(item.getCdItem(),position);

                if (item.getIsInWishlist() == 1) {

                    bookmark.setImageResource(R.drawable.ic_favorite_red);
                } else {
                    bookmark.setImageResource(R.drawable.ic_favorite_border_red);
                }
            }
            else {

                frame.setVisibility(View.GONE);
                txtviewall.setVisibility(View.VISIBLE);
            }
        }
    }
    private void addToWishList(String cdItem, final int pos) {
        SharedPreferences sharedpreferences =context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);

        final ProgressDialog progressDialog = new ProgressDialog(context,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();



        Call<ResponseBody> callback = client.addWishList(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),cdItem,"1");
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject= new JSONObject(response.body().string());
                        if(jsonObject.getString("status").equalsIgnoreCase("1")){

                            items.get(pos).setIsInWishlist(0);
                            notifyItemRangeChanged(0,items.size());


                            Toast.makeText(context, "Item added to wishlist successfully", Toast.LENGTH_SHORT).show();

                        }else{

                            Toast.makeText(context, "Some thing went wrong.Please try after some time", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }
    public interface CallbackInterfaceResult{

        void onHandleResult( String text);
    }


//    public void checkWishListExistence(String cditem, final int position)
//    {
//
//    }


    private void removeWishList(String orderid, final int pos) {



        final ProgressDialog progressDialog = new ProgressDialog(context,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        SharedPreferences sharedpreferences =context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);



        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();



        Call<ResponseBody> callback = client.removeWishList(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),orderid);
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject= new JSONObject(response.body().string());
                        if(jsonObject.getString("status").equalsIgnoreCase("1")){
                            //items.remove(pos);

                            items.get(pos).setIsInWishlist(0);
                            notifyItemRangeChanged(0,items.size());

                            Toast.makeText(context, "item removed successfully", Toast.LENGTH_SHORT).show();

                        }else{

                            Toast.makeText(context, " ", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }
}