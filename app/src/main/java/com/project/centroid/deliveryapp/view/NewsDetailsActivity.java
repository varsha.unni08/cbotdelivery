package com.project.centroid.deliveryapp.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.data.remote.model.NewsItem;

public class NewsDetailsActivity extends AppCompatActivity {

    NewsItem item;

    TextView news_title_text,section_name_text;

    ImageView imgpic,imgback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        getSupportActionBar().hide();
             item=(NewsItem) getIntent().getSerializableExtra("News");
        imgback=findViewById(R.id.imgback);
        section_name_text=findViewById(R.id.section_name_text);
        news_title_text=findViewById(R.id.news_title_text);
        imgpic=findViewById(R.id.imgpic);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                onBackPressed();
            }
        });


        if (ApiConstants.isNonEmpty(item.getDsSource())) {
            news_title_text.setText("Souce : " + item.getDsSource());
        }

        if (ApiConstants.isNonEmpty(item.getDsNews())) {
            section_name_text.setText("" + item.getDsNews());
        }
        if (ApiConstants.isNonEmpty(item.getDsNewsImage())) {
            // holder.storyImage.setText(""+item.getDsNews());
            try {
                Glide.with(NewsDetailsActivity.this).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(item.getDsNewsImage()).into(imgpic);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }
}
