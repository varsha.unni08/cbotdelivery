package com.project.centroid.deliveryapp.view;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.MatrixCursor;
import android.os.Handler;
import android.provider.BaseColumns;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.Constants;
import com.project.centroid.deliveryapp.ViewItemSingle;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.data.remote.model.SearchSuggestions;
import com.project.centroid.deliveryapp.data.remote.model.SubCategory;
import com.project.centroid.deliveryapp.data.remote.model.UserItem;
import com.project.centroid.deliveryapp.data.remote.modelnew.MainCategory;
import com.project.centroid.deliveryapp.data.remote.modelnew.MainSubcategory;
import com.project.centroid.deliveryapp.data.remote.modelnew.Offer;
import com.project.centroid.deliveryapp.data.remote.modelnew.OfferItems;
import com.project.centroid.deliveryapp.data.remote.modelnew.ProductWithCategory;
import com.project.centroid.deliveryapp.reciever.NetworkChangeReceiver;
import com.project.centroid.deliveryapp.view.adapter_new.CategoryAdapter;
import com.project.centroid.deliveryapp.view.adapter_new.MainCategoryAdapter;
import com.project.centroid.deliveryapp.view.adapter_new.MainHomePagerAdapter;
import com.project.centroid.deliveryapp.view.adapter_new.ProductAdapter;
import com.project.centroid.deliveryapp.view.adapter_new.ProductCategoryAdapter;
import com.project.centroid.deliveryapp.view.progress.ProgressFragment;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainHomeActivity extends AppCompatActivity {

    ImageView imgmenu, imgcart, imgorders;
    DrawerLayout drawer;

    ViewPager viewPager;
    TabLayout tab_layout;
    Handler handler;
    int currentPage = 0;
    int NUM_PAGES = 5;
    Timer timer;
    Runnable Update;

    Dialog dialog;

    RecyclerView recyclerView_maincat, recycler_products;

    List<SearchSuggestions> searchSuggestions = new ArrayList<>();

    CursorAdapter mAdapter;
    TextView txtCartCount,nav_header_textView;
    SearchView searchView;

    View drawerview;
    NavigationView navigation;

    ImageView improfile,imgoffer;
    ProductCategoryAdapter productCategoryAdapter=null;

     List<ProductWithCategory>productWithCategories=new ArrayList<>();
     List<Offer>offers;


     String selected_catid="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_home);
        getSupportActionBar().hide();
        imgmenu = findViewById(R.id.imgmenu);
        drawer = findViewById(R.id.drawer);
        tab_layout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.viewPager);
        recyclerView_maincat = findViewById(R.id.recyclerView_maincat);
        recycler_products = findViewById(R.id.recycler_products);
        imgcart = findViewById(R.id.imgcart);
        txtCartCount = findViewById(R.id.txtCartCount);
        imgorders = findViewById(R.id.imgorders);
        searchView = findViewById(R.id.searchView);
        navigation = findViewById(R.id.navigation);
        imgoffer=findViewById(R.id.imgoffer);

        handler = new Handler();
        offers=new ArrayList<>();

        //setupTab();
        //setupCategory();
        //showAllData();

        showOfferSlider();
        setupCartCount();
        setupSearchViewAdapter();
        setupProfile();

        drawerview=navigation.getHeaderView(0);
        improfile=findViewById(R.id.improfile);
        nav_header_textView=findViewById(R.id.nav_header_textView);


        imgorders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainHomeActivity.this, OrdersActivity.class));

            }
        });

        improfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainHomeActivity.this, UserProfileActivity.class));

            }
        });

        imgoffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainHomeActivity.this, OffersActivity.class));

            }
        });


        nav_header_textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainHomeActivity.this, UserProfileActivity.class));

            }
        });


        imgmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer.openDrawer(Gravity.START);

            }
        });


        imgcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainHomeActivity.this, CartActivity.class));


            }
        });


        txtCartCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainHomeActivity.this, CartActivity.class));

            }
        });
    }






    public void getImage()
    {
        final SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<JsonArray>jsonArrayCall=client.getImage(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""));


        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                //   progressDialog.dismiss();

                if(response.isSuccessful())
                {

                    try{

                        JSONArray jsonArray=new JSONArray(response.body().toString());

                        if(jsonArray.length()>0)
                        {

                            JSONObject jsonObject=jsonArray.getJSONObject(0);

                            if(jsonObject.getInt("status")==1)
                            {

                                // Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();


                              String  url=jsonObject.getString("message");



                                GlideUrl glideUrl=  new GlideUrl(url, new LazyHeaders.Builder()
                                        .addHeader("Authorization", ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""))
                                        .build());



                                //  Glide.with(getActivity()).load(jsonObject.getString("message")).apply(RequestOptions.circleCropTransform()).into(profile);

                                Glide.with(MainHomeActivity.this).applyDefaultRequestOptions(new RequestOptions()).load(glideUrl).apply(RequestOptions.circleCropTransform()).into(improfile);

                            }
                            else {

                                //  Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                            }


                        }


                    }catch (Exception e)
                    {

                    }




                }
                else {

                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {

                // progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }




    private void setupProfile() {
        final Context context = MainHomeActivity.this;


        SharedPreferences sharedpreferences = context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);

        RestService.RestApiInterface client = RestService.getClient();


        Call<List<UserItem>> callback = client.getUser(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""));
        callback.enqueue(new Callback<List<UserItem>>() {
            @Override
            public void onResponse(Call<List<UserItem>> call, Response<List<UserItem>> response) {


                if (response.isSuccessful()) {
//                    progressDialog.dismiss();
//                    progressDialog.hide();
                    try {
                        if (response.body() != null) {
                            List<UserItem> array = response.body();
                            UserItem userItem = array.get(0);
                            if (!TextUtils.isEmpty(userItem.getDsCustomer())) {
                                nav_header_textView.setText( userItem.getDsCustomer());
                            }

                            getImage();
//                            if (!TextUtils.isEmpty(userItem.getDsAddress())) {
//                                mobile.setText(userItem.getDsPhoneno());
//                            }
//                            if (!TextUtils.isEmpty(userItem.getDsEmail())) {
//                                email.setText(  userItem.getDsEmail());
//                            }
//                            if (!TextUtils.isEmpty(userItem.getDsDeliveryAddress())) {
//                                dob.setText(userItem.getDsDeliveryAddress() + "\n" + userItem.getDsPincode());
//                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<List<UserItem>> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }











    private void setupSearchViewAdapter() {
        final String[] from = new String[]{"cityName"};
        final int[] to = new int[]{R.id.txt};
        mAdapter = new SimpleCursorAdapter(MainHomeActivity.this,
                R.layout.layout_listitem,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        searchView.setSuggestionsAdapter(mAdapter);


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                                              @Override
                                              public boolean onQueryTextSubmit(String query) {
                                                  searchView.clearFocus();

                                                  return true;
                                              }

                                              @Override
                                              public boolean onQueryTextChange(String newText) {

                                                  getSearchSuggestions(newText);
                                                  return false;
                                              }
                                          }

        );


        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int i) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int i) {

                SearchSuggestions searchSuggest = searchSuggestions.get(i);
                String id = searchSuggest.getCdItem();
                Intent intent = new Intent(MainHomeActivity.this, ViewItemSingle.class);
                intent.putExtra("id", id);
                startActivity(intent);


                return false;
            }
        });
    }


    public void getSearchSuggestions(String txt) {
        RestService.RestApiInterface client = RestService.getClient();

        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        Call<List<SearchSuggestions>> jsonArrayCall = client.getSearchSuggestios(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), txt);

        jsonArrayCall.enqueue(new Callback<List<SearchSuggestions>>() {
            @Override
            public void onResponse(Call<List<SearchSuggestions>> call, Response<List<SearchSuggestions>> response) {

                if (response.isSuccessful()) {

                    try {

                        //JSONArray jsonArray=new JSONArray(response.body().toString());

                        if (response.body().size() > 0) {

                            searchSuggestions.clear();

                            searchSuggestions.addAll(response.body());

                            final MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "cityName"});
                            for (int i = 0; i < response.body().size(); i++) {
                                // if (SUGGESTIONS[i].toLowerCase().startsWith(query.toLowerCase()))
                                c.addRow(new Object[]{i, response.body().get(i).getDsItem()});
                            }
                            mAdapter.changeCursor(c);

                        }


                    } catch (Exception e) {

                    }


                }


            }

            @Override
            public void onFailure(Call<List<SearchSuggestions>> call, Throwable t) {

                t.printStackTrace();
            }
        });

    }



    public void showOfferSlider()
    {
        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"jdfm");

        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        RestService.RestApiInterface client = RestService.getClient();
        Call<OfferItems> callBack = client.getOfferlist(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""));

        callBack.enqueue(new Callback<OfferItems>() {
            @Override
            public void onResponse(Call<OfferItems> call, Response<OfferItems> response) {

                progressFragment.dismiss();


                    if (response.body() != null) {

                        if(response.body().getData().size()>0) {


                            offers.addAll(response.body().getData());


                            setupCategory();
                        }


                    }



            }

            @Override
            public void onFailure(Call<OfferItems> call, Throwable t) {

                progressFragment.dismiss();

            }
        });



        //        callBack.enqueue(new Callback<List<Offer>>() {
//            @Override
//            public void onResponse(Call<List<Offer>> call, final Response<List<Offer>> response) {
//
//                progressFragment.dismiss();
//
//                if (!response.body().isEmpty()) {
//                    if (response.body() != null) {
//
//
//
//
//
//
//                    }
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<Offer>> call, Throwable t) {
//                progressFragment.dismiss();
//
//            }
    }


    public void setupCategory() {
       // String arr_cat[] = new String[]{"Textitles", "Grocery", "Hardwares", "Electronics", "Home Appliances", "Plastics"};

      final   List<MainCategory> mainCategories = new ArrayList<>();
//        for (int i = 0; i < arr_cat.length; i++) {
//            MainCategory mainCategory = new MainCategory();
//            mainCategory.setData(arr_cat[i]);
//            mainCategories.add(mainCategory);
//
//        }

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"");






        RestService.RestApiInterface client = RestService.getClient();

        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        Call<List<MainCategory>> jsonArrayCall = client.get_maincategories(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""));

     jsonArrayCall.enqueue(new Callback<List<MainCategory>>() {
         @Override
         public void onResponse(Call<List<MainCategory>> call, Response<List<MainCategory>> response) {
progressFragment.dismiss();

             if (response.isSuccessful())
             {
                 if(response.body().size()>0)
                 {

                     if(response.body().get(0).getStatus().equals("1")) {

                         mainCategories.addAll(response.body());

                         MainCategoryAdapter mainCategoryAdapter = new MainCategoryAdapter(MainHomeActivity.this, mainCategories);
                         recyclerView_maincat.setLayoutManager(new LinearLayoutManager(MainHomeActivity.this));
                         recyclerView_maincat.setAdapter(mainCategoryAdapter);
                     }

                     showAllData(mainCategories);

                 }




             }






         }

         @Override
         public void onFailure(Call<List<MainCategory>> call, Throwable t) {

             progressFragment.dismiss();

             if(!NetworkChangeReceiver.isConnected())
             {
                 Toast.makeText(MainHomeActivity.this,"Please check the internet connection",Toast.LENGTH_SHORT).show();
             }



         }
     });




    }

    @Override
    protected void onRestart() {
        super.onRestart();

        setupCartCount();
    }


    public void setupCartCount() {
        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        String token = ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, "");

        Log.e("Token forcount", token);

        RestService.RestApiInterface client = RestService.getClient();
        Call<JsonObject> callback = client.getCartCount(token);

        callback.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    try {

                        JSONObject jsonArray = new JSONObject(response.body().toString());


                        if (jsonArray.getInt("count") == 0) {
                            txtCartCount.setVisibility(View.GONE);
                        } else {

                            // JSONObject jsonObject=jsonArray.getJSONObject(0);

                            txtCartCount.setVisibility(View.VISIBLE);

                            if (jsonArray.getInt("count") > 9) {
                                txtCartCount.setText("9+");
                            } else {

                                txtCartCount.setText("" + jsonArray.getInt("count"));
                            }


                        }


                        // notifyItemRangeChanged(0,items.size());


                    } catch (Exception e) {

                    }
                } else {

                    Toast.makeText(MainHomeActivity.this, "failure", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                t.printStackTrace();

                Toast.makeText(MainHomeActivity.this, "failure", Toast.LENGTH_SHORT).show();

            }
        });
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (handler != null) {
            handler.removeCallbacks(Update);
        }
    }





    public void showSubCatList(String catId) {

        this.selected_catid=catId;

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"jdfm");


        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);



        final String authToken = ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, "");
        RestService.RestApiInterface service = RestService.getClient();
        Call<List<SubCategory>> callBack = service.getSubcategoryList(authToken, catId);
        callBack.enqueue(new Callback<List<SubCategory>>() {
            @Override
            public void onResponse(Call<List<SubCategory>> call, Response<List<SubCategory>> response) {
                try {

                    progressFragment.dismiss();
                    if(response.isSuccessful()) {


                        List<SubCategory> categories = new ArrayList<>();
                        // categories.add(new SubCategory("0", "Select"));
                        categories.addAll(response.body());


                        showCategoryDialog(categories);
                    }
                    else {
                        Toast.makeText(MainHomeActivity.this,"Failure",Toast.LENGTH_SHORT).show();
                    }

//                    ArrayAdapter<SubCategory> adapter = new ArrayAdapter<>(ProductActivity.this, R.layout.support_simple_spinner_dropdown_item, categories);
//                    spinnerSubCategory.setAdapter(adapter);
//
//                    if (Constants.mSubCatId != null && categories != null && categories.size() > 0) {
//                        for (int i = 0; i < categories.size(); i++) {
//                            if (categories.get(i).getCdSubcategory().equalsIgnoreCase(Constants.mSubCatId)) {
//                                spinnerSubCategory.setSelection(i);
//                            }
//
//                        }
//                    }


                } catch (Exception e) {
                    e.printStackTrace();

                }


            }

            @Override
            public void onFailure(Call<List<SubCategory>> call, Throwable t) {
                Toast.makeText(MainHomeActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();

                progressFragment.dismiss();
            }
        });
    }




    public void showCategoryDialog(final List<SubCategory> categories) {



        if(categories.get(0).getStatus()==null) {


            if (drawer.isDrawerOpen(Gravity.START)) {
                drawer.closeDrawer(Gravity.START);
            }


            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            dialog = new Dialog(MainHomeActivity.this);
            dialog.setContentView(R.layout.layout_searchdialog);
            dialog.setTitle("Village mart");
            dialog.getWindow().setLayout(width - 30, height - 60);
            EditText edtSearch = dialog.findViewById(R.id.edtSearch);
            final RecyclerView recyclerView = dialog.findViewById(R.id.recycler);

            recyclerView.setLayoutManager(new LinearLayoutManager(MainHomeActivity.this));
            recyclerView.setAdapter(new CategoryAdapter(MainHomeActivity.this,categories));

            edtSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(!charSequence.toString().equals("")){

                    List<SubCategory>data_search=new ArrayList<>();


                    String a=charSequence.toString();
                    for (SubCategory data:categories
                    ) {

                        if(data.getDsSubcategory().toLowerCase().contains(a)||data.getDsSubcategory().toUpperCase().contains(a))
                        {
                            data_search.add(data);

                        }

                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(MainHomeActivity.this));
                    recyclerView.setAdapter(new CategoryAdapter(MainHomeActivity.this,data_search));

                }
                else {

                    recyclerView.setLayoutManager(new LinearLayoutManager(MainHomeActivity.this));
                    recyclerView.setAdapter(new CategoryAdapter(MainHomeActivity.this,categories));


                }

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            dialog.show();
        }
        else {

            Toast.makeText(MainHomeActivity.this,"No data found",Toast.LENGTH_SHORT).show();
        }
    }

    public void showData(SubCategory subCategory) {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }

        int i=0;

        for (int j=0;j<productWithCategories.size();j++
             ) {

            if(productWithCategories.get(j).getCatid().equals(selected_catid))
            {
                i=j;

            }

        }

        Log.e("TAG",i+"");

        recycler_products.smoothScrollToPosition(i);



    }


    public void showAllData(List<MainCategory>mainCategories) {




        ProductWithCategory productWithCategory=new ProductWithCategory();
        productWithCategory.setCatid("0");
        productWithCategory.setCategorydata("0");
        productWithCategories.add(productWithCategory);





        if(mainCategories.size()>0) {


        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"jdfm");

        for (final MainCategory mainCategory:mainCategories
             ) {

            for (final MainSubcategory mainSubcategory : mainCategory.getSubcategories()) {


                SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


                RestService.RestApiInterface client = RestService.getClient();
                Call<List<Item>> callBack = client.getFilterCategorySubcategoryList(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), mainCategory.getCdMainCategory(), mainSubcategory.getCd_category(), "1");

                callBack.enqueue(new Callback<List<Item>>() {
                    @Override
                    public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                        try {
                            progressFragment.dismiss();

                            if (!response.body().isEmpty()) {
                                if (response.body() != null) {


                                    List<Item> posts = response.body();

                                    if(posts.size()>0)
                                    {
                                        ProductWithCategory productWithCategory=new ProductWithCategory();
                                       productWithCategory.setCategorydata(mainSubcategory.getDs_category());
                                       productWithCategory.setCatid(mainSubcategory.getCd_category());
                                       productWithCategory.setMaincatid(mainCategory.getCdMainCategory());
                                      productWithCategory.setMaincategory(mainCategory.getDsMainCategory());

                                       productWithCategory.setItems(posts);
                                        productWithCategories.add(productWithCategory);

                                        productCategoryAdapter.notifyDataSetChanged();



                                        //productWithCategories
                                    }




//                            if (posts.size() > 0) {
//                                Log.v("TAG", "newloading");
//                                recyclerAdapterProduct.setLoaded();
//                                productItems.remove(productItems.size() - 1);
//                                productItems.addAll(posts);
//                                recyclerAdapterProduct.notifyItemInserted(productItems.size());
//
//
//                            } else {
//
//                                Log.v("TAG", "loadingfalse");
//                                recyclerAdapterProduct.setLoadedFalse();
//                                pageCount = 1;
//                                allContactsLoaded = true;
//                                productItems.remove(productItems.size() - 1);
//                                recyclerAdapterProduct.notifyDataSetChanged();
//
//
//                            }
                                } else {
                                    Log.v("TAG", "loadingend");

//                            allContactsLoaded = true;
//                            productItems.remove(productItems.size() - 1);
//                            recyclerAdapterProduct.setLoadedFalse();
//                            recyclerAdapterProduct.notifyDataSetChanged();


                                }
                            } else {
//                        allContactsLoaded = true;
//                        productItems.remove(productItems.size() - 1);
//                        recyclerAdapterProduct.setLoadedFalse();
//                        recyclerAdapterProduct.notifyDataSetChanged();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Item>> call, Throwable t) {

                        progressFragment.dismiss();
                    }
                });

            }

        }



         productCategoryAdapter=new ProductCategoryAdapter(MainHomeActivity.this, productWithCategories,offers);



           recycler_products.setLayoutManager(new LinearLayoutManager(MainHomeActivity.this));
           recycler_products.setAdapter(productCategoryAdapter);

           productCategoryAdapter.notifyDataSetChanged();

        }

    }





}
