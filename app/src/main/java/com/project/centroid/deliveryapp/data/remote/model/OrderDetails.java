package com.project.centroid.deliveryapp.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetails {

    @SerializedName("Sales_Invoice")
    @Expose
    private String salesInvoice;
    @SerializedName("Order_Date")
    @Expose
    private String orderDate;
    @SerializedName("Delivery_Date")
    @Expose
    private String deliveryDate;
    @SerializedName("delivery_address")
    @Expose
    private String deliveryAddress;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;
    @SerializedName("cd_item")
    @Expose
    private String cdItem;
    @SerializedName("Item Name")
    @Expose
    private String itemName;
    @SerializedName("nr_qty")
    @Expose
    private String nrQty;
    @SerializedName("vl_net_total")
    @Expose
    private String vlNetTotal;


    public OrderDetails() {
    }

    public String getCdItem() {
        return cdItem;
    }

    public void setCdItem(String cdItem) {
        this.cdItem = cdItem;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getNrQty() {
        return nrQty;
    }

    public void setNrQty(String nrQty) {
        this.nrQty = nrQty;
    }

    public String getVlNetTotal() {
        return vlNetTotal;
    }

    public void setVlNetTotal(String vlNetTotal) {
        this.vlNetTotal = vlNetTotal;
    }

    public String getSalesInvoice() {
        return salesInvoice;
    }

    public void setSalesInvoice(String salesInvoice) {
        this.salesInvoice = salesInvoice;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
}
