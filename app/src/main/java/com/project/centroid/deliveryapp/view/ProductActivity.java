package com.project.centroid.deliveryapp.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.Constants;
import com.project.centroid.deliveryapp.Utils.GridSpacingItemDecoration;
import com.project.centroid.deliveryapp.Utils.LinearLayoutManagerWrapper;
import com.project.centroid.deliveryapp.ViewItemSingle;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Cart;
import com.project.centroid.deliveryapp.data.remote.model.Category;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.data.remote.model.SubCategory;
import com.project.centroid.deliveryapp.view.adapter.CategoryRecyclerAdapter;
import com.project.centroid.deliveryapp.view.adapter.RecyclerAdapterProduct;
import com.project.centroid.deliveryapp.view.interfaces.cartListner;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.project.centroid.deliveryapp.Utils.Constants.cartArray;
import static com.project.centroid.deliveryapp.Utils.Constants.mCartItemCount;

public class ProductActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.list_grid)
    ImageView listGrid;
    @BindView(R.id.filter)
    ImageView filter;
    List<Item> data;
    @BindView(R.id.filterlayout)
    CardView filterlayout;
    @BindView(R.id.filterText)
    TextView filterText;
    Spinner spinnerCategory, spinnerSubCategory;

    private SharedPreferences sharedpreferences;
    private CategoryRecyclerAdapter categoryRecyclerAdapter;
    private List<Item> productItems;
    private RecyclerAdapterProduct recyclerAdapterProduct;
    private Integer page;

    private TextView textCartItemCount;
    private boolean allContactsLoaded = false;
    private int pageCount = 1;
    private cartListner listner;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        ButterKnife.bind(this);
        sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initViews();
    }

    private void initViews() {
        try {
            if (Constants.type.equalsIgnoreCase("all")) {
                initRecyclerview();
            } else if (Constants.type.equalsIgnoreCase("search")) {
                String key = null;
                if (getIntent().getStringExtra("key") != null) {
                    key = getIntent().getStringExtra("key");
                    initRecyclerviewSearch(key);
                }

            } else {
                filter.setVisibility(View.GONE);
                filterText.setVisibility(View.GONE);
                initRecyclerview(Constants.mCategoryId, Constants.mSubCatId);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


  /*  private void getAllProduct(String type) {


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Item>> callback = client.getAll(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""));
        callback.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                try {
                    recyclerAdapterProduct = new ProductRecyclerAdapter(ProductActivity.this, response.body());
                    recyclerView.setLayoutManager(new GridLayoutManager(ProductActivity.this, 2));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setNestedScrollingEnabled(false);
                    int spanCount = 2; // 3 columns
                    int spacing = 10; // 50px
                    boolean includeEdge = false;
                    recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));

                    recyclerView.setAdapter(recyclerAdapterProduct);
                    recyclerView.setHasFixedSize(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {

            }
        });
    }*/


    private void initRecyclerview(final String catId, final String subCatId) {


        final ProgressDialog progressDialog = new ProgressDialog(ProductActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();


        LinearLayoutManager linearLayoutManager = new LinearLayoutManagerWrapper(ProductActivity.this, LinearLayoutManager.VERTICAL, false);

        data = new ArrayList<>();
        if (Constants.cartArray != null) {
            data = Constants.cartArray;
        }
        productItems = new ArrayList<>();
        recyclerAdapterProduct = new RecyclerAdapterProduct(ProductActivity.this, productItems, recyclerView);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        int spanCount = 2; // 3 columns
        int spacing = 10;

        //recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, false));

        recyclerView.setAdapter(recyclerAdapterProduct);
        listner = new cartListner() {
            @Override
            public void OnaddCart(String cdItem) {
                if (cdItem != null) {
                    Log.v("TAG", "v" + cdItem);

                    if (Constants.cartArray == null) {
                        Cart c = new Cart();
                        c.setCdItem(cdItem);
                        c.setDate(Constants.getdate());
                        c.setQuantity("1");
                        Gson gson = new Gson();
                        Item item = gson.fromJson(cdItem, Item.class);
                        data.add(item);
                        Constants.cartArray = data;
                        // Log.v("TAG", "v" + Constants.cartArray);
                        Constants.mCartItemCount = Constants.cartArray.size();
                        Toast.makeText(ProductActivity.this, "Item  added to cart", Toast.LENGTH_SHORT).show();


                    } else {
                        Gson gson = new Gson();
                        Item item = gson.fromJson(cdItem, Item.class);
                        // Log.v("TAG", cdItem + "v" + Constants.cartArray);
                        if (Constants.cartArray != null) {
                            if (Constants.isItemPresent(item.getDsItemCode(), Constants.cartArray)) {
                                Toast.makeText(ProductActivity.this, "Item already added to cart", Toast.LENGTH_SHORT).show();
                            } else {
                                Cart c = new Cart();
                                c.setCdItem(cdItem);
                                c.setDate(Constants.getdate());
                                c.setQuantity("1");

                                data.add(item);

                                Constants.cartArray = data;
                                Constants.mCartItemCount = Constants.cartArray.size();
                                Toast.makeText(ProductActivity.this, "Item  added to cart", Toast.LENGTH_SHORT).show();

                            }
                        }

                    }

                }
                addCart();

            }


            @Override
            public void OnRemoveCart() {

            }
        };
        recyclerAdapterProduct.setListner(listner);

        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Item>> callBack = client.getFilterCategorySubcategoryList(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), catId, subCatId, "1");

        callBack.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                progressDialog.dismiss();

                try {
                    Log.v("Tag", "v");
                    if (response.body() != null) {
                        Log.v("Tag1", "v");
                        List<Item> posts = response.body();

                        if (posts.size() > 0) {
                            productItems.addAll(posts);
                            recyclerAdapterProduct.notifyDataSetChanged();

                            if (!allContactsLoaded) {
                                try {
                                    loadMore(catId, subCatId);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                        } else {


                        }

                    } else {


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                Log.v("Tag2", "v");

                progressDialog.dismiss();
            }



        });

    }

    private void initRecyclerview() {
        if (cartArray != null) {
            Log.v("TAG", "v" + Constants.cartArray.size());
        }


        productItems = new ArrayList<>();
        data = new ArrayList<>();
        if (Constants.cartArray != null) {
            data = Constants.cartArray;
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManagerWrapper(ProductActivity.this, LinearLayoutManager.VERTICAL, false);


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        listner = new cartListner() {
            @Override
            public void OnaddCart(String cdItem) {
                if (cdItem != null) {
                    Log.v("TAG", "v" + cdItem);


                    if (Constants.cartArray == null) {
                        Cart c = new Cart();
                        c.setCdItem(cdItem);
                        c.setDate(Constants.getdate());
                        c.setQuantity("1");
                        Gson gson = new Gson();
                        Item item = gson.fromJson(cdItem, Item.class);
                        data.add(item);
                        Constants.cartArray = data;

                        Constants.mCartItemCount = Constants.cartArray.size();
                        //Log.v("TAG", "v" + Constants.cartArray);

                        Toast.makeText(ProductActivity.this, "Item  added to cart", Toast.LENGTH_SHORT).show();


                    } else {
                        Log.v("TAG", cdItem + "v" + Constants.cartArray);
                        if (Constants.cartArray != null) {
                            Item item = null;
                            Gson gson = new Gson();
                            try {
                                item = gson.fromJson(cdItem, Item.class);

                                Log.v("TAG2", "v" + Constants.cartArray);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (Constants.isItemPresent(item.getDsItemCode(), Constants.cartArray)) {
                                Toast.makeText(ProductActivity.this, "Item already added to cart", Toast.LENGTH_SHORT).show();
                            } else {
                                Cart c = new Cart();
                                c.setCdItem(cdItem);
                                c.setDate(Constants.getdate());
                                c.setQuantity("1");

                                data.add(item);

                                Constants.cartArray = data;
                                Constants.mCartItemCount = Constants.cartArray.size();
                                Toast.makeText(ProductActivity.this, "Item  added to cart", Toast.LENGTH_SHORT).show();

                            }
                        }

                    }

                }
                addCart();

            }


            @Override
            public void OnRemoveCart() {

            }
        };


        int spanCount = 2; // 3 columns
        int spacing = 10;

        //recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, false));


        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Item>> callBack = client.getAll(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "1");

        callBack.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                try {

                    Log.v("Tag", "v");
                    if (response.body() != null) {

                        List<Item> posts = response.body();

                        if (posts.size() > 0) {
                            Log.v("Tag1", "v");

                            productItems.addAll(posts);
                            recyclerAdapterProduct = new RecyclerAdapterProduct(ProductActivity.this, productItems, recyclerView);

                            recyclerAdapterProduct.setListner(listner);
                            recyclerView.setAdapter(recyclerAdapterProduct);


                            int spanCount = 2; // 3 columns
                            int spacing = 10;
                          //  recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, false));

                            if (!allContactsLoaded) {
                                try {
                                    Log.v("Tag ", "loadmore");
                                    loadMore();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                        } else {


                        }

                    } else {


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                Log.v("Tag2", "v");
            }
        });

    }

    private void loadMore() {
        Log.v("TAG", "load");
        recyclerAdapterProduct.setOnLoadMoreListener(new RecyclerAdapterProduct.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.v("TAG", "loading");

                productItems.add(null);
                recyclerView.post(new Runnable() {
                    public void run() {
                        recyclerAdapterProduct.notifyItemInserted(productItems.size() - 1);
                    }
                });
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //remove progress item
                     /*   if(newsItems.size()>0){
                        ////newsItems.remove(newsItems.size() - 1);
                        recyclerAdapterProduct.notifyItemInserted(newsItems.size());}
                     */   //add items one by one
/*                        for (int i = 0; i < 15; i++) {
                            myDataset.add("Item" + (myDataset.size() + 1));
                            mAdapter.notifyItemInserted(myDataset.size());

                        }*/

                        if (!allContactsLoaded) {
                            Log.v("TAG", "vit"+allContactsLoaded);
                            pageCount++;

                            try {
                                getMoreData(pageCount);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //recyclerAdapterProduct.setLoaded();

                        }
                        //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                    }
                }, 2000);
                //System.out.println("load");
            }
        });

    }

    private void getMoreData(final int page) {
        RestService.RestApiInterface client = RestService.getClient();
        Call<List<Item>> callBack = client.getAll(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), String.valueOf(page));

        callBack.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                try {

                    if (!response.body().isEmpty()) {
                        if (response.body() != null) {


                            List<Item> posts = response.body();
                            if (posts.size() > 0) {
                                Log.v("TAG", "newloading");
                                recyclerAdapterProduct.setLoaded();
                                productItems.remove(productItems.size() - 1);
                                productItems.addAll(posts);
                                recyclerAdapterProduct.notifyItemInserted(productItems.size());


                            } else {

                                Log.v("TAG", "loadingfalse");
                                recyclerAdapterProduct.setLoadedFalse();
                                pageCount = 1;
                                allContactsLoaded = true;
                                productItems.remove(productItems.size() - 1);
                                recyclerAdapterProduct.notifyDataSetChanged();


                            }
                        } else {
                            Log.v("TAG", "loadingend");

                            allContactsLoaded = true;
                            productItems.remove(productItems.size() - 1);
                            recyclerAdapterProduct.setLoadedFalse();
                            recyclerAdapterProduct.notifyDataSetChanged();


                        }
                    } else {
                        allContactsLoaded = true;
                        productItems.remove(productItems.size() - 1);
                        recyclerAdapterProduct.setLoadedFalse();
                        recyclerAdapterProduct.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {

            }
        });


    }


    //setSearchView();


    private void loadMore(final String catId, final String subcat) {
        recyclerAdapterProduct.setOnLoadMoreListener(new RecyclerAdapterProduct.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.v("TAG", "loadmore");

                productItems.add(null);
                recyclerView.post(new Runnable() {
                    public void run() {
                        recyclerAdapterProduct.notifyItemInserted(productItems.size() - 1);
                    }
                });
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //remove progress item
                     /*   if(newsItems.size()>0){
                        ////newsItems.remove(newsItems.size() - 1);
                        recyclerAdapterProduct.notifyItemInserted(newsItems.size());}
                     */   //add items one by one
/*                        for (int i = 0; i < 15; i++) {
                            myDataset.add("Item" + (myDataset.size() + 1));
                            mAdapter.notifyItemInserted(myDataset.size());

                        }*/
                        Log.v("TAG", "loading");
                        if (!allContactsLoaded) {

                            pageCount++;

                            try {
                                getMoreData(pageCount, catId, subcat);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //recyclerAdapterProduct.setLoaded();

                        }
                        //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                    }
                }, 2000);
                //System.out.println("load");
            }
        });

    }


    private void getMoreData(final int page, String catId, String subCat) {
        RestService.RestApiInterface client = RestService.getClient();
        Call<List<Item>> callBack = client.getFilterCategorySubcategoryList(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), catId, subCat, String.valueOf(page));

        callBack.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                try {

                    if (!response.body().isEmpty()) {
                        if (response.body() != null) {


                            List<Item> posts = response.body();
                            if (posts.size() > 0) {
                                Log.v("TAG", "newloading");
                                recyclerAdapterProduct.setLoaded();
                                productItems.remove(productItems.size() - 1);
                                productItems.addAll(posts);
                                recyclerAdapterProduct.notifyItemInserted(productItems.size());


                            } else {

                                Log.v("TAG", "loadingfalse");
                                recyclerAdapterProduct.setLoadedFalse();
                                pageCount = 1;
                                allContactsLoaded = true;
                                productItems.remove(productItems.size() - 1);
                                recyclerAdapterProduct.notifyDataSetChanged();


                            }
                        } else {
                            Log.v("TAG", "loadingend");

                            allContactsLoaded = true;
                            productItems.remove(productItems.size() - 1);
                            recyclerAdapterProduct.setLoadedFalse();
                            recyclerAdapterProduct.notifyDataSetChanged();


                        }
                    } else {
                        allContactsLoaded = true;
                        productItems.remove(productItems.size() - 1);
                        recyclerAdapterProduct.setLoadedFalse();
                        recyclerAdapterProduct.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {

            }
        });


    }

    private void initRecyclerviewSearch(final String key) {
        if (cartArray != null) {
            Log.v("TAG", "v" + Constants.cartArray.size());
        }


        productItems = new ArrayList<>();
        data = new ArrayList<>();
        if (Constants.cartArray != null) {
            data = Constants.cartArray;
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManagerWrapper(ProductActivity.this, LinearLayoutManager.VERTICAL, false);

        recyclerAdapterProduct = new RecyclerAdapterProduct(ProductActivity.this, productItems, recyclerView);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        listner = new cartListner() {
            @Override
            public void OnaddCart(String cdItem) {
                if (cdItem != null) {
                    Log.v("TAG", "v" + cdItem);


                    if (Constants.cartArray == null) {
                        Cart c = new Cart();
                        c.setCdItem(cdItem);
                        c.setDate(Constants.getdate());
                        c.setQuantity("1");
                        Gson gson = new Gson();
                        Item item = gson.fromJson(cdItem, Item.class);
                        data.add(item);
                        Constants.cartArray = data;

                        Constants.mCartItemCount = Constants.cartArray.size();
                        //Log.v("TAG", "v" + Constants.cartArray);

                        Toast.makeText(ProductActivity.this, "Item  added to cart", Toast.LENGTH_SHORT).show();


                    } else {
                        Log.v("TAG", cdItem + "v" + Constants.cartArray);
                        if (Constants.cartArray != null) {
                            Item item = null;
                            Gson gson = new Gson();
                            try {
                                item = gson.fromJson(cdItem, Item.class);

                                Log.v("TAG2", "v" + Constants.cartArray);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (Constants.isItemPresent(item.getDsItemCode(), Constants.cartArray)) {
                                Toast.makeText(ProductActivity.this, "Item already added to cart", Toast.LENGTH_SHORT).show();
                            } else {
                                Cart c = new Cart();
                                c.setCdItem(cdItem);
                                c.setDate(Constants.getdate());
                                c.setQuantity("1");

                                data.add(item);

                                Constants.cartArray = data;
                                Constants.mCartItemCount = Constants.cartArray.size();
                                Toast.makeText(ProductActivity.this, "Item  added to cart", Toast.LENGTH_SHORT).show();

                            }
                        }

                    }

                }
                addCart();

            }


            @Override
            public void OnRemoveCart() {

            }
        };

        recyclerAdapterProduct.setListner(listner);

        int spanCount = 2; // 3 columns
        int spacing = 10;

        //recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, false));

        recyclerView.setAdapter(recyclerAdapterProduct);
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Item>> callBack = client.searchProduct(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), key, "1");

        callBack.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                try {

                    Log.v("Tag", "v");
                    if (response.body() != null) {

                        List<Item> posts = response.body();

                        if (posts.size() > 0) {
                            Log.v("Tag1", "v");

                            productItems.addAll(posts);
                            recyclerAdapterProduct.notifyDataSetChanged();


                            int spanCount = 2; // 3 columns
                            int spacing = 10;
                            recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, false));

                            if (!allContactsLoaded) {
                                try {
                                    loadMoreSearch(key);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                        } else {


                        }

                    } else {


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                Log.v("Tag2", "v");
            }
        });

    }

    private void loadMoreSearch(final String key) {
        recyclerAdapterProduct.setOnLoadMoreListener(new RecyclerAdapterProduct.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.v("TAG", "loadmore");

                productItems.add(null);
                recyclerView.post(new Runnable() {
                    public void run() {
                        recyclerAdapterProduct.notifyItemInserted(productItems.size() - 1);
                    }
                });
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //remove progress item
                     /*   if(newsItems.size()>0){
                        ////newsItems.remove(newsItems.size() - 1);
                        recyclerAdapterProduct.notifyItemInserted(newsItems.size());}
                     */   //add items one by one
/*                        for (int i = 0; i < 15; i++) {
                            myDataset.add("Item" + (myDataset.size() + 1));
                            mAdapter.notifyItemInserted(myDataset.size());

                        }*/
                        Log.v("TAG", "loading");
                        if (!allContactsLoaded) {

                            pageCount++;

                            try {
                                getMoreDataSearch(pageCount, key);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //recyclerAdapterProduct.setLoaded();

                        }
                        //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                    }
                }, 2000);
                //System.out.println("load");
            }
        });

    }

    private void getMoreDataSearch(final int page, String Key) {
        RestService.RestApiInterface client = RestService.getClient();
        Call<List<Item>> callBack = client.getAll(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), String.valueOf(page));

        callBack.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                try {

                    if (!response.body().isEmpty()) {
                        if (response.body() != null) {


                            List<Item> posts = response.body();
                            if (posts.size() > 0) {
                                Log.v("TAG", "newloading");
                                recyclerAdapterProduct.setLoaded();
                                productItems.remove(productItems.size() - 1);
                                productItems.addAll(posts);
                                recyclerAdapterProduct.notifyItemInserted(productItems.size());


                            } else {

                                Log.v("TAG", "loadingfalse");
                                recyclerAdapterProduct.setLoadedFalse();
                                pageCount = 1;
                                allContactsLoaded = true;
                                productItems.remove(productItems.size() - 1);
                                recyclerAdapterProduct.notifyDataSetChanged();


                            }
                        } else {
                            Log.v("TAG", "loadingend");

                            allContactsLoaded = true;
                            productItems.remove(productItems.size() - 1);
                            recyclerAdapterProduct.setLoadedFalse();
                            recyclerAdapterProduct.notifyDataSetChanged();


                        }
                    } else {
                        allContactsLoaded = true;
                        productItems.remove(productItems.size() - 1);
                        recyclerAdapterProduct.setLoadedFalse();
                        recyclerAdapterProduct.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {

            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart_menu_p, menu);

        final MenuItem menuItem = menu.findItem(R.id.action_cart);

        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        setupBadge();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_cart:
                // Do something
                Intent intent = new Intent(this, CartActivity.class);
                startActivityForResult(intent, 123);
               break;

               case android.R.id.home:
                    Intent intent1=getIntent();
                    setResult(Activity.RESULT_OK, intent1);
                    finish();

                  break;



        }
        return super.onOptionsItemSelected(item);
    }

    private void setupBadge() {


        if (textCartItemCount != null) {


            SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
            String token=ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, "");

            Log.e("Token forcount",token);

            RestService.RestApiInterface client = RestService.getClient();
            Call<JsonObject> callback = client.getCartCount(token);

            callback.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if(response.isSuccessful())
                    {
                        try{

                            JSONObject jsonArray=new JSONObject(response.body().toString());


                            if(jsonArray.getInt("count")==0)
                            {
                                textCartItemCount.setVisibility(View.GONE);
                            }
                            else {

                                // JSONObject jsonObject=jsonArray.getJSONObject(0);

                                textCartItemCount.setVisibility(View.VISIBLE);

                                textCartItemCount.setText(""+jsonArray.getInt("count"));
                            }


                            // notifyItemRangeChanged(0,items.size());


                        }catch (Exception e)
                        {

                        }
                    }
                    else {

                        Toast.makeText(ProductActivity.this, "failure", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    t.printStackTrace();

                    Toast.makeText(ProductActivity.this, "failure", Toast.LENGTH_SHORT).show();

                }
            });






//            if (Constants.cartArray == null) {
//                textCartItemCount.setText(""+0);
//            }
//
//            if (mCartItemCount != null && mCartItemCount == 0) {
//
//                if (textCartItemCount.getVisibility() != View.GONE) {
//                    textCartItemCount.setText(String.valueOf(Constants.mCartItemCount));
//                    // textCartItemCount.setVisibility(View.VISIBLE);
//                }
//            } else {
//
//                if (Constants.cartArray != null) {
//                    Constants.mCartItemCount = Constants.cartArray.size();
//                } else {
//                    textCartItemCount.setText(String.valueOf(0));
//                }
//                textCartItemCount.setText(String.valueOf(Constants.mCartItemCount));
//                if (Constants.cartArray == null) {
//                    textCartItemCount.setText(""+0);
//                }
//                if (textCartItemCount.getVisibility() != View.VISIBLE) {
//                    textCartItemCount.setVisibility(View.VISIBLE);
//                }
//            }
        }
    }

    @OnClick(R.id.list_grid)
    public void ViewFlipper() {
        try {
            boolean isSwitched = recyclerAdapterProduct.toggleItemViewType();
            if (!isSwitched) {
                listGrid.setImageDrawable(getResources().getDrawable(R.drawable.ic_list_gray));
            } else {
                listGrid.setImageDrawable(getResources().getDrawable(R.drawable.ic_view_grid));

            }
            recyclerView.setLayoutManager(isSwitched ? new LinearLayoutManager(this) : new GridLayoutManager(this, 2));
            recyclerAdapterProduct.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addCart() {
     //   setupBadge();

    }

    private void categoryCall() {


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Category>> callback = client.getAllCategories(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""));
        callback.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, final Response<List<Category>> response) {

                try {

                    List<Category> categories = new ArrayList<>();
                    categories.add(new Category("0", "Select", "Select"));
                    categories.addAll(response.body());

                    ArrayAdapter<Category> adapter = new ArrayAdapter<>(ProductActivity.this, R.layout.support_simple_spinner_dropdown_item, categories);
                    spinnerCategory.setAdapter(adapter);
                    spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (spinnerCategory.getSelectedItemPosition() > 0) {
                                Category c = (Category) spinnerCategory.getSelectedItem();
                                if (!TextUtils.isEmpty(c.getCdCategory())) {
                                    showSubCatList(c.getCdCategory());
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                    if (Constants.mCategoryId != null && categories != null && categories.size() > 0) {
                        for (int i = 0; i < categories.size(); i++) {
                            if (categories.get(i).getCdCategory().equalsIgnoreCase(Constants.mCategoryId)) {
                                spinnerCategory.setSelection(i);
                            }

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {

            }
        });
    }

    private void showDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        final View view = layoutInflater.inflate(R.layout.layout_filterdialog, null, false);
        final AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        spinnerCategory = (Spinner) view.findViewById(R.id.spinnerCategory);
        spinnerSubCategory = (Spinner) view.findViewById(R.id.spinnerSubCategory);
        LinearLayout filter = (LinearLayout) view.findViewById(R.id.filterBtn);
        LinearLayout clear = (LinearLayout) view.findViewById(R.id.clear);

        pictureDialog.setTitle("Filter");
        pictureDialog.setView(view);
        pictureDialog.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        categoryCall();

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (spinnerCategory.getSelectedItemPosition() != 0) {
                        Constants.mCategoryId = ((Category) spinnerCategory.getSelectedItem()).getCdCategory();
                    } else {
                        Constants.mCategoryId = null;
                    }
                    if (spinnerSubCategory.getSelectedItemPosition() != 0) {
                        Constants.mSubCatId = ((SubCategory) spinnerSubCategory.getSelectedItem()).getCdSubcategory();
                    } else {
                        Constants.mSubCatId = null;
                    }

                    initRecyclerview(Constants.mCategoryId, Constants.mSubCatId);
                    dialog.dismiss();
                    dialog.cancel();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Constants.mCategoryId = null;
                Constants.mSubCatId = null;
                Constants.type = "all";
                initViews();
                dialog.dismiss();
                dialog.cancel();

            }
        });


        dialog = pictureDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
    }

    private void showSubCatList(String catId) {
        final String authToken = ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, "");
        RestService.RestApiInterface service = RestService.getClient();
        Call<List<SubCategory>> callBack = service.getSubcategoryList(authToken, catId);
        callBack.enqueue(new Callback<List<SubCategory>>() {
            @Override
            public void onResponse(Call<List<SubCategory>> call, Response<List<SubCategory>> response) {
                try {
                    List<SubCategory> categories = new ArrayList<>();
                    categories.add(new SubCategory("0", "Select"));
                    categories.addAll(response.body());

                    ArrayAdapter<SubCategory> adapter = new ArrayAdapter<>(ProductActivity.this, R.layout.support_simple_spinner_dropdown_item, categories);
                    spinnerSubCategory.setAdapter(adapter);

                    if (Constants.mSubCatId != null && categories != null && categories.size() > 0) {
                        for (int i = 0; i < categories.size(); i++) {
                            if (categories.get(i).getCdSubcategory().equalsIgnoreCase(Constants.mSubCatId)) {
                                spinnerSubCategory.setSelection(i);
                            }

                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();

                }


            }

            @Override
            public void onFailure(Call<List<SubCategory>> call, Throwable t) {
                Toast.makeText(ProductActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();

            }
        });
    }


    @OnClick(R.id.filter)
    public void onViewClicked() {
        try {
            showDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();

        setupBadge();
    }
}
