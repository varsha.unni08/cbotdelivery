package com.project.centroid.deliveryapp.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.Constants;
import com.project.centroid.deliveryapp.Utils.GridSpacingItemDecoration;
import com.project.centroid.deliveryapp.Utils.LinearLayoutManagerWrapper;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Cart;
import com.project.centroid.deliveryapp.data.remote.model.    NewsItem;
import com.project.centroid.deliveryapp.data.remote.model.NewsItem;

import com.project.centroid.deliveryapp.view.adapter.RecyclerViewNews;
import com.project.centroid.deliveryapp.view.interfaces.cartListner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.project.centroid.deliveryapp.Utils.Constants.cartArray;

public class NewsViewActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private RecyclerViewNews recyclerAdapter;
    private ArrayList<NewsItem> newsItems;
    private SharedPreferences sharedpreferences;

    @BindView(R.id.layoutProgress)
    LinearLayout layoutProgress;

    int currentpage=1;

    boolean isend=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_view);
        ButterKnife.bind(this);
        newsItems= new ArrayList<>();
        getSupportActionBar().setHomeButtonEnabled(true);
        sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
      initRecyclerView();

    }


    public void loadmore()
    {

        if(!isend) {
            layoutProgress.setVisibility(View.VISIBLE);
            currentpage++;
          initRecyclerView();
        }
        else {
            layoutProgress.setVisibility(View.GONE);
        }
    }

    private void initRecyclerView() {




       
        LinearLayoutManager linearLayoutManager = new LinearLayoutManagerWrapper(this, LinearLayoutManager.VERTICAL, false);

        recyclerAdapter = new RecyclerViewNews(newsItems,this );

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        
        recyclerView.setAdapter(     recyclerAdapter);
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<NewsItem>> callBack = client.getNews(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),currentpage);

        callBack.enqueue(new Callback<List<    NewsItem>>() {
            @Override
            public void onResponse(Call<List<    NewsItem>> call, Response<List<    NewsItem>> response) {

                try {

                  //  Log.v("Tag", "v");
                    if (response.body() != null) {

                        List<    NewsItem> posts = response.body();

                        if (posts.size() > 0) {
                            //Log.v("Tag1", "v");

                            if(posts.size()<10)
                            {
                              isend=true;
                                      layoutProgress.setVisibility(View.GONE);
                            }

                            newsItems.addAll(posts);
                            recyclerAdapter.notifyDataSetChanged();


                           
                          

                        } else {


                        }

                    } else {


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<    NewsItem>> call, Throwable t) {

            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = getIntent();
                setResult(Activity.RESULT_OK, intent);
                finish();
                // do what you want to be done on home button click event
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

}
