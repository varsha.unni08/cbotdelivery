package com.project.centroid.deliveryapp.view.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.Constants;
import com.project.centroid.deliveryapp.Utils.GridSpacingItemDecoration;
import com.project.centroid.deliveryapp.Utils.LinearLayoutManagerWrapper;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Cart;
import com.project.centroid.deliveryapp.data.remote.model.OrderItem;
import com.project.centroid.deliveryapp.data.remote.model.OrderItem;
import com.project.centroid.deliveryapp.view.ProductActivity;
import com.project.centroid.deliveryapp.view.adapter.OrderRecyclerAdapter;
import com.project.centroid.deliveryapp.view.adapter.OrderRecyclerAdapterNew;
import com.project.centroid.deliveryapp.view.adapter.RecyclerAdapterProduct;
import com.project.centroid.deliveryapp.view.interfaces.cartListner;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.project.centroid.deliveryapp.Utils.Constants.cartArray;


public class OrderFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_OrderItem_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    Unbinder unbinder;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private SharedPreferences sharedpreferences;
    private OrderRecyclerAdapterNew adapter;
    private boolean allContactsLoaded=false;
    private ArrayList<OrderItem> orderItems1;
int pageCount=1;
    public OrderFragment() {
        // Required empty public constructor
    }

    public static OrderFragment newInstance() {
        OrderFragment fragment = new OrderFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank, container, false);
        sharedpreferences = getActivity().getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        unbinder = ButterKnife.bind(this, view);
      //  initRecyclerview();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }



//    private void getLatest() {
//
//
//        // TODO: Implement your own authentication logic here.
//        RestService.RestApiInterface client = RestService.getClient();
//
//        Call<List<OrderItem>> callback = client.getAllOrder(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),1);
//        callback.enqueue(new Callback<List<OrderItem>>() {
//            @Override
//            public void onResponse(Call<List<OrderItem>> call, Response<List<OrderItem>> response) {
//
//                try {
//                    List<OrderItem> orderItems1 = new ArrayList<>();
//                    orderItems1.addAll(response.body());
//                    OrderRecyclerAdapter adapter=new OrderRecyclerAdapter(getActivity(),orderItems1);
//                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//                    recyclerView.setItemAnimator(new DefaultItemAnimator());
//                    recyclerView.setNestedScrollingEnabled(false);
//
//                    recyclerView.setAdapter(adapter);
//                    recyclerView.setHasFixedSize(false);
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//
//            @Override
//            public void onFailure(Call<List<OrderItem>> call, Throwable t) {
//
//            }
//        });
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name

    }

    private void initRecyclerview() {

    orderItems1 = new ArrayList<>();
      

      
        LinearLayoutManager linearLayoutManager = new LinearLayoutManagerWrapper(getActivity(), LinearLayoutManager.VERTICAL, false);



        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
         adapter=new OrderRecyclerAdapterNew(getActivity(),orderItems1,recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
   
      

        int spanCount = 2; // 3 columns
        int spacing = 10;

        //recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, false));

        recyclerView.setAdapter(adapter);
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<OrderItem>> callBack = client.getAllOrder(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),1);

        callBack.enqueue(new Callback<List<OrderItem>>() {
            @Override
            public void onResponse(Call<List<OrderItem>> call, Response<List<OrderItem>> response) {

                try {

                    Log.v("Tag", "v");
                    if (response.body() != null) {

                        List<OrderItem> posts = response.body();

                        if (posts.size() > 0) {
                            Log.v("Tag1", "v");

                            orderItems1.addAll(posts);
                            adapter.notifyDataSetChanged();


                            int spanCount = 2; // 3 columns
                            int spacing = 10;
                            recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, false));

                            if (!allContactsLoaded) {
                                try {
                                    loadMore();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                        } else {


                        }

                    } else {


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<OrderItem>> call, Throwable t) {
                Log.v("Tag2", "v");
            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();

        initRecyclerview();

    }

    private void loadMore() {
        adapter.setOnLoadMoreListener(new OrderRecyclerAdapterNew.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.v("TAG", "loadmore");

                orderItems1.add(null);
                recyclerView.post(new Runnable() {
                    public void run() {
                        adapter.notifyItemInserted(orderItems1.size() - 1);
                    }
                });
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //remove progress item
                     /*   if(newsItems.size()>0){
                        ////newsItems.remove(newsItems.size() - 1);
                        adapter.notifyItemInserted(newsItems.size());}
                     */   //add items one by one
/*                        for (int i = 0; i < 15; i++) {
                            myDataset.add("OrderItem" + (myDataset.size() + 1));
                            mAdapter.notifyItemInserted(myDataset.size());

                        }*/
                        Log.v("TAG", "loading");
                        if (!allContactsLoaded) {

                            pageCount++;

                            try {
                                getMoreData(pageCount);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //adapter.setLoaded();

                        }
                        //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                    }
                }, 2000);
                //System.out.println("load");
            }
        });

    }

    private void getMoreData(final int page) {
        RestService.RestApiInterface client = RestService.getClient();
        Call<List<OrderItem>> callBack =  client.getAllOrder(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),page);

        callBack.enqueue(new Callback<List<OrderItem>>() {
            @Override
            public void onResponse(Call<List<OrderItem>> call, Response<List<OrderItem>> response) {

                try {

                    if (!response.body().isEmpty()) {
                        if (response.body() != null) {


                            List<OrderItem> posts = response.body();
                            if (posts.size() > 0) {
                                Log.v("TAG", "newloading");
                                adapter.setLoaded();
                                orderItems1.remove(orderItems1.size() - 1);
                                orderItems1.addAll(posts);
                                adapter.notifyItemInserted(orderItems1.size());


                            } else {

                                Log.v("TAG", "loadingfalse");
                                adapter.setLoadedFalse();
                                pageCount = 1;
                                allContactsLoaded = true;
                                orderItems1.remove(orderItems1.size() - 1);
                                adapter.notifyDataSetChanged();


                            }
                        } else {
                            Log.v("TAG", "loadingend");

                            allContactsLoaded = true;
                            orderItems1.remove(orderItems1.size() - 1);
                            adapter.setLoadedFalse();
                            adapter.notifyDataSetChanged();


                        }
                    } else {
                        allContactsLoaded = true;
                        orderItems1.remove(orderItems1.size() - 1);
                        adapter.setLoadedFalse();
                        adapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<OrderItem>> call, Throwable t) {

            }
        });


    }

}
