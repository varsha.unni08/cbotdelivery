package com.project.centroid.deliveryapp.view.adapter_new;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.data.remote.modelnew.MainCategory;

import java.util.List;

public class MainCategoryAdapter extends RecyclerView.Adapter<MainCategoryAdapter.MainCategoryHolder> {


    Context context;
    List<MainCategory>mainCategories;

    public MainCategoryAdapter(Context context, List<MainCategory> mainCategories) {
        this.context = context;
        this.mainCategories = mainCategories;
    }

    public class MainCategoryHolder extends RecyclerView.ViewHolder{

        TextView txtData;
        RecyclerView recyclerview_subcat;
        AppCompatImageView img_arrowkey;


        public MainCategoryHolder(@NonNull View itemView) {
            super(itemView);
            txtData=itemView.findViewById(R.id.txtData);
            recyclerview_subcat=itemView.findViewById(R.id.recyclerview_subcat);
            img_arrowkey=itemView.findViewById(R.id.img_arrowkey);

            img_arrowkey.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   if(mainCategories.get(getAdapterPosition()).getSelected()==0)
                   {

                       for (MainCategory mainCategory:mainCategories
                            ) {

                           mainCategory.setSelected(0);

                       }

                       mainCategories.get(getAdapterPosition()).setSelected(1);
                       notifyItemRangeChanged(0,mainCategories.size());

                   }
                   else {



                       mainCategories.get(getAdapterPosition()).setSelected(0);
                       notifyItemRangeChanged(0,mainCategories.size());


                   }


                }
            });

            txtData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if(mainCategories.get(getAdapterPosition()).getSelected()==0)
                    {
                        for (MainCategory mainCategory:mainCategories
                        ) {

                            mainCategory.setSelected(0);

                        }

                        mainCategories.get(getAdapterPosition()).setSelected(1);
                        notifyItemRangeChanged(0,mainCategories.size());
                    }

                    else{

                        mainCategories.get(getAdapterPosition()).setSelected(0);
                        notifyItemRangeChanged(0,mainCategories.size());

                    }

                }
            });
        }
    }


    @NonNull
    @Override
    public MainCategoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_maincategory_adapter,viewGroup,false);


        return new MainCategoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainCategoryHolder mainCategoryHolder, int i) {

        if(mainCategories.get(i).getSelected()==0)
        {

            mainCategoryHolder.recyclerview_subcat.setVisibility(View.GONE);
            mainCategoryHolder.img_arrowkey.setImageResource(R.drawable.ic_keyboard_arrow_down);
        }
        else{

            mainCategoryHolder.recyclerview_subcat.setVisibility(View.VISIBLE);
            mainCategoryHolder.img_arrowkey.setImageResource(R.drawable.ic_keyboard_arrow_up);
        }

        mainCategoryHolder.txtData.setText(mainCategories.get(i).getDsMainCategory());
        mainCategoryHolder.recyclerview_subcat.setLayoutManager(new LinearLayoutManager(context));
        mainCategoryHolder.recyclerview_subcat.setAdapter(new SubCategoryAdapter(context,mainCategories.get(i).getSubcategories()));


    }

    @Override
    public int getItemCount() {
        return mainCategories.size();
    }
}
