package com.project.centroid.deliveryapp.Utils;

import android.util.Log;

import com.project.centroid.deliveryapp.data.remote.model.Cart;
import com.project.centroid.deliveryapp.data.remote.model.CartItems;
import com.project.centroid.deliveryapp.data.remote.model.Category;
import com.project.centroid.deliveryapp.data.remote.model.Item;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Constants {
    public static List<Item> cartArray;
    public static Integer mCartItemCount;
    public static String  mCategoryId;
    public static String mSubCatId;
    public static String type;


    public static String getdate(){
        SimpleDateFormat dnt = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        System.out.println("Today Date & Time at Now :"+dnt.format(date));
        return  dnt.format(date);
    }

    public static boolean isItemPresent(String item, List<Item> cart) {

        if(item!=null&&cart!=null){
            for(Item c:cart){
                System.out.println( "vt" + c.getDsItemCode());
                System.out.println("vt" + item);

                if(c.getDsItemCode().equalsIgnoreCase(item)){

                    return  true;
                }
            }
        }
        return false;
    }

    public static  List<Item> removeItem(String item, List<Item> cart) {
        try{
        if(item!=null&&cart!=null){
            int i=0;
            for(Item c:cart){


                if(c.getDsItemCode().equalsIgnoreCase(item)){
                    cart.remove(i);
                  return cart;
                }
                i++;
            }
        }}catch (Exception e){
            e.printStackTrace();
        }
        return  cart;
    }
}
