package com.project.centroid.deliveryapp.view.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonArray;
import com.project.centroid.deliveryapp.ProfilefullscreenActivity;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.UpdateUser;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.UserItem;
import com.project.centroid.deliveryapp.view.LoginActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class AccountFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.profile)
    ImageView profile;
    @BindView(R.id.username)
    TextView username;
    @BindView(R.id.mobile)
    TextView mobile;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.dob)
    TextView dob;
    Unbinder unbinder;
    @BindView(R.id.edit)
    LinearLayout edit;
    @BindView(R.id.logout)
    LinearLayout logout;

    @BindView(R.id.imgedit)
    ImageView imgedit;

    String url="";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AccountFragment() {
        // Required empty public constructor
    }

    public static AccountFragment newInstance() {
        AccountFragment fragment = new AccountFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_userprofile, container, false);
        unbinder = ButterKnife.bind(this, view);
        try {
            setViews();
        } catch (Exception e) {
            e.printStackTrace();
        }

        getImage();

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent=new Intent(getActivity(), ProfilefullscreenActivity.class);
                intent.putExtra("Profile",url);
                startActivity(intent);
            }
        });



        imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                {

                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},110);
                }
                else {

                    Intent i = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, 2);
                }
            }
        });


        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2 && resultCode == RESULT_OK
                && null != data) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            File file=new File(picturePath);
uploadImageFile(file);




        }
    }


    public void uploadImageFile(File file)
    {

        // Create a request body with file and image media type
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
        // Create MultipartBody.Part using file request-body,file name and part name
        MultipartBody.Part part = MultipartBody.Part.createFormData("ds_image", file.getName(), fileReqBody);
        //Create request body with text description and text media type
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), "image-type");



        final ProgressDialog progressDialog = new ProgressDialog(getActivity(),
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<JsonArray>jsonArrayCall=client.uploadImage(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),part);


        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                progressDialog.dismiss();

                if(response.isSuccessful())
                {

                   try{

                       JSONArray jsonArray=new JSONArray(response.body().toString());

                       if(jsonArray.length()>0)
                       {

                           JSONObject jsonObject=jsonArray.getJSONObject(0);

                           if(jsonObject.getInt("status")==1)
                           {

                               Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();

                          getImage();

                           }
                           else {

                               Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                           }


                       }


                   }catch (Exception e)
                   {

                   }




                }
                else {

                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {

                progressDialog.dismiss();
                t.printStackTrace();
            }
        });



    }



    public void getImage()
    {
       final SharedPreferences sharedpreferences = getActivity().getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<JsonArray>jsonArrayCall=client.getImage(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""));


        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

             //   progressDialog.dismiss();

                if(response.isSuccessful())
                {

                    try{

                        JSONArray jsonArray=new JSONArray(response.body().toString());

                        if(jsonArray.length()>0)
                        {

                            JSONObject jsonObject=jsonArray.getJSONObject(0);

                            if(jsonObject.getInt("status")==1)
                            {

                               // Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();


                                 url=jsonObject.getString("message");



                              GlideUrl  glideUrl=  new GlideUrl(url, new LazyHeaders.Builder()
                                        .addHeader("Authorization", ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""))
                                        .build());



                              //  Glide.with(getActivity()).load(jsonObject.getString("message")).apply(RequestOptions.circleCropTransform()).into(profile);

                                Glide.with(getActivity()).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(glideUrl).apply(RequestOptions.circleCropTransform()).into(profile);

                            }
                            else {

                              //  Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                            }


                        }


                    }catch (Exception e)
                    {

                    }




                }
                else {

                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {

               // progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }





    private void setViews() {
        final Context context = getActivity();





        final ProgressDialog progressDialog = new ProgressDialog(context,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        SharedPreferences sharedpreferences = context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();


        Call<List<UserItem>> callback = client.getUser(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""));
        callback.enqueue(new Callback<List<UserItem>>() {
            @Override
            public void onResponse(Call<List<UserItem>> call, Response<List<UserItem>> response) {


                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        if (response.body() != null) {
                            List<UserItem> array = response.body();
                            UserItem userItem = array.get(0);
                            if (!TextUtils.isEmpty(userItem.getDsCustomer())) {
                                username.setText( userItem.getDsCustomer());
                            }
                            if (!TextUtils.isEmpty(userItem.getDsAddress())) {
                                mobile.setText(userItem.getDsPhoneno());
                            }
                            if (!TextUtils.isEmpty(userItem.getDsEmail())) {
                                email.setText(  userItem.getDsEmail());
                            }
                            if (!TextUtils.isEmpty(userItem.getDsDeliveryAddress())) {
                                dob.setText(userItem.getDsDeliveryAddress() + "\n" + userItem.getDsPincode());
                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<List<UserItem>> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }



    @OnClick({R.id.logout,R.id.edit})
    public void onViewClicked(View v) {
      SharedPreferences  sharedpreferences = getActivity().getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        switch (v.getId()){

            case R.id.logout:SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(ApiConstants.LOGINSTATE, "");
                editor.apply();
                editor.commit();
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();

                break;
            case R.id.edit:
                startActivity(new Intent(getActivity(), UpdateUser.class));
                break;
        }



    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name

    }
}
