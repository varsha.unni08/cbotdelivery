package com.project.centroid.deliveryapp.application;

import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.util.Log;

import com.project.centroid.deliveryapp.reciever.NetworkChangeReceiver;

public class MyApplication extends Application {




    private static MyApplication mInstance;
    private NetworkChangeReceiver connectivityReceiver;

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        registerConnectivityReceiver();
    }

    public MyApplication() {
        super();
    }

    private void registerConnectivityReceiver(){
        try {
            if (android.os.Build.VERSION.SDK_INT >= 24) {
                IntentFilter filter = new IntentFilter();
                filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
                registerReceiver(getConnectivityReceiver(), filter);
            }
        } catch (Exception e) {
            Log.e("TAG", e.getMessage());
        }
    }

    private NetworkChangeReceiver getConnectivityReceiver() {
        if (connectivityReceiver == null)
            connectivityReceiver = new NetworkChangeReceiver();

        return connectivityReceiver;
    }

    public void setConnectivityListener(NetworkChangeReceiver.ConnectivityReceiverListener listener) {
        NetworkChangeReceiver.connectivityReceiverListener = listener;
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }

}
