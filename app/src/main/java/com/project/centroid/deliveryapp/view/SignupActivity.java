package com.project.centroid.deliveryapp.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.MatrixCursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.UpdateUser;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Pincode;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";
    final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[6789]\\d{9}$";
    @BindView(R.id.input_name) EditText _nameText;
    @BindView(R.id.input_address) EditText _addressText;
    @BindView(R.id.input_email) EditText _emailText;
    @BindView(R.id.input_mobile) EditText _mobileText;
    @BindView(R.id.input_password) EditText _passwordText;
    @BindView(R.id.input_reEnterPassword) EditText _reEnterPasswordText;
    @BindView(R.id.btn_signup) Button _signupButton;
    @BindView(R.id.link_login) TextView _loginLink;
    @BindView(R.id.searchpincode)
    SearchView searchpincode;

    @BindView(R.id.txtmsg)
    TextView txtmsg;

    private String selected="male";


    CursorAdapter mAdapter;

    boolean pincodeFlag=false;

    List<Pincode>pincodes=new ArrayList<>();

    final String[] from = new String[] {"pincode"};
    final int[] to = new int[] {R.id.txt};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        getSupportActionBar().hide();


        mAdapter = new SimpleCursorAdapter(SignupActivity.this,
                R.layout.layout_listitem,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        searchpincode.setSuggestionsAdapter(mAdapter);



        searchpincode.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                                           @Override
                                           public boolean onQueryTextSubmit(String query) {
                                               searchpincode.clearFocus();
//                        Constants.type = "search";
//                        Intent intent = new Intent(getApplicationContext(), ProductActivity.class);
//                        intent.putExtra("key", query);
//                        startActivity(intent);

                                               // pincode.setQuery(pincodes.get(i).getDsPincode(),true);
                                               return true;
                                           }

                                           @Override
                                           public boolean onQueryTextChange(String newText) {

                                               getSearchSuggestions(newText);
                                               return false;
                                           }
                                       }

        );

        searchpincode.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int i) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int i) {


                searchpincode.setQuery(pincodes.get(i).getDsPincode(),true);

                checkPincode();


                return false;
            }
        });




        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }


    private void checkPincode() {

        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        final ProgressDialog progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();



        Call<ResponseBody> callback = client.checkPincode(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),searchpincode.getQuery().toString());
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject= new JSONObject(response.body().string());
                        if(jsonObject.getString("status").equalsIgnoreCase("1")){

                            pincodeFlag = true;
                            Snackbar.make(findViewById(android.R.id.content), "Items deliverable to this pincode.", Snackbar.LENGTH_SHORT).show();


                            txtmsg.setText("Delivering available to this pincode");
                            txtmsg.setVisibility(View.VISIBLE);
                            txtmsg.setTextColor(Color.parseColor("#29a65a"));

                        }else{

                            pincodeFlag = false;

                            txtmsg.setText("Delivering is not available to this pincode.Please try another");
                            txtmsg.setVisibility(View.VISIBLE);
                            txtmsg.setTextColor(Color.parseColor("#f0453c"));

                            Snackbar.make(findViewById(android.R.id.content), "Items are not deliverable to this pincode.Please try another", Snackbar.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }


    public void getSearchSuggestions(String txt)
    {
        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Pincode>> callBack = client.getPincodeSuggestions(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),txt)
                ;

        callBack.enqueue(new Callback<List<Pincode>>() {
            @Override
            public void onResponse(Call<List<Pincode>> call, Response<List<Pincode>> response) {

                if(response.isSuccessful())

                    if(response.body()!=null)
                    {
                        {

                            pincodes.clear();

                            pincodes.addAll(response.body());

                            final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, "pincode" });
                            for (int i=0; i<pincodes.size(); i++) {
                                // if (SUGGESTIONS[i].toLowerCase().startsWith(query.toLowerCase()))
                                c.addRow(new Object[] {i, pincodes.get(i).getDsPincode()});
                            }
                            mAdapter.changeCursor(c);



                        }}



            }

            @Override
            public void onFailure(Call<List<Pincode>> call, Throwable t) {

                t.printStackTrace();


            }
        });
    }








    public void signup() {
        Log.d(TAG, "Signup");

        if (validate()) {


            //onSignupFailed();
            //return;


            String name = _nameText.getText().toString();
            String address = _addressText.getText().toString();
            String email = _emailText.getText().toString();
            String mobile = _mobileText.getText().toString();
            String password = _passwordText.getText().toString();
            String reEnterPassword = _reEnterPasswordText.getText().toString();

            // TODO: Implement your own signup logic here.

            regCall();
        }

       /* new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onSignupSuccess or onSignupFailed
                        // depending on success
                        onSignupSuccess();
                        // onSignupFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);*/
    }





    @OnCheckedChanged({R.id.radio0, R.id.radio1})
    public void onRadioButtonCheckChanged(@NonNull CompoundButton button, boolean checked) {

        switch (button.getId()) {
            case R.id.radio0:
                if (checked) {
                    // Pirates are the best
                   selected="male";
                }
                break;
            case R.id.radio1:
                if (checked) {
                    selected="female";



                }
                break;
        }

    }
    public void onSignupSuccess() {
        _signupButton.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    public void onSignupFailed() {

        if(!pincodeFlag)
        {
            Toast.makeText(getBaseContext(),"Please select the delivery available pincode",Toast.LENGTH_SHORT).show();

        }


        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _signupButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = false;

        String name = _nameText.getText().toString();
        String address = _addressText.getText().toString();
        String email = _emailText.getText().toString();
        String mobile = _mobileText.getText().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

        if(pincodeFlag) {
            //valid=false;

//            Toast.makeText(SignupActivity.this,"Please select the delivery available pincode",Toast.LENGTH_SHORT).show();


            if (!name.isEmpty() || name.length() > 3) {


                if (!address.isEmpty()) {
                   // _addressText.setError("Enter Valid Address");
                  //  valid = false;

                    if (!email.isEmpty() || android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                        //_emailText.setError("enter a valid email address");
                       // valid = false;

                        if (!mobile.isEmpty() || mobile.length()==10) {


                            if (!password.isEmpty() || password.length() > 4 || password.length() < 10) {



                                if (!reEnterPassword.isEmpty() || reEnterPassword.length() > 4 || reEnterPassword.length() < 10 || (reEnterPassword.equals(password))) {
                                   // _reEnterPasswordText.setError("Password Do not match");
                                    valid = true;



                                } else {



                                    Toast.makeText(SignupActivity.this,"Password do not match",Toast.LENGTH_SHORT).show();


                                }




                            } else {


                                Toast.makeText(SignupActivity.this,"enter password between 4 and 10 alphanumeric characters",Toast.LENGTH_SHORT).show();


                                // _passwordText.setError(null);
                            }






                        } else {
                           // _mobileText.setError(null);


                            Toast.makeText(SignupActivity.this,"Enter a valid mobile number",Toast.LENGTH_SHORT).show();

                        }



                    } else {
                        //_emailText.setError(null);

                        Toast.makeText(SignupActivity.this,"Enter email address",Toast.LENGTH_SHORT).show();

                    }




                } else {
                    Toast.makeText(SignupActivity.this,"Enter address",Toast.LENGTH_SHORT).show();
                }




            } else {
                //_nameText.setError(null);

                Toast.makeText(SignupActivity.this,"Enter name",Toast.LENGTH_SHORT).show();

            }

        }else {

            Toast.makeText(SignupActivity.this,"Please select the delivery available pincode",Toast.LENGTH_SHORT).show();

        }
















        return valid;
    }
    private void regCall() {

        String name = _nameText.getText().toString();
        String address = _addressText.getText().toString();
        String email = _emailText.getText().toString();
        String mobile = _mobileText.getText().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        final SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<ResponseBody> callback = client.regUser("signup", name, mobile, email, password,selected,searchpincode.getQuery().toString());
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                           Toast.makeText(getApplicationContext(),"Sign up success",Toast.LENGTH_LONG).show();
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString(ApiConstants.USERTOKEN, jsonObject.getString("token"));
                            editor.putString(ApiConstants.LOGINSTATE, "true");
                            editor.apply();
                            editor.commit();
                          //  startActivity(new Intent(SignupActivity.this, HomeActivity.class));

                            Intent intent = new Intent(SignupActivity.this, MainHomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();


                        } else {

                            Snackbar.make(findViewById(android.R.id.content), "Something went wrong please try again!", Snackbar.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }
}