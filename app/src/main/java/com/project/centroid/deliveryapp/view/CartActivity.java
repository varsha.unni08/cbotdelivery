package com.project.centroid.deliveryapp.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.Constants;
import com.project.centroid.deliveryapp.ViewItemSingle;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.CartProduct;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.progress.ProgressFragment;
import com.project.centroid.deliveryapp.view.adapter.CartProductAdapter;
import com.project.centroid.deliveryapp.view.adapter.CartRecyclerAdapter;
import com.project.centroid.deliveryapp.view.interfaces.cartAmoutListner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity implements cartAmoutListner {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.pricetot)
    TextView pricetot;
    @BindView(R.id.placeorder)
    Button placeorder;
    @BindView(R.id.deliverycharge)
    TextView deliverycharge;

    @BindView(R.id.layout_total)
    LinearLayout layout_total;



    int currentpage=1;

    private CartRecyclerAdapter recyclerAdapter;
    private cartAmoutListner mlistner;

    CartProductAdapter cartProductAdapter;

    boolean isend=false;

    List<CartProduct>cartProducts;

    double totalprice=0;

    ProgressFragment progressFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        cartProducts=new ArrayList<>();
        initViews();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void loadmore()
    {

        if(!isend) {
            //layoutProgress.setVisibility(View.VISIBLE);
            currentpage++;
            initViews();
        }
    }


    public void getTotal()
    {
        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        final String token=ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, "");

        Log.e("Token forcount",token);

        RestService.RestApiInterface client = RestService.getClient();
        Call<JsonArray> callback = client.getTotalPriceFromcart(token,currentpage);

        callback.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                try {

                    JSONArray jsonArray=new JSONArray(response.body().toString());

                    if(jsonArray.length()>0)
                    {


                        JSONObject jsonObject=jsonArray.getJSONObject(0);
                        String total=jsonObject.getString("grandtotal_price");

                        double tot=Double.parseDouble(total);
                        totalprice=totalprice+tot;

                        pricetot.setText("Total : "+totalprice +" Rs");



                    }




                }catch (Exception e)
                {

                }



            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }


    private void initViews() {


progressFragment=new ProgressFragment();
progressFragment.show(getSupportFragmentManager(),"aalka");



        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        String token=ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, "");

        Log.e("Token forcount",token);

        RestService.RestApiInterface client = RestService.getClient();
        Call<List<CartProduct>> callback = client.getCartProducts(token,currentpage);

        callback.enqueue(new Callback<List<CartProduct>>() {
            @Override
            public void onResponse(Call<List<CartProduct>> call, Response<List<CartProduct>> response) {

                progressFragment.dismiss();
              //  progressDialog.dismiss();
                if(response.isSuccessful())
                {
                    try{


//                        if(response.body().size()==1)
//                        {
//                            Toast.makeText(CartActivity.this, "no data found", Toast.LENGTH_SHORT).show();
//
//                            layout_total.setVisibility(View.GONE);
//
//                            pricetot.setText("Total : "+response.body().get(0).getGrandtotal_price()+" Rs");
//
//                        }
//                        else {

                            if(response.body().size()<10)
                            {
                                isend=true;
                            }


                            cartProducts.addAll(response.body());

                            recyclerView.setLayoutManager(new LinearLayoutManager(CartActivity.this));


                            //cartProducts.remove(cartProducts.size()-1);

                        cartProductAdapter=  new CartProductAdapter(CartActivity.this,cartProducts);

                            recyclerView.setAdapter(cartProductAdapter);


                            getTotal();

                        //}



                        // notifyItemRangeChanged(0,items.size());


                    }catch (Exception e)
                    {

                    }
                }
                else {

                    if(cartProducts.size()==0) {

                        layout_total.setVisibility(View.GONE);
                    }

                    //Toast.makeText(CartActivity.this, "No items", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<CartProduct>> call, Throwable t) {
                t.printStackTrace();
                progressFragment.dismiss();
                if(cartProducts.size()==0) {

                    layout_total.setVisibility(View.GONE);
                }

            //    progressDialog.dismiss();
           //     layout_total.setVisibility(View.GONE);

              //  Toast.makeText(CartActivity.this, "failure", Toast.LENGTH_SHORT).show();

            }
        });




    }


    public void reduceValue(double val)
    {
        totalprice=totalprice-val;
    }


    public void setCartQuantity(final int qty, String cdItem, final int position)
    {
        final ProgressDialog progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Calendar cldr=Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        month++;

        String date=day+"-"+month+"-"+year;

        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);



        Call<JsonArray> callback = client.updateProductCart(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),cdItem,String.valueOf(qty),date);

        callback.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                progressDialog.dismiss();
                if(response.isSuccessful())
                {


                    try {
                        //JSONObject jsonObject= new JSONObject(response.body().toString());




                        int qy=Integer.parseInt(cartProducts.get(position).getCartQty());

                        double val=Double.parseDouble(cartProducts.get(position).getVlSellingPrice())*qy;
                        totalprice=totalprice-val;


                        double value=Double.parseDouble(cartProducts.get(position).getVlSellingPrice())*qty;


                        totalprice=totalprice+value;

                        pricetot.setText("Total : "+totalprice +" Rs");

                        cartProducts.get(position).setCartQty(String.valueOf(qty));

                        cartProductAdapter.notifyDataSetChanged();




                    } catch (Exception e) {
                        e.printStackTrace();
                    }



                }




            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {

                progressDialog.dismiss();
                t.printStackTrace();

            }
        });
    }


    public void deleteFromCart(String cditem, final int position)
    {

        final ProgressDialog progressDialog = new ProgressDialog(CartActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();



        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        String token=ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, "");

        Log.e("Token forcount",token);

        RestService.RestApiInterface client = RestService.getClient();

        Call<JsonObject>jsonObjectCall=client.deleteFromCart(token,cditem);
        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                progressDialog.dismiss();

                if(response.isSuccessful())
                {
                    try{

                        JSONObject jsonObject=new JSONObject(response.body().toString());
                        if(jsonObject.getInt("status")==1)
                        {

                            Toast.makeText(CartActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();

                            //initViews();;

                            cartProducts.remove(position);
                            cartProductAdapter.notifyDataSetChanged();
                        }
                        else {
                            Toast.makeText(CartActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                        }


                    }catch (Exception e)
                    {

                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }






    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
//                Intent intent = getIntent();
//                setResult(Activity.RESULT_OK, intent);
//                finish();

                onBackPressed();
                // do what you want to be done on home button click event
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    @Override
    public void OnQuantity(String cdItem, String quantity) {

        if (Constants.cartArray != null) {
            Integer amount = 0;
            if (Constants.cartArray.size() > 0) {
                for (Item singleItem : Constants.cartArray) {

                    Log.v("MRPV", singleItem.getVl_selling_price() + "v" + singleItem.getItemQuantity());
                    if (singleItem.getVl_selling_price() != null) {
                        int multiplier = 0;
                        if (singleItem.getItemQuantity() == null) {
                            multiplier = 1;
                        } else multiplier = singleItem.getItemQuantity();
                        amount += multiplier * Integer.parseInt(singleItem.getVlMrp());
                        pricetot.setText("Rs." + amount);
                    }

                }
            } else {
                Toast.makeText(getApplicationContext(), "Cart is empty", Toast.LENGTH_SHORT).show();
                pricetot.setText("");
            }
        } else {
            Toast.makeText(getApplicationContext(), "Cart is empty", Toast.LENGTH_SHORT).show();
            pricetot.setText("");
        }

    }

    @OnClick(R.id.placeorder)
    public void onViewClicked() {



            Intent intent=new Intent(this,OrderPlaceActivity.class);
            startActivity(intent);

    }
}
