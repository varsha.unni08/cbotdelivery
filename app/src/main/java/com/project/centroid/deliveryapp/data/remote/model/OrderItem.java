 package com.project.centroid.deliveryapp.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderItem {

    @SerializedName("cd_sales_order")
    @Expose
    private String cdSalesOrder;
    @SerializedName("ds_sales_order")
    @Expose
    private String dsSalesOrder;
    @SerializedName("dt_sales_order")
    @Expose
    private String dtSalesOrder;
    @SerializedName("dt_delivery")
    @Expose
    private String dtDelivery;
    @SerializedName("vl_grand_total")
    @Expose
    private String vlGrandTotal;
    @SerializedName("cd_sales_order_status")
    @Expose
    private String cdSalesOrderStatus;

    public OrderItem() {
    }

    public String getCdSalesOrder() {
        return cdSalesOrder;
    }

    public void setCdSalesOrder(String cdSalesOrder) {
        this.cdSalesOrder = cdSalesOrder;
    }

    public String getDsSalesOrder() {
        return dsSalesOrder;
    }

    public void setDsSalesOrder(String dsSalesOrder) {
        this.dsSalesOrder = dsSalesOrder;
    }

    public String getDtSalesOrder() {
        return dtSalesOrder;
    }

    public void setDtSalesOrder(String dtSalesOrder) {
        this.dtSalesOrder = dtSalesOrder;
    }

    public String getDtDelivery() {
        return dtDelivery;
    }

    public void setDtDelivery(String dtDelivery) {
        this.dtDelivery = dtDelivery;
    }

    public String getVlGrandTotal() {
        return vlGrandTotal;
    }

    public void setVlGrandTotal(String vlGrandTotal) {
        this.vlGrandTotal = vlGrandTotal;
    }

    public String getCdSalesOrderStatus() {
        return cdSalesOrderStatus;
    }

    public void setCdSalesOrderStatus(String cdSalesOrderStatus) {
        this.cdSalesOrderStatus = cdSalesOrderStatus;
    }
}