package com.project.centroid.deliveryapp.view.adapter_new;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.ViewItemSingle;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.view.HomeActivity;
import com.project.centroid.deliveryapp.view.MainHomeActivity;
import com.project.centroid.deliveryapp.view.UserProfileActivity;
import com.project.centroid.deliveryapp.view.adapter.RecyclerAdapterProduct;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductHolder> {

    Context context;
    List<Item> items;

    int page;

    public ProductAdapter(Context context, List<Item> items,int page) {
        this.context = context;
        this.items = items;
        this.page=page;
    }

    public class ProductHolder extends RecyclerView.ViewHolder{

        ImageView itemPic;
        TextView itemdetails,itemdetailsprice,itemdetailsmrpprice;

        public ProductHolder(View itemView) {
            super(itemView);

            itemPic=itemView.findViewById(R.id.itemPic);
            itemdetails=itemView.findViewById(R.id.itemdetails);
            itemdetailsprice=itemView.findViewById(R.id.itemdetailsprice);
            itemdetailsmrpprice=itemView.findViewById(R.id.itemdetailsmrpprice);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ViewItemSingle.class);
                    intent.putExtra("id", items.get(getAdapterPosition()).getCdItem());
                    context.startActivity(intent);
                }
            });
        }
    }

    @NonNull
    @Override
    public ProductHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_prod,viewGroup,false);

        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHolder productHolder, int i) {

        //productHolder.itemPic


//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        ((MainHomeActivity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int height = displayMetrics.heightPixels;
//        int width = displayMetrics.widthPixels;
//
//
//        double w1=width;
//        double w=w1/1.7;
//
//        productHolder.itemPic.setLayoutParams(new LinearLayout.LayoutParams(width, (int) w));
//

        Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(ApiConstants.imgbaseurl+items.get(i).getDsItemPic()).into(productHolder.itemPic);


        productHolder.itemdetailsprice.setText(" "+items.get(i).getVl_selling_price()+" "+context.getString(R.string.rs));
        productHolder.itemdetailsmrpprice.setText(items.get(i).getVlMrp()+" "+context.getString(R.string.rs));
        productHolder.itemdetailsmrpprice.setPaintFlags(productHolder.itemdetailsmrpprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        productHolder.itemdetails.setText(items.get(i).getDsItem()+"\n");

    }

    @Override
    public int getItemCount() {
        return page;
    }
}
