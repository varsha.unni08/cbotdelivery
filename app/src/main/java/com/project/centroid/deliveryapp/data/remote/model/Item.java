package com.project.centroid.deliveryapp.data.remote.model;

/**
 * @author Sooraj Soman on 11/1/2018
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Item implements Serializable {

    @SerializedName("cd_item")
    @Expose
    private String cdItem;
    @SerializedName("ds_item_code")
    @Expose
    private String dsItemCode;
    @SerializedName("ds_item")
    @Expose
    private String dsItem;
    @SerializedName("cd_category")
    @Expose
    private String cdCategory;
    @SerializedName("cd_subcategory")
    @Expose
    private String cdSubcategory;
    @SerializedName("nr_openingstock")
    @Expose
    private String nrOpeningstock;
    @SerializedName("cd_uom")
    @Expose
    private String cdUom;
    @SerializedName("vl_mrp")
    @Expose
    private String vlMrp;
    @SerializedName("vl_selling_price")
    @Expose
    private String vl_selling_price;
    @SerializedName("ds_hsncode")
    @Expose
    private String dsHsncode;
    @SerializedName("ds_item_description")
    @Expose
    private String dsItemDescription;
    @SerializedName("ds_item_pic")
    @Expose
    private String dsItemPic;
    @SerializedName("ds_item_pic_small")
    @Expose
    private String dsItemPicSmall;
    @SerializedName("vl_cgst")
    @Expose
    private String vlCgst;
    @SerializedName("vl_sgst")
    @Expose
    private String vlSgst;
    @SerializedName("ds_del_status")
    @Expose
    private String dsDelStatus;
    Integer itemQuantity;
    @SerializedName("vl_offer")
    @Expose
    private String vlOffer;

    private int isInWishlist=0;

    public int getIsInWishlist() {
        return isInWishlist;
    }

    public void setIsInWishlist(int isInWishlist) {
        this.isInWishlist = isInWishlist;
    }

    public void setVl_selling_price(String vl_selling_price) {
        this.vl_selling_price = vl_selling_price;
    }

    public String getCdItem() {
        return cdItem;
    }

    public void setCdItem(String cdItem) {
        this.cdItem = cdItem;
    }

    public String getDsItemCode() {
        return dsItemCode;
    }

    public void setDsItemCode(String dsItemCode) {
        this.dsItemCode = dsItemCode;
    }

    public String getDsItem() {
        return dsItem;
    }

    public void setDsItem(String dsItem) {
        this.dsItem = dsItem;
    }

    public String getCdCategory() {
        return cdCategory;
    }

    public void setCdCategory(String cdCategory) {
        this.cdCategory = cdCategory;
    }

    public String getCdSubcategory() {
        return cdSubcategory;
    }

    public void setCdSubcategory(String cdSubcategory) {
        this.cdSubcategory = cdSubcategory;
    }

    public String getNrOpeningstock() {
        return nrOpeningstock;
    }

    public void setNrOpeningstock(String nrOpeningstock) {
        this.nrOpeningstock = nrOpeningstock;
    }

    public String getCdUom() {
        return cdUom;
    }

    public void setCdUom(String cdUom) {
        this.cdUom = cdUom;
    }

    public String getVlMrp() {
        return vlMrp;
    }

    public void setVlMrp(String vlMrp) {
        this.vlMrp = vlMrp;
    }

    public String getDsHsncode() {
        return dsHsncode;
    }

    public void setDsHsncode(String dsHsncode) {
        this.dsHsncode = dsHsncode;
    }

    public String getDsItemDescription() {
        return dsItemDescription;
    }

    public void setDsItemDescription(String dsItemDescription) {
        this.dsItemDescription = dsItemDescription;
    }

    public String getDsItemPic() {
        return dsItemPic;
    }

    public void setDsItemPic(String dsItemPic) {
        this.dsItemPic = dsItemPic;
    }

    public String getDsItemPicSmall() {
        return dsItemPicSmall;
    }

    public void setDsItemPicSmall(String dsItemPicSmall) {
        this.dsItemPicSmall = dsItemPicSmall;
    }

    public String getVlCgst() {
        return vlCgst;
    }

    public void setVlCgst(String vlCgst) {
        this.vlCgst = vlCgst;
    }

    public String getVlSgst() {
        return vlSgst;
    }

    public void setVlSgst(String vlSgst) {
        this.vlSgst = vlSgst;
    }

    public String getDsDelStatus() {
        return dsDelStatus;
    }

    public void setDsDelStatus(String dsDelStatus) {
        this.dsDelStatus = dsDelStatus;
    }

    public Integer getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(Integer itemQuantity) {
        this.itemQuantity = itemQuantity;
    }
    public String getVlOffer() {
        return vlOffer;
    }

    public void setVlOffer(String vlOffer) {
        this.vlOffer = vlOffer;
    }

    public String getVl_selling_price() {
        return vl_selling_price;
    }

    @Override
    public String toString() {
        return "Item{" +
                "cdItem='" + cdItem + '\'' +
                ", dsItemCode='" + dsItemCode + '\'' +
                ", dsItem='" + dsItem + '\'' +
                ", cdCategory='" + cdCategory + '\'' +
                ", cdSubcategory='" + cdSubcategory + '\'' +
                ", nrOpeningstock='" + nrOpeningstock + '\'' +
                ", cdUom='" + cdUom + '\'' +
                ", vlMrp='" + vlMrp + '\'' +
                ", dsHsncode='" + dsHsncode + '\'' +
                ", dsItemDescription='" + dsItemDescription + '\'' +
                ", dsItemPic='" + dsItemPic + '\'' +
                ", dsItemPicSmall='" + dsItemPicSmall + '\'' +
                ", vlCgst='" + vlCgst + '\'' +
                ", vlSgst='" + vlSgst + '\'' +
                ", dsDelStatus='" + dsDelStatus + '\'' +
                ", itemQuantity=" + itemQuantity +
                '}';
    }

}
