package com.project.centroid.deliveryapp.view.adapter_new;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.ViewItemSingle;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.view.MainHomeActivity;

import java.util.List;

public class ProductItemAdapter extends RecyclerView.Adapter<ProductItemAdapter.ItemHolder> {


    Context context;
    List<Item>items;

    public ProductItemAdapter(Context context, List<Item> items) {
        this.context = context;
        this.items = items;
    }

    public class ItemHolder extends RecyclerView.ViewHolder{

        ImageView itemPic;

        TextView itemName,itemPrice,textnewprice;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            textnewprice=itemView.findViewById(R.id.textnewprice);
            itemPrice=itemView.findViewById(R.id.itemPrice);
            itemName=itemView.findViewById(R.id.itemName);

            itemPic=itemView.findViewById(R.id.itemPic);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, ViewItemSingle.class);
                    intent.putExtra("id", items.get(2).getCdItem());
                    context.startActivity(intent);
                }
            });
        }
    }


    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_product,viewGroup,false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder itemHolder, int i) {


int width=(int)context.getResources().getDimension(R.dimen.dimen_150dp);
        double w1=width;
        double w=w1/1.7;

        itemHolder.itemPic.setLayoutParams(new LinearLayout.LayoutParams(width, (int) w));


        Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(ApiConstants.imgbaseurl+items.get(i).getDsItemPic()).into(itemHolder.itemPic);

        itemHolder.textnewprice.setText(" "+items.get(i).getVl_selling_price()+" "+context.getString(R.string.rs));
        itemHolder.itemPrice.setText(items.get(i).getVlMrp()+" "+context.getString(R.string.rs));
        itemHolder.itemPrice.setPaintFlags(itemHolder.itemPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        itemHolder.itemName.setText(items.get(i).getDsItem()+"\n");

    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
