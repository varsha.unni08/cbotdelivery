package com.project.centroid.deliveryapp.view.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.Constants;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Cart;
import com.project.centroid.deliveryapp.data.remote.model.Category;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.view.HomeActivity;
import com.project.centroid.deliveryapp.view.ProductActivity;
import com.project.centroid.deliveryapp.view.adapter.CategoryRecyclerAdapter;
import com.project.centroid.deliveryapp.view.adapter.ItemRecyclerAdapter;
import com.project.centroid.deliveryapp.view.adapter.LatestItemRecyclerAdapter;
import com.project.centroid.deliveryapp.view.adapter.OfferedItemRecyclerAdapter;
import com.project.centroid.deliveryapp.view.interfaces.cartListner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    List<Item> data;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    Unbinder unbinder;
    @BindView(R.id.recyclerViewHorrizontal)
    RecyclerView recyclerViewHz;
    @BindView(R.id.recyclerViewGrid)
    RecyclerView recyclerViewGrid;
    @BindView(R.id.viewmore)
    Button viewmore;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private ItemRecyclerAdapter recyclerAdapter;
    private LatestItemRecyclerAdapter latestItemRecyclerAdapter;
    private CategoryRecyclerAdapter categoryRecyclerAdapter;
    private SharedPreferences sharedpreferences;
    private cartListner listner;
    private OfferedItemRecyclerAdapter offered;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

        sharedpreferences = getActivity().getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        initViews();
        return view;
    }

    private void initViews() {
        data = new ArrayList<>();
        if(Constants.cartArray!=null){
            data=Constants.cartArray;
        }
       /* LinearLayoutManager horizontalLayoutManager
                = new LinearLayoutManager(getActivity()
                , LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        final List<Item> items = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            items.add(new Item());
        }

        recyclerAdapter = new ItemRecyclerAdapter(getActivity(), items);
        recyclerView.setAdapter(recyclerAdapter);*/

        //recyclerView.setOnTouchListener(mTouchListener);

        LinearLayoutManager horizontalLayoutManager
                = new LinearLayoutManager(getActivity()
                , LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager horizontalLayoutManager1
                = new LinearLayoutManager(getActivity()
                , LinearLayoutManager.HORIZONTAL, false);
        recyclerViewHz.setLayoutManager(horizontalLayoutManager1);
        recyclerViewHz.setItemAnimator(new DefaultItemAnimator());
        recyclerViewHz.setHasFixedSize(false);
        getOffered();
        getLatest();



        listner = new cartListner() {
            @Override
            public void OnaddCart(String cdItem) {
                if (cdItem != null) {
                    //Log.v("TAG","v"+Constants.cartArray);

                    if (Constants.cartArray == null) {
                        Cart c = new Cart();
                        c.setCdItem(cdItem);
                        c.setDate(Constants.getdate());
                        c.setQuantity("1");
                        Gson gson=new Gson();
                        Item item=gson.fromJson(cdItem,Item.class);
                        data.add(item);
                        Constants.cartArray = data;
                       // Log.v("TAG","v"+Constants.cartArray);
                        Constants.mCartItemCount = Constants.cartArray.size();
                        Toast.makeText(getActivity(), "Item  added to cart", Toast.LENGTH_SHORT).show();


                    } else {
                        Gson gson=new Gson();
                        Item item=gson.fromJson(cdItem,Item.class);
                       // Log.v("TAG",cdItem+"v"+Constants.cartArray);
                        if(Constants.cartArray!=null){
                        if (Constants.isItemPresent(item.getDsItemCode(), Constants.cartArray)) {
                            Toast.makeText(getActivity(), "Item already added to cart", Toast.LENGTH_SHORT).show();
                        } else {

                            Log.v("TAG", "12"+item);
                            Log.v("TAG", "23"+cdItem);
                            Cart c = new Cart();
                            c.setCdItem(cdItem);
                            c.setDate(Constants.getdate());
                            c.setQuantity("1");

                            data.add(item);

                            Constants.cartArray = data;
                            Constants.mCartItemCount = Constants.cartArray.size();

                            Toast.makeText(getActivity(), "Item  added to cart", Toast.LENGTH_SHORT).show();

                        }}

                    }
                    ((HomeActivity) getActivity()).OnaddCart();
                }

            }


            @Override
            public void OnRemoveCart() {

            }
        };

        //recyclerViewHz.setOnTouchListener(mTouchListener);

        categoryCall();
       /* categoryRecyclerAdapter = new CategoryRecyclerAdapter(getActivity(), items1);
        recyclerViewGrid.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerViewGrid.setItemAnimator(new DefaultItemAnimator());
        recyclerViewGrid.setNestedScrollingEnabled(false);

        recyclerViewGrid.setAdapter(categoryRecyclerAdapter);
        recyclerView.setHasFixedSize(false);*/
        //recyclerViewGrid.setScrollin
        // recyclerViewGrid.setOnTouchListener(mTouchListener);


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private void categoryCall() {


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Category>> callback = client.getAllCategories(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""));
        callback.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {

                try {
                    categoryRecyclerAdapter = new CategoryRecyclerAdapter(getActivity(), response.body());
                    recyclerViewGrid.setLayoutManager(new GridLayoutManager(getActivity(), 3));
                    recyclerViewGrid.setItemAnimator(new DefaultItemAnimator());
                    recyclerViewGrid.setNestedScrollingEnabled(false);

                    recyclerViewGrid.setAdapter(categoryRecyclerAdapter);
                    recyclerView.setHasFixedSize(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {

            }
        });
    }

    private void getLatest() {


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Item>> callback = client.getFeaturedItems(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "latest");
        callback.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                try {
                    List<Item> items1 = new ArrayList<>();
                    items1.addAll(response.body());

                    Item item=new Item();
                    item.setCdItem("0");
                    items1.add(item);



                    latestItemRecyclerAdapter = new LatestItemRecyclerAdapter(getActivity(), items1);
                    recyclerViewHz.setAdapter(latestItemRecyclerAdapter);
                    latestItemRecyclerAdapter.setListner(listner);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {

            }
        });
    }

    private void getOffered() {


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<Item>> callback = client.getFeaturedItems(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), "offered");
        callback.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                try {
                    if(response.body()!=null){
                        if(response.body().size()>0){
                    List<Item> items1 = new ArrayList<>();
                    items1.addAll(response.body());

                    Item item=new Item();
                    item.setCdItem("0");
                    items1.add(item);


                    offered = new OfferedItemRecyclerAdapter(getActivity(), items1);
                    recyclerView.setAdapter(offered);
                    offered.setListner(listner);}
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        }
    };

    @OnClick(R.id.viewmore)
    public void onViewClicked() {

        Intent intent = new Intent(getActivity(), ProductActivity.class);
        Constants.type="all";
       Constants.mCategoryId=null;
               Constants.mSubCatId=null;
        startActivityForResult(intent,205);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name

    }
}
