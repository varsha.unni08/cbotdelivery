package com.project.centroid.deliveryapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.RequestOptions;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;

public class ProfilefullscreenActivity extends AppCompatActivity {

    ImageView imgProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilefullscreen);
        getSupportActionBar().hide();

        imgProfile=findViewById(R.id.imgProfile);

        String url=getIntent().getStringExtra("Profile");

        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


       // String url=jsonObject.getString("message");



        GlideUrl glideUrl=  new GlideUrl(url, new LazyHeaders.Builder()
                .addHeader("Authorization", ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""))
                .build());



        //  Glide.with(getActivity()).load(jsonObject.getString("message")).apply(RequestOptions.circleCropTransform()).into(profile);

        Glide.with(ProfilefullscreenActivity.this).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(glideUrl).into(imgProfile);

    }
}
