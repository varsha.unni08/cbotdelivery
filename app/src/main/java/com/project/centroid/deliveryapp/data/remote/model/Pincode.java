package com.project.centroid.deliveryapp.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pincode {

    @SerializedName("ds_pincode")
    @Expose
    private String dsPincode;

    public Pincode() {
    }

    public String getDsPincode() {
        return dsPincode;
    }

    public void setDsPincode(String dsPincode) {
        this.dsPincode = dsPincode;
    }
}
