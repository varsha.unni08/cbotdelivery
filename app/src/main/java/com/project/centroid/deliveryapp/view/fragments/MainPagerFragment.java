package com.project.centroid.deliveryapp.view.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.modelnew.Offer;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainPagerFragment extends Fragment {


    public MainPagerFragment() {
        // Required empty public constructor
    }

    View v;

    ImageView img;
    CardView cardv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       v=  inflater.inflate(R.layout.fragment_main_pager, container, false);
        img=v.findViewById(R.id.img);
        cardv=v.findViewById(R.id.cardV);

        Bundle bundle=getArguments();
        Offer pos=(Offer)bundle.getSerializable("pos");

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        double w=width/2.2;

        cardv.setLayoutParams(new LinearLayout.LayoutParams(width, (int) w));
        Glide.with(getActivity()).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(ApiConstants.imgbaseurl+pos.getDsSliderImage()).into(img);






        return v;
    }

}
