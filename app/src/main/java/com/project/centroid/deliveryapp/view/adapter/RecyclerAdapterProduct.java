package com.project.centroid.deliveryapp.view.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.LinearLayoutManagerWrapper;
import com.project.centroid.deliveryapp.ViewItemSingle;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.view.interfaces.cartListner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerAdapterProduct extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;

    private cartListner listner;
    private boolean isSwitchView = true;
    private static final int LIST_ITEM = 0;
    private static final int GRID_ITEM = 1;

    private static final int VIEW_PROG = 2;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private RecyclerAdapterProduct.OnLoadMoreListener onLoadMoreListener;
    private List<Item> users;
    private Item item;

    public RecyclerAdapterProduct(Context context, List<Item> users, RecyclerView recyclerView) {
        this.users = users;
        this.context = context;

        Log.v("TAG r", "" + recyclerView.getLayoutManager());
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            Log.v("TAG ", "linear v");
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    totalItemCount = linearLayoutManager.getItemCount();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        } else if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {


            final GridLayoutManager linearLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            Log.v("TAG r1", "" + recyclerView.getLayoutManager());

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    Log.v("TAG ", "linear v");
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    totalItemCount = linearLayoutManager.getItemCount();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = null;
        Log.v("TAG 1r", "er" + i);
        RecyclerView.ViewHolder vh = null;
        switch (i) {
            case LIST_ITEM:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_products, viewGroup, false);
                vh = new ViewHolder(v);
                break;
            case GRID_ITEM:
                Log.v("TAG 2r", "er");
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.grid_item_products, viewGroup, false);
                vh = new ViewHolder(v);
                break;
            case VIEW_PROG:
                Log.v("TAG 2r", "er");
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_progress_loadmore, viewGroup, false);
                vh = new ProgressViewHolder(v);
                break;
        }

        return vh;

    }

    @Override
    public int getItemViewType(int position) {
        if (isSwitchView) {
            return users.get(position) != null ? LIST_ITEM : VIEW_PROG;
        } else {
            return users.get(position) != null ? GRID_ITEM : VIEW_PROG;
        }

    }

    public boolean toggleItemViewType() {
        isSwitchView = !isSwitchView;
        return isSwitchView;
    }


    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolderNew, final int i) {


        if (viewHolderNew instanceof ViewHolder) {
            Log.v("TAG r", "er");

            //viewHolderNew.setIsRecyclable(false);

            ViewHolder holder = (ViewHolder) viewHolderNew;
            item = users.get(i);

            try {
                holder.set(item,i);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent=new Intent(context, ViewItemSingle.class);
                        intent.putExtra("id",users.get(i).getCdItem());
                        context.startActivity(intent);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (viewHolderNew instanceof ProgressViewHolder) {
            ProgressViewHolder viewHolder = (ProgressViewHolder) viewHolderNew;

            viewHolder.progressBar.setIndeterminate(true);
        }


        SharedPreferences sharedpreferences = context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        RestService.RestApiInterface client = RestService.getClient();
        Call<JsonArray> callback = client.getWishListExistence(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), item.getCdItem());
        callback.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                if (response.isSuccessful()) {
                    try {

                        JSONArray jsonArray = new JSONArray(response.body().toString());
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        if (jsonObject.getInt("whishlist_occurance") == 1) {

                            users.get(i).setIsInWishlist(1);

                            ImageView img=viewHolderNew.itemView.findViewById(R.id.bookmark);

                            img.setImageResource(R.drawable.ic_favorite_red);
                        } else {

                            users.get(i).setIsInWishlist(0);

                            ImageView img=viewHolderNew.itemView.findViewById(R.id.bookmark);

                            img.setImageResource(R.drawable.ic_favorite_border_red);
                        }




                    } catch (Exception e) {

                    }
                } else {

                    Toast.makeText(context, "failure", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                t.printStackTrace();

                Toast.makeText(context, "failure", Toast.LENGTH_SHORT).show();

            }
        });

    }


    @Override
    public int getItemCount() {

        return users == null ? 0 : users.size();
        //   return 5;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemPic)
        ImageView itemPic;
        @BindView(R.id.itemName)
        TextView itemName;
        @BindView(R.id.itemDesc)
        TextView itemDesc;
        @BindView(R.id.itemPrice)
        TextView itemPrice;
        @BindView(R.id.textnewprice)
        TextView textnewprice;
        @BindView(R.id.addbtn)
        LinearLayout addbtn;
        @BindView(R.id.bookmark)
        ImageView bookmark;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void set(final Item item, final int position) {
            if (ApiConstants.isNonEmpty(item.getDsItem())) {
                itemName.setText(item.getDsItem());
            }
            if (ApiConstants.isNonEmpty(item.getDsItemDescription())) {
                itemDesc.setText(item.getDsItemDescription());
            }
            if (ApiConstants.isNonEmpty(item.getVlMrp())) {
                itemPrice.setText(ApiConstants.amountFormatter(item.getVlMrp()));
            }
            if(ApiConstants.isNonEmpty(item.getVl_selling_price())){
                if (ApiConstants.isNonEmpty(item.getVlMrp())) {
                    if(!item.getVlMrp().equalsIgnoreCase(item.getVl_selling_price())){

                        itemPrice.setPaintFlags(itemPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        textnewprice.setText(ApiConstants.amountFormatter(item.getVl_selling_price()));
                    }}
            }
            if (ApiConstants.isNonEmpty(item.getDsItemPicSmall())) {
                Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(item.getDsItemPic()).into(itemPic);
            }
            //UI setting code
            final Item item1 = item;
            Gson gson = new Gson();
            final String data = gson.toJson(item);

            addbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listner.OnaddCart(data);
                }
            });
            bookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(item1.getIsInWishlist()==0) {


                        addToWishList(item1.getCdItem(), position);
                    }
                    else {

                        removeWishList(item1.getCdItem(),position);
                    }
                }
            });

          //  checkWishListExistence(item.getCdItem(),position);

            if(item.getIsInWishlist()==1)
            {

                bookmark.setImageResource(R.drawable.ic_favorite_red);
            }
            else {
                bookmark.setImageResource(R.drawable.ic_favorite_border_red);
            }
        }
    }

    private void addToWishList(final String cdItem,final int position) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);

        final ProgressDialog progressDialog = new ProgressDialog(context,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();


        Call<ResponseBody> callback = client.addWishList(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), cdItem, "1");
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.getString("status").equalsIgnoreCase("1")) {


                            Toast.makeText(context, "Item added to wishlist successfully", Toast.LENGTH_SHORT).show();

                            //checkWishListExistence(cdItem,position);

                            users.get(position).setIsInWishlist(1);
                            notifyItemRangeChanged(0,users.size());

                        } else {

                            Toast.makeText(context, "Some thing went wrong.Please try after some time", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                progressDialog.dismiss();

                t.printStackTrace();
            }
        });
    }


    public void checkWishListExistence(String cditem, final int position)
    {


//        if(item.getIsInWishlist()==1)
//        {
//
//            bookmark.setImageResource(R.drawable.ic_favorite_red);
//        }
//        else {
//            bookmark.setImageResource(R.drawable.ic_favorite_border_red);
//        }


    }


    private void removeWishList(String orderid, final int pos) {



        final ProgressDialog progressDialog = new ProgressDialog(context,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        SharedPreferences sharedpreferences =context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);



        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();



        Call<ResponseBody> callback = client.removeWishList(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),orderid);
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject= new JSONObject(response.body().string());
                        if(jsonObject.getString("status").equalsIgnoreCase("1")){
                            //items.remove(pos);

                            users.get(pos).setIsInWishlist(0);
                            notifyItemRangeChanged(0,users.size());

                            Toast.makeText(context, "item removed successfully", Toast.LENGTH_SHORT).show();

                        }else{

                            Toast.makeText(context, " ", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(@NonNull View v) {
            super(v);
            progressBar = v.findViewById(R.id.progress);
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public void setLoaded() {
        loading = false;
    }

    public void setLoadedFalse() {
        loading = true;
    }

    public void setListner(cartListner listner) {
        this.listner = listner;
    }

}
