package com.project.centroid.deliveryapp.view.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.ViewItemSingle;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.view.HomeActivity;
import com.project.centroid.deliveryapp.view.fragments.WishListFragment;
import com.project.centroid.deliveryapp.view.interfaces.cartListner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Sooraj Soman on 11/1/2018
 */
public class WishListRecyclerAdapter extends RecyclerView.Adapter<WishListRecyclerAdapter.ItemViewHolder> {
    private final Context context;
    private List<Item> items;
    private cartListner listner;
    WishListFragment wishListFragment;

    public WishListRecyclerAdapter(WishListFragment wishListFragment,Context context, List<Item> items) {
        this.items = items;
        this.context = context;
        this.wishListFragment=wishListFragment;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_wishlist, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        try {

            if(position==items.size()-1)
            {
                wishListFragment.loadmore();
            }



           final Item item = items.get(position);
            holder.set(item);
            holder.  delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setMessage("Are you sure, You wanted to remove the item");
                    alertDialogBuilder.setPositiveButton("yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    removeWishList(item.getCdItem(),position);
                                }
                            });

                    alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();


                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, ViewItemSingle.class);
                    intent.putExtra("id",items.get(position).getCdItem());
                    ((HomeActivity) context).startActivityForResult(intent,205);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void setListner(cartListner listner) {
        this.listner = listner;
    }
    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemPic)
        ImageView itemPic;
        @BindView(R.id.itemName)
        TextView itemName;
        @BindView(R.id.itemDesc)
        TextView itemDesc;
        @BindView(R.id.itemPrice)
        TextView itemPrice;
        @BindView(R.id.addbtn)
        LinearLayout addbtn;
        @BindView(R.id.bookmark)
        ImageView delete;
        @BindView(R.id.textnewprice)
        TextView textnewprice;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }



        public void set(final Item item) {
            if (ApiConstants.isNonEmpty(item.getDsItem())) {
                itemName.setText(item.getDsItem());
            }
            if (ApiConstants.isNonEmpty(item.getDsItemDescription())) {
                itemDesc.setText(item.getDsItemDescription());
            }
            if (ApiConstants.isNonEmpty(item.getVlMrp())) {
                itemPrice.setText(ApiConstants.amountFormatter(item.getVlMrp()));
            }
            if(ApiConstants.isNonEmpty(item.getVl_selling_price())){
                if (ApiConstants.isNonEmpty(item.getVlMrp())) {
                if(!item.getVlMrp().equalsIgnoreCase(item.getVl_selling_price())){

                    itemPrice.setPaintFlags(itemPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    textnewprice.setText(ApiConstants.amountFormatter(item.getVl_selling_price()));
                }}
            }
            if (ApiConstants.isNonEmpty(item.getDsItemPicSmall())) {
                Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(item.getDsItemPic()).into(itemPic);
            }


            //UI setting code
        }
    }

    private void removeWishList(String orderid, final int pos) {



        final ProgressDialog progressDialog = new ProgressDialog(context,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        SharedPreferences sharedpreferences =context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);



        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();



        Call<ResponseBody> callback = client.removeWishList(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),orderid);
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject= new JSONObject(response.body().string());
                        if(jsonObject.getString("status").equalsIgnoreCase("1")){
                             items.remove(pos);
                             notifyDataSetChanged();

                            Toast.makeText(context, "item removed successfully", Toast.LENGTH_SHORT).show();

                        }else{

                            Toast.makeText(context, " ", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }


}