package com.project.centroid.deliveryapp.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubCategory {

    @SerializedName("cd_subcategory")
    @Expose
    private String cdSubcategory;
    @SerializedName("ds_subcategory")
    @Expose
    private String dsSubcategory;


    @SerializedName("status")
    @Expose
    private String Status="1";
    @SerializedName("message")
    @Expose
    private String Warning;

    public SubCategory(String cdSubcategory, String dsSubcategory) {
        this.cdSubcategory = cdSubcategory;
        this.dsSubcategory = dsSubcategory;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getCdSubcategory() {
        return cdSubcategory;
    }

    public void setCdSubcategory(String cdSubcategory) {
        this.cdSubcategory = cdSubcategory;
    }

    public String getDsSubcategory() {
        return dsSubcategory;
    }

    public void setDsSubcategory(String dsSubcategory) {
        this.dsSubcategory = dsSubcategory;
    }
    @Override
    public String toString() {
        return ""+this.getDsSubcategory();
    }

}