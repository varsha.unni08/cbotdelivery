package com.project.centroid.deliveryapp.view.adapter_new;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.ZoomOutPageTransformer;
import com.project.centroid.deliveryapp.ViewItemSingle;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Item;
import com.project.centroid.deliveryapp.data.remote.modelnew.Offer;
import com.project.centroid.deliveryapp.data.remote.modelnew.ProductWithCategory;
import com.project.centroid.deliveryapp.view.MainHomeActivity;
import com.project.centroid.deliveryapp.view.ProductFullActivity;
import com.project.centroid.deliveryapp.view.progress.ProgressFragment;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<ProductWithCategory> productWithCategories;

    Handler handler ;
    int currentPage=0;
    int NUM_PAGES=5;
    Timer timer;
    Runnable Update;
    List<Offer>offers;

    public ProductCategoryAdapter(Context context, List<ProductWithCategory> productWithCategories,List<Offer>offers) {
        this.context = context;
        this.productWithCategories = productWithCategories;
        this.offers=offers;
    }

    public class ProductCategoryHolder extends RecyclerView.ViewHolder{
        RecyclerView recyclerview;
        TextView categoryname;
        LinearLayout layout_pro_head;

        ImageView itemPic;
        TextView itemdetails,itemdetailsprice,itemdetailsmrpprice;
        Button btnviewall;

        public ProductCategoryHolder(@NonNull View itemView) {
            super(itemView);
            recyclerview=itemView.findViewById(R.id.recyclerview);
            categoryname=itemView.findViewById(R.id.categoryname);
            layout_pro_head=itemView.findViewById(R.id.layout_pro_head);


            itemPic=itemView.findViewById(R.id.itemPic);
            itemdetails=itemView.findViewById(R.id.itemdetails);
            itemdetailsprice=itemView.findViewById(R.id.itemdetailsprice);
            btnviewall=itemView.findViewById(R.id.btnviewall);
            itemdetailsmrpprice=itemView.findViewById(R.id.itemdetailsmrpprice);

            btnviewall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, ProductFullActivity.class);
                    intent.putExtra("prod",productWithCategories.get(getAdapterPosition()));

                    context.startActivity(intent);
                }
            });

        }


    }

    public class ProductPagerHolder extends RecyclerView.ViewHolder{

        ViewPager viewPager;
        TabLayout tab_layout;

        public ProductPagerHolder(@NonNull View itemView) {
            super(itemView);
            viewPager=itemView.findViewById(R.id.viewPager);
            tab_layout=itemView.findViewById(R.id.tab_layout);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        RecyclerView.ViewHolder viewHolder=null;

        if(i==0) {

            View view1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_pageradapter, viewGroup, false);

            viewHolder=new ProductPagerHolder(view1);
        }
        else {

            View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_prod_adapter,viewGroup,false);

            viewHolder=new ProductCategoryHolder(view);
        }


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        holder.setIsRecyclable(false);



        if(holder.getItemViewType()==1) {

            if(holder instanceof ProductCategoryHolder) {



                ProductCategoryHolder productCategoryHolder = (ProductCategoryHolder) holder;

              //  productCategoryHolder.recyclerview.getRecycledViewPool().setMaxRecycledViews(1, 0);

                productCategoryHolder.categoryname.setText(productWithCategories.get(i).getCategorydata());

              final   List<Item>items=productWithCategories.get(i).getItems();



                if (productWithCategories.get(i).getItems().size() == 1) {

                    productCategoryHolder.recyclerview.setLayoutManager(new LinearLayoutManager(context));
                    productCategoryHolder.recyclerview.setAdapter(new ProductAdapter(context, items,1));
                    productCategoryHolder.layout_pro_head.setVisibility(View.GONE);
                    productCategoryHolder.recyclerview.getAdapter().notifyDataSetChanged();

                } else if (productWithCategories.get(i).getItems().size() == 3) {

                    productCategoryHolder.recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
                    productCategoryHolder.recyclerview.setAdapter(new ProductAdapter(context, items,2));






                    productCategoryHolder.layout_pro_head.setVisibility(View.VISIBLE);


                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    ((MainHomeActivity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    int height = displayMetrics.heightPixels;
                    int width = displayMetrics.widthPixels;


                    double w1=width;
                    double w=w1/1.7;
                    productCategoryHolder.itemPic.setLayoutParams(new LinearLayout.LayoutParams(width, (int) w));




                    Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(ApiConstants.imgbaseurl+items.get(2).getDsItemPic()).into(productCategoryHolder.itemPic);



                    productCategoryHolder.itemdetails.setText(items.get(2).getDsItem()+"\n");
                    productCategoryHolder.itemdetailsprice.setText(items.get(2).getVl_selling_price()+" "+context.getString(R.string.rs));

                    productCategoryHolder.itemdetailsmrpprice.setText(items.get(2).getVlMrp()+" "+context.getString(R.string.rs));
                    productCategoryHolder.itemdetailsmrpprice.setPaintFlags(productCategoryHolder.itemdetailsmrpprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


                    productCategoryHolder.recyclerview.getAdapter().notifyDataSetChanged();


                    productCategoryHolder.itemdetails.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, ViewItemSingle.class);
                            intent.putExtra("id", items.get(2).getCdItem());
                            context.startActivity(intent);
                        }
                    });


                    productCategoryHolder.itemdetailsprice.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, ViewItemSingle.class);
                            intent.putExtra("id", items.get(2).getCdItem());
                            context.startActivity(intent);
                        }
                    });

                    productCategoryHolder.itemPic.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, ViewItemSingle.class);
                            intent.putExtra("id", items.get(2).getCdItem());
                            context.startActivity(intent);
                        }
                    });







                } else if (productWithCategories.get(i).getItems().size() == 2) {

                    productCategoryHolder.recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
                    productCategoryHolder.recyclerview.setAdapter(new ProductAdapter(context, items,2));
                    productCategoryHolder.recyclerview.getAdapter().notifyDataSetChanged();

                } else if (productWithCategories.get(i).getItems().size() >= 4) {

                    productCategoryHolder.recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
                    productCategoryHolder.recyclerview.setAdapter(new ProductAdapter(context, items,4));

                    productCategoryHolder.recyclerview.getAdapter().notifyDataSetChanged();
                }
            }

        }
        else {


            if(holder instanceof ProductPagerHolder) {
                ProductPagerHolder productCategoryHolder = (ProductPagerHolder) holder;
                setupTab(productCategoryHolder);
            }






        }


    }

    @Override
    public int getItemCount() {
        return productWithCategories.size();
    }

    @Override
    public int getItemViewType(int position) {

        if(position==0)
        {
            return 0;
        }
        else{

            return 1;

        }




}


    public void setupTab(final ProductPagerHolder productPagerHolder)
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((MainHomeActivity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
       final int height = displayMetrics.heightPixels;
      final   int width = displayMetrics.widthPixels;
      final   double w=width/2.2;




        productPagerHolder.viewPager.setLayoutParams(new FrameLayout.LayoutParams(width, (int) w));


        for (int i=0;i<offers.size();i++)
        {

            productPagerHolder.tab_layout.addTab(productPagerHolder.tab_layout.newTab());
            productPagerHolder.viewPager.setId(i);
        }

        productPagerHolder.viewPager.setAdapter(new MainHomePagerAdapter(((MainHomeActivity)context).getSupportFragmentManager(),offers));
        productPagerHolder.tab_layout.setupWithViewPager(productPagerHolder.viewPager);

        productPagerHolder.viewPager.setPageTransformer(true,new ZoomOutPageTransformer());

        productPagerHolder.tab_layout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                productPagerHolder.viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        handler = new Handler();
//
//        Update = new Runnable() {
//            public void run() {
//                if (currentPage == offers.size()-1) {
//                    currentPage = 0;
//                }
//
//                currentPage=currentPage+1;
//                productPagerHolder.viewPager.setCurrentItem(currentPage, true);
//            }
//        };
//
//        timer = new Timer(); // This will create a new Thread
//        timer.schedule(new TimerTask() { // task to be scheduled
//            @Override
//            public void run() {
//                handler.post(Update);
//            }
//        }, 500, 2000);
















    }
}
