package com.project.centroid.deliveryapp.view.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.Constants;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.Category;
import com.project.centroid.deliveryapp.data.remote.model.SubCategory;
import com.project.centroid.deliveryapp.view.ProductActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Sooraj Soman on 11/1/2018
 */
public class CategoryRecyclerAdapter extends RecyclerView.Adapter<CategoryRecyclerAdapter.ItemViewHolder> {
    private final Context context;

    private List<Category> items;
    SharedPreferences sharedpreferences;

    public CategoryRecyclerAdapter(Context context, List<Category> items) {
        this.items = items;
        this.context = context;
        this.sharedpreferences = context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_item_category, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {
        final Category item = items.get(position);
        holder.set(item);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.mCategoryId = item.getCdCategory();
                showSubCatList(Constants.mCategoryId);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }


    private void showSubCatList(String catId) {

        final ProgressDialog progressDialog = new ProgressDialog(context,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();



        final String authToken = ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, "");
        RestService.RestApiInterface service = RestService.getClient();
        Call<List<SubCategory>> callBack = service.getSubcategoryList(authToken, catId);
        callBack.enqueue(new Callback<List<SubCategory>>() {
            @Override
            public void onResponse(Call<List<SubCategory>> call, Response<List<SubCategory>> response) {
                try {

                    progressDialog.dismiss();


                    showDialog(response.body());


                } catch (Exception e) {
                    e.printStackTrace();

                }


            }

            @Override
            public void onFailure(Call<List<SubCategory>> call, Throwable t) {

                progressDialog.dismiss();

                Toast.makeText(context, "Something went wrong!", Toast.LENGTH_SHORT).show();

            }
        });
    }


    private void showDialog(final List<SubCategory> data) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final View view = layoutInflater.inflate(R.layout.dialog_subcategrory, null, false);
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(context);
        final ListView listView = view.findViewById(R.id.list);

        pictureDialog.setTitle("Select sub category!");
        Log.v("TAG", "tag1");
        pictureDialog.setView(view);
        pictureDialog.setNegativeButton("Close",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });


        ListAdapter adp = new ListAdapter(context, data);
        listView.setAdapter(adp);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Constants.mSubCatId = data.get(i).getCdSubcategory();
                Intent intent = new Intent(context, ProductActivity.class);
                Constants.type = "cat";
                context.startActivity(intent);
            }
        });
        AlertDialog dialog = pictureDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemPic)
        ImageView itemPic;
        @BindView(R.id.itemName)
        TextView itemName;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void set(Category item) {
            itemName.setText(item.getDsCategory());
            Glide.with(context).load(item.getDsImage()).into(itemPic);

            //UI setting code
        }
    }
}