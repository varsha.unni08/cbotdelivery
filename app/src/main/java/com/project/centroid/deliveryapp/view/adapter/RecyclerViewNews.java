package com.project.centroid.deliveryapp.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.model.NewsItem;
import com.project.centroid.deliveryapp.view.NewsDetailsActivity;
import com.project.centroid.deliveryapp.view.NewsViewActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerViewNews extends RecyclerView.Adapter<RecyclerViewNews.NewsViewHolder> {
    private final Context context;
    private List<NewsItem> items;

    public RecyclerViewNews(List<NewsItem> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_news, parent, false);
        return new NewsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {

        if(position==items.size()-1)
        {
            ((NewsViewActivity)context).loadmore();
        }


        NewsItem item = items.get(position);
             if (item != null) {
            if (ApiConstants.isNonEmpty(item.getDsNews())) {
                holder.newsTitleText.setText("" + item.getDsNews());
            } else {

            }
            if (ApiConstants.isNonEmpty(item.getDsNewsImage())) {
                // holder.storyImage.setText(""+item.getDsNews());
                try {
                    Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(item.getDsNewsImage()).into(holder.storyImage);
                } catch (Exception e) {
                    e.printStackTrace();
                }


        } else {

        }
        if (ApiConstants.isNonEmpty(item.getDsNews())) {
            holder.sectionNameText.setText("Source :" + item.getDsSource());
        } else {

        }
    }
    //TODO Fill in your logic for binding the view.
}

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

public class NewsViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.story_image)
    ImageView storyImage;
    @BindView(R.id.news_title_text)
    TextView newsTitleText;
    @BindView(R.id.section_name_text)
    TextView sectionNameText;
    @BindView(R.id.story_card)
    CardView storyCard;

    public NewsViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context, NewsDetailsActivity.class);

                intent.putExtra("News",items.get(getAdapterPosition()));
                context.startActivity(intent);


            }
        });
    }
}
}