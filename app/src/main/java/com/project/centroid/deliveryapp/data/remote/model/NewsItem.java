package com.project.centroid.deliveryapp.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NewsItem implements Serializable {

    @SerializedName("ds_news")
    @Expose
    private String dsNews;
    @SerializedName("ds_source")
    @Expose
    private String dsSource;
    @SerializedName("ds_news_image")
    @Expose
    private String dsNewsImage;

    public String getDsNews() {
        return dsNews;
    }

    public void setDsNews(String dsNews) {
        this.dsNews = dsNews;
    }

    public String getDsSource() {
        return dsSource;
    }

    public void setDsSource(String dsSource) {
        this.dsSource = dsSource;
    }

    public String getDsNewsImage() {
        return dsNewsImage;
    }

    public void setDsNewsImage(String dsNewsImage) {
        this.dsNewsImage = dsNewsImage;
    }

}