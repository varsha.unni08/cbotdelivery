package com.project.centroid.deliveryapp.view.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.OrderItem;
import com.project.centroid.deliveryapp.view.interfaces.cartListner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Sooraj Soman on 11/1/2018
 */
public class OrderRecyclerAdapter extends RecyclerView.Adapter<OrderRecyclerAdapter.ItemViewHolder> {
    private final Context context;

    private List<OrderItem> items;
    private cartListner listner;

    public OrderRecyclerAdapter(Context context, List<OrderItem> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_itemorders, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        try {
            final OrderItem item = items.get(position);
            holder.set(item);
            if( items.get(position).getCdSalesOrderStatus().equalsIgnoreCase("Initial")){
            holder.cancelOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setMessage("Are you sure, You wanted to cancel order");
                            alertDialogBuilder.setPositiveButton("yes",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                          cancelorder(items.get(position).getCdSalesOrder(),position);
                                        }
                                    });

                    alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();


                }
            });}else{
                holder.cancelOrder.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setListner(cartListner listner) {
        this.listner = listner;
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }
    private void cancelorder(String orderid, final int pos) {



        final ProgressDialog progressDialog = new ProgressDialog(context,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        SharedPreferences sharedpreferences =context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);



        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();



        Call<ResponseBody> callback = client.cancelOrder(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),orderid);
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject= new JSONObject(response.body().string());
                        if(jsonObject.getString("status").equalsIgnoreCase("1")){
                            // items.remove(i)
                            Toast.makeText(context, "order cancelled successfully", Toast.LENGTH_SHORT).show();


                            items.remove(pos);
                            notifyItemRemoved(pos);
                        }else{

                            Toast.makeText(context, " ", Toast.LENGTH_SHORT).show();

                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.orderdate)
        TextView orderdate;
        @BindView(R.id.deliverydate)
        TextView deliverydate;
        @BindView(R.id.amount)
        TextView itemPrice;
        @BindView(R.id.btnremove)
        Button  cancelOrder;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        public void set(OrderItem item) {
            //if (ApiConstants.isNonEmpty(item.getDsSalesOrder())) {
            name.setText(" Order : " + item.getCdSalesOrder());
//            }
//            if (ApiConstants.isNonEmpty(item.getDtSalesOrder())) {
            orderdate.setText("Delivery date : " + item.getDtDelivery());
//            }
//            if (ApiConstants.isNonEmpty(item.getDtDelivery())) {
            deliverydate.setText(" " + item.getCdSalesOrderStatus());
//            }
//            if (ApiConstants.isNonEmpty(item.getVlGrandTotal())) {
            itemPrice.setText("Amount :"+ApiConstants.amountFormatter(item.getVlGrandTotal()));
            // }
            //UI setting code
        }
    }
}