package com.project.centroid.deliveryapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.UserItem;
import com.project.centroid.deliveryapp.view.HomeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateUser extends AppCompatActivity {

    @BindView(R.id.profile)
    ImageView profile;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.mobile)
    EditText mobile;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.homeaddress)
    EditText homeaddress;
    @BindView(R.id.deladdress)
    EditText deladdress;
    @BindView(R.id.pincode)
    EditText pincode;
    @BindView(R.id.update)
    Button update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user);
        ButterKnife.bind(this);
        try {
            setViews();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.update)
    public void onViewClicked() {
        try {


            String name = username.getText().toString();
            String address = homeaddress.getText().toString();
            String email1 = email.getText().toString();
            String mobile1 = mobile.getText().toString();
            String pincode1 = pincode.getText().toString();
            String deladd = deladdress.getText().toString();

            final String regexStr = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[6789]\\d{9}$";



            if (ApiConstants.isNonEmpty(name)) {
                if (!mobile1.isEmpty() || mobile1.length()==10) {
                    if (!email1.isEmpty() || android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches()) {

                        if (ApiConstants.isNonEmpty(address)) {
                            if (ApiConstants.isNonEmpty(deladd)) {
                                if (ApiConstants.isNonEmpty(pincode1)) {
                                    updateCall();
                                } else {
                                    Snackbar.make(findViewById(android.R.id.content), "Please enter pincode", Snackbar.LENGTH_SHORT).show();

                                }
                            } else {
                                Snackbar.make(findViewById(android.R.id.content), "Please enter delivery address", Snackbar.LENGTH_SHORT).show();

                            }
                        } else {
                            Snackbar.make(findViewById(android.R.id.content), "Please enter home address", Snackbar.LENGTH_SHORT).show();

                        }
                    } else {
                        Snackbar.make(findViewById(android.R.id.content), "Please enter emailid", Snackbar.LENGTH_SHORT).show();

                    }
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Please enter mobile no", Snackbar.LENGTH_SHORT).show();


                }
            } else {
                Snackbar.make(findViewById(android.R.id.content), "Please enter name", Snackbar.LENGTH_SHORT).show();


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setViews() {
        final Context context = UpdateUser.this;

        final ProgressDialog progressDialog = new ProgressDialog(context,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        SharedPreferences sharedpreferences = context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();


        Call<List<UserItem>> callback = client.getUser(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""));
        callback.enqueue(new Callback<List<UserItem>>() {
            @Override
            public void onResponse(Call<List<UserItem>> call, Response<List<UserItem>> response) {


                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        if (response.body() != null) {
                            List<UserItem> array = response.body();
                            UserItem userItem = array.get(0);
                            if (!TextUtils.isEmpty(userItem.getDsCustomer())) {
                                username.setText("" + userItem.getDsCustomer());
                            }
                            if (!TextUtils.isEmpty(userItem.getDsPhoneno())) {
                                mobile.setText("" + userItem.getDsPhoneno());
                            }
                            if (!TextUtils.isEmpty(userItem.getDsEmail())) {
                                email.setText(userItem.getDsEmail());
                            }
                            if (!TextUtils.isEmpty(userItem.getDsAddress())) {
                                homeaddress.setText(userItem.getDsAddress());
                            }
                            if (!TextUtils.isEmpty(userItem.getDsDeliveryAddress())) {
                                deladdress.setText(userItem.getDsDeliveryAddress());
                            }
                            if (!TextUtils.isEmpty(userItem.getDsPincode())) {
                               pincode.setText(userItem.getDsPincode());
                            }


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<List<UserItem>> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }

    private void updateCall() {

        final ProgressDialog progressDialog = new ProgressDialog(UpdateUser.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        final SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<ResponseBody> callback = client.updateUser(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""), username.getText().toString(), mobile.getText().toString(), email.getText().toString(), homeaddress.getText().toString(), deladdress.getText().toString(), pincode.getText().toString());
        callback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.v("TAG", response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.getString("status").equalsIgnoreCase("1")) {

                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString(ApiConstants.USERTOKEN, jsonObject.getString("token"));
                            editor.putString(ApiConstants.LOGINSTATE, "true");
                            editor.apply();
                            editor.commit();
                            startActivity(new Intent(UpdateUser.this, HomeActivity.class));

                        } else {

                            Snackbar.make(findViewById(android.R.id.content), "Something went wrong please try again!", Snackbar.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }

}
