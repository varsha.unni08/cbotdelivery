package com.project.centroid.deliveryapp.view.adapter_new;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.data.remote.modelnew.MainSubcategory;
import com.project.centroid.deliveryapp.view.MainHomeActivity;

import java.util.List;

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.SubCategoryHolder> {


    Context context;
    List<MainSubcategory>subCategories;

   // String arr_cat[]=new String[]{"Mens","Womans","IT","Cloths","Vegetables","Meat"};


    public SubCategoryAdapter(Context context, List<MainSubcategory>subCategories) {
        this.context = context;
        this.subCategories=subCategories;
    }

    public class SubCategoryHolder extends RecyclerView.ViewHolder{

        TextView txt;


        public SubCategoryHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((MainHomeActivity)context).showSubCatList(subCategories.get(getAdapterPosition()).getCd_category());
                }
            });
        }
    }

    @NonNull
    @Override
    public SubCategoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_maincategory_txt,viewGroup,false);



        return new SubCategoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubCategoryHolder subCategoryHolder, int i) {

        if(subCategories.get(i).getStatus().equals("1")) {
            subCategoryHolder.txt.setText(subCategories.get(i).getDs_category());
        }

    }

    @Override
    public int getItemCount() {
        return subCategories.size();
    }
}
