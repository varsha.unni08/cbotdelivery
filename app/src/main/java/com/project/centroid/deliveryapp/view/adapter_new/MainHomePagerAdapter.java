package com.project.centroid.deliveryapp.view.adapter_new;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.project.centroid.deliveryapp.data.remote.modelnew.Offer;
import com.project.centroid.deliveryapp.view.fragments.MainPagerFragment;

import java.util.List;

public class MainHomePagerAdapter extends FragmentPagerAdapter {

    List<Offer>offers;

    public MainHomePagerAdapter(FragmentManager fm, List<Offer>offers) {
        super(fm);
        this.offers=offers;
    }

    @Override
    public Fragment getItem(int i) {

        Fragment fr=new MainPagerFragment();
        Bundle bundle=new Bundle();
        bundle.putSerializable("pos",offers.get(i));
        fr.setArguments(bundle);

        return fr;
    }

    @Override
    public int getCount() {

        if(offers.size()>4)
        {
            return 4;
        }
        else {


            return offers.size();
        }



    }
}
