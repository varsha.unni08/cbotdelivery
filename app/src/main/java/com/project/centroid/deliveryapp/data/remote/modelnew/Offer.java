package com.project.centroid.deliveryapp.data.remote.modelnew;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Offer implements Serializable {
    @SerializedName("cd_offerinfo")
    @Expose
    private String cdOfferinfo;
    @SerializedName("ds_offerinfo")
    @Expose
    private String dsOfferinfo;
    @SerializedName("cd_item")
    @Expose
    private String cdItem;
    @SerializedName("ds_offer_type")
    @Expose
    private String dsOfferType;
    @SerializedName("ds_offer_percent")
    @Expose
    private Object dsOfferPercent;
    @SerializedName("ds_offer_desc")
    @Expose
    private String dsOfferDesc;
    @SerializedName("ds_slider_image")
    @Expose
    private String dsSliderImage;
    @SerializedName("ds_image")
    @Expose
    private String dsImage;

    public Offer() {
    }

    public String getCdOfferinfo() {
        return cdOfferinfo;
    }

    public void setCdOfferinfo(String cdOfferinfo) {
        this.cdOfferinfo = cdOfferinfo;
    }

    public String getDsOfferinfo() {
        return dsOfferinfo;
    }

    public void setDsOfferinfo(String dsOfferinfo) {
        this.dsOfferinfo = dsOfferinfo;
    }

    public String getCdItem() {
        return cdItem;
    }

    public void setCdItem(String cdItem) {
        this.cdItem = cdItem;
    }

    public String getDsOfferType() {
        return dsOfferType;
    }

    public void setDsOfferType(String dsOfferType) {
        this.dsOfferType = dsOfferType;
    }

    public Object getDsOfferPercent() {
        return dsOfferPercent;
    }

    public void setDsOfferPercent(Object dsOfferPercent) {
        this.dsOfferPercent = dsOfferPercent;
    }

    public String getDsOfferDesc() {
        return dsOfferDesc;
    }

    public void setDsOfferDesc(String dsOfferDesc) {
        this.dsOfferDesc = dsOfferDesc;
    }

    public String getDsSliderImage() {
        return dsSliderImage;
    }

    public void setDsSliderImage(String dsSliderImage) {
        this.dsSliderImage = dsSliderImage;
    }

    public String getDsImage() {
        return dsImage;
    }

    public void setDsImage(String dsImage) {
        this.dsImage = dsImage;
    }
}
