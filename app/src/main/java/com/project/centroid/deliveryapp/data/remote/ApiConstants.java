package com.project.centroid.deliveryapp.data.remote;

import android.support.annotation.NonNull;
import android.text.TextUtils;

/**
 * @author Sooraj Soman on 11/1/2018
 */
public class ApiConstants {
    public static final String BASEURL = "http://www.centroidsolutions.in/9_villagemart/api/";
    public static final String SHAREDPREF = "MYPREF";
    public static final String USERTOKEN = "TOKEN";

    public static final String imgbaseurl="http://www.centroidsolutions.in/9_villagemart/images/";


    public static final String LOGINSTATE = "";
    public static final String BEARER = "Bearer ";

    public static Boolean isNonEmpty(@NonNull String data) {
        return !TextUtils.isEmpty(data) && !data.equals("");
    }

    public static String amountFormatter(@NonNull String data) {

        return "\u20B9 " + data;
    }

    public static double isNonEmpty(@NonNull String data, String cgst, String sgst) {
        Double total = null;
        try {
            Double data1 = Double.valueOf(data);
            Double data2 = Double.valueOf(cgst);
            Double data3 = Double.valueOf(sgst);
            total = data1 + (data1 / (data2 * 100)) + (data1 / (data3 * 100));
            return total;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return total;
    }

}
