package com.project.centroid.deliveryapp.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.Utils.GridSpacingItemDecoration;
import com.project.centroid.deliveryapp.Utils.LinearLayoutManagerWrapper;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.OrderItem;
import com.project.centroid.deliveryapp.view.adapter.OrderRecyclerAdapterNew;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersActivity extends AppCompatActivity {

    ImageView imgback;
    RecyclerView recyclerView;

    private SharedPreferences sharedpreferences;
    private OrderRecyclerAdapterNew adapter;

    private ArrayList<OrderItem> orderItems1;

    private boolean allContactsLoaded=false;

    int pageCount=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        recyclerView=findViewById(R.id.recyclerView);
        sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        initRecyclerview();
    }




    private void initRecyclerview() {

        orderItems1 = new ArrayList<>();



        LinearLayoutManager linearLayoutManager = new LinearLayoutManagerWrapper(OrdersActivity.this, LinearLayoutManager.VERTICAL, false);



        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter=new OrderRecyclerAdapterNew(OrdersActivity.this,orderItems1,recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(OrdersActivity.this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());



        int spanCount = 2; // 3 columns
        int spacing = 10;

        //recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, false));

        recyclerView.setAdapter(adapter);
        RestService.RestApiInterface client = RestService.getClient();

        Call<List<OrderItem>> callBack = client.getAllOrder(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),1);

        callBack.enqueue(new Callback<List<OrderItem>>() {
            @Override
            public void onResponse(Call<List<OrderItem>> call, Response<List<OrderItem>> response) {

                try {

                    Log.v("Tag", "v");
                    if (response.body() != null) {

                        List<OrderItem> posts = response.body();

                        if (posts.size() > 0) {
                            Log.v("Tag1", "v");

                            orderItems1.addAll(posts);
                            adapter.notifyDataSetChanged();


                            int spanCount = 2; // 3 columns
                            int spacing = 10;
                            recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, false));

                            if (!allContactsLoaded) {
                                try {
                                    loadMore();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                        } else {


                        }

                    } else {


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<OrderItem>> call, Throwable t) {
                Log.v("Tag2", "v");
            }
        });

    }



    private void loadMore() {
        adapter.setOnLoadMoreListener(new OrderRecyclerAdapterNew.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.v("TAG", "loadmore");

                orderItems1.add(null);
                recyclerView.post(new Runnable() {
                    public void run() {
                        adapter.notifyItemInserted(orderItems1.size() - 1);
                    }
                });
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //remove progress item
                     /*   if(newsItems.size()>0){
                        ////newsItems.remove(newsItems.size() - 1);
                        adapter.notifyItemInserted(newsItems.size());}
                     */   //add items one by one
/*                        for (int i = 0; i < 15; i++) {
                            myDataset.add("OrderItem" + (myDataset.size() + 1));
                            mAdapter.notifyItemInserted(myDataset.size());

                        }*/
                        Log.v("TAG", "loading");
                        if (!allContactsLoaded) {

                            pageCount++;

                            try {
                                getMoreData(pageCount);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //adapter.setLoaded();

                        }
                        //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                    }
                }, 2000);
                //System.out.println("load");
            }
        });

    }


    private void getMoreData(final int page) {
        RestService.RestApiInterface client = RestService.getClient();
        Call<List<OrderItem>> callBack =  client.getAllOrder(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),page);

        callBack.enqueue(new Callback<List<OrderItem>>() {
            @Override
            public void onResponse(Call<List<OrderItem>> call, Response<List<OrderItem>> response) {

                try {

                    if (!response.body().isEmpty()) {
                        if (response.body() != null) {


                            List<OrderItem> posts = response.body();
                            if (posts.size() > 0) {
                                Log.v("TAG", "newloading");
                                adapter.setLoaded();
                                orderItems1.remove(orderItems1.size() - 1);
                                orderItems1.addAll(posts);
                                adapter.notifyItemInserted(orderItems1.size());


                            } else {

                                Log.v("TAG", "loadingfalse");
                                adapter.setLoadedFalse();
                                pageCount = 1;
                                allContactsLoaded = true;
                                orderItems1.remove(orderItems1.size() - 1);
                                adapter.notifyDataSetChanged();


                            }
                        } else {
                            Log.v("TAG", "loadingend");

                            allContactsLoaded = true;
                            orderItems1.remove(orderItems1.size() - 1);
                            adapter.setLoadedFalse();
                            adapter.notifyDataSetChanged();


                        }
                    } else {
                        allContactsLoaded = true;
                        orderItems1.remove(orderItems1.size() - 1);
                        adapter.setLoadedFalse();
                        adapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<OrderItem>> call, Throwable t) {

            }
        });


    }

}
