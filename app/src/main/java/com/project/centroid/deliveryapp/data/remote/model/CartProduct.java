package com.project.centroid.deliveryapp.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartProduct {

    @SerializedName("cd_item")
    @Expose
    private String cdItem="0";
    @SerializedName("ds_item_code")
    @Expose
    private String dsItemCode="";
    @SerializedName("ds_item")
    @Expose
    private String dsItem="";
    @SerializedName("cd_category")
    @Expose
    private String cdCategory="0";
    @SerializedName("cd_subcategory")
    @Expose
    private String cdSubcategory="0";
    @SerializedName("cd_uom")
    @Expose
    private String cdUom="";
    @SerializedName("vl_mrp")
    @Expose
    private String vlMrp="0";
    @SerializedName("vl_selling_price")
    @Expose
    private String vlSellingPrice="0";
    @SerializedName("ds_hsncode")
    @Expose
    private String dsHsncode="0";
    @SerializedName("ds_item_description")
    @Expose
    private String dsItemDescription="";
    @SerializedName("ds_item_pic")
    @Expose
    private String dsItemPic="";
    @SerializedName("ds_item_pic_small")
    @Expose
    private String dsItemPicSmall="";
    @SerializedName("vl_cgst")
    @Expose
    private String vlCgst;
    @SerializedName("vl_sgst")
    @Expose
    private String vlSgst;
    @SerializedName("cart_qty")
    @Expose
    private String cartQty="0";
    @SerializedName("item_total")
    @Expose
    private String itemTotal;
    @SerializedName("dt_Cart_date")
    @Expose
    private String dtCartDate;
    @SerializedName("grandtotal_qty")
    @Expose
    private String grandtotal_qty;
    @SerializedName("grandtotal_price")
    @Expose
    private String grandtotal_price;


    public CartProduct() {
    }

    public String getCdItem() {
        return cdItem;
    }

    public void setCdItem(String cdItem) {
        this.cdItem = cdItem;
    }

    public String getDsItemCode() {
        return dsItemCode;
    }

    public void setDsItemCode(String dsItemCode) {
        this.dsItemCode = dsItemCode;
    }

    public String getDsItem() {
        return dsItem;
    }

    public void setDsItem(String dsItem) {
        this.dsItem = dsItem;
    }

    public String getCdCategory() {
        return cdCategory;
    }

    public void setCdCategory(String cdCategory) {
        this.cdCategory = cdCategory;
    }

    public String getCdSubcategory() {
        return cdSubcategory;
    }

    public void setCdSubcategory(String cdSubcategory) {
        this.cdSubcategory = cdSubcategory;
    }

    public String getCdUom() {
        return cdUom;
    }

    public void setCdUom(String cdUom) {
        this.cdUom = cdUom;
    }

    public String getVlMrp() {
        return vlMrp;
    }

    public void setVlMrp(String vlMrp) {
        this.vlMrp = vlMrp;
    }

    public String getVlSellingPrice() {
        return vlSellingPrice;
    }

    public void setVlSellingPrice(String vlSellingPrice) {
        this.vlSellingPrice = vlSellingPrice;
    }

    public String getDsHsncode() {
        return dsHsncode;
    }

    public void setDsHsncode(String dsHsncode) {
        this.dsHsncode = dsHsncode;
    }

    public String getDsItemDescription() {
        return dsItemDescription;
    }

    public void setDsItemDescription(String dsItemDescription) {
        this.dsItemDescription = dsItemDescription;
    }

    public String getDsItemPic() {
        return dsItemPic;
    }

    public void setDsItemPic(String dsItemPic) {
        this.dsItemPic = dsItemPic;
    }

    public String getDsItemPicSmall() {
        return dsItemPicSmall;
    }

    public void setDsItemPicSmall(String dsItemPicSmall) {
        this.dsItemPicSmall = dsItemPicSmall;
    }

    public String getVlCgst() {
        return vlCgst;
    }

    public void setVlCgst(String vlCgst) {
        this.vlCgst = vlCgst;
    }

    public String getVlSgst() {
        return vlSgst;
    }

    public void setVlSgst(String vlSgst) {
        this.vlSgst = vlSgst;
    }

    public String getCartQty() {
        return cartQty;
    }

    public void setCartQty(String cartQty) {
        this.cartQty = cartQty;
    }

    public String getItemTotal() {
        return itemTotal;
    }

    public void setItemTotal(String itemTotal) {
        this.itemTotal = itemTotal;
    }

    public String getDtCartDate() {
        return dtCartDate;
    }

    public void setDtCartDate(String dtCartDate) {
        this.dtCartDate = dtCartDate;
    }

    public String getGrandtotal_qty() {
        return grandtotal_qty;
    }

    public void setGrandtotal_qty(String grandtotal_qty) {
        this.grandtotal_qty = grandtotal_qty;
    }

    public String getGrandtotal_price() {
        return grandtotal_price;
    }

    public void setGrandtotal_price(String grandtotal_price) {
        this.grandtotal_price = grandtotal_price;
    }
}
