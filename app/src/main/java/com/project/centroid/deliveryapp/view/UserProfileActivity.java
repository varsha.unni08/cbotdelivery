package com.project.centroid.deliveryapp.view;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonArray;
import com.project.centroid.deliveryapp.ProfilefullscreenActivity;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.UpdateUser;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.RestService;
import com.project.centroid.deliveryapp.data.remote.model.UserItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileActivity extends AppCompatActivity {

    @BindView(R.id.profile)
    ImageView profile;
    @BindView(R.id.username)
    TextView username;
    @BindView(R.id.mobile)
    TextView mobile;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.dob)
    TextView dob;
    Unbinder unbinder;
    @BindView(R.id.edit)
    LinearLayout edit;
    @BindView(R.id.logout)
    LinearLayout logout;

    @BindView(R.id.imgedit)
    ImageView imgedit;

    @BindView(R.id.imgback)
            ImageView imgback;

    String url="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        getSupportActionBar().hide();

        ButterKnife.bind(UserProfileActivity.this);
        setViews();


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent=new Intent(UserProfileActivity.this, ProfilefullscreenActivity.class);
                intent.putExtra("Profile",url);
                startActivity(intent);
            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });



        imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(ContextCompat.checkSelfPermission(UserProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                {

                    ActivityCompat.requestPermissions(UserProfileActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},110);
                }
                else {

                    Intent i = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, 2);
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2 && resultCode == RESULT_OK
                && null != data) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            File file=new File(picturePath);
            uploadImageFile(file);




        }
    }

    @OnClick({R.id.logout,R.id.edit})
    public void onViewClicked(View v) {
        SharedPreferences  sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);
        switch (v.getId()){

            case R.id.logout:SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(ApiConstants.LOGINSTATE, "");
                editor.apply();
                editor.commit();
                startActivity(new Intent(UserProfileActivity.this, LoginActivity.class));
                finish();

                break;
            case R.id.edit:
                startActivity(new Intent(UserProfileActivity.this, UpdateUser.class));
                break;
        }



    }


    public void uploadImageFile(File file)
    {

        // Create a request body with file and image media type
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
        // Create MultipartBody.Part using file request-body,file name and part name
        MultipartBody.Part part = MultipartBody.Part.createFormData("ds_image", file.getName(), fileReqBody);
        //Create request body with text description and text media type
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), "image-type");



        final ProgressDialog progressDialog = new ProgressDialog(UserProfileActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<JsonArray> jsonArrayCall=client.uploadImage(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""),part);


        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                progressDialog.dismiss();

                if(response.isSuccessful())
                {

                    try{

                        JSONArray jsonArray=new JSONArray(response.body().toString());

                        if(jsonArray.length()>0)
                        {

                            JSONObject jsonObject=jsonArray.getJSONObject(0);

                            if(jsonObject.getInt("status")==1)
                            {

                                Toast.makeText(UserProfileActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();

                                getImage();

                            }
                            else {

                                Toast.makeText(UserProfileActivity.this,jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                            }


                        }


                    }catch (Exception e)
                    {

                    }




                }
                else {

                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {

                progressDialog.dismiss();
                t.printStackTrace();
            }
        });



    }



    public void getImage()
    {
        final SharedPreferences sharedpreferences = getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();

        Call<JsonArray>jsonArrayCall=client.getImage(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""));


        jsonArrayCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                //   progressDialog.dismiss();

                if(response.isSuccessful())
                {

                    try{

                        JSONArray jsonArray=new JSONArray(response.body().toString());

                        if(jsonArray.length()>0)
                        {

                            JSONObject jsonObject=jsonArray.getJSONObject(0);

                            if(jsonObject.getInt("status")==1)
                            {

                                // Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();


                                url=jsonObject.getString("message");



                                GlideUrl glideUrl=  new GlideUrl(url, new LazyHeaders.Builder()
                                        .addHeader("Authorization", ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""))
                                        .build());



                                //  Glide.with(getActivity()).load(jsonObject.getString("message")).apply(RequestOptions.circleCropTransform()).into(profile);

                                Glide.with(UserProfileActivity.this).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(glideUrl).apply(RequestOptions.circleCropTransform()).into(profile);

                            }
                            else {

                                //  Toast.makeText(getActivity(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
                            }


                        }


                    }catch (Exception e)
                    {

                    }




                }
                else {

                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {

                // progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }





    private void setViews() {
        final Context context = UserProfileActivity.this;





        final ProgressDialog progressDialog = new ProgressDialog(context,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        SharedPreferences sharedpreferences = context.getSharedPreferences(ApiConstants.SHAREDPREF, Context.MODE_PRIVATE);


        // TODO: Implement your own authentication logic here.
        RestService.RestApiInterface client = RestService.getClient();


        Call<List<UserItem>> callback = client.getUser(ApiConstants.BEARER + sharedpreferences.getString(ApiConstants.USERTOKEN, ""));
        callback.enqueue(new Callback<List<UserItem>>() {
            @Override
            public void onResponse(Call<List<UserItem>> call, Response<List<UserItem>> response) {


                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    progressDialog.hide();
                    try {
                        if (response.body() != null) {
                            List<UserItem> array = response.body();
                            UserItem userItem = array.get(0);
                            if (!TextUtils.isEmpty(userItem.getDsCustomer())) {
                                username.setText( userItem.getDsCustomer());
                            }
                            if (!TextUtils.isEmpty(userItem.getDsAddress())) {
                                mobile.setText(userItem.getDsPhoneno());
                            }
                            if (!TextUtils.isEmpty(userItem.getDsEmail())) {
                                email.setText(  userItem.getDsEmail());
                            }
                            if (!TextUtils.isEmpty(userItem.getDsDeliveryAddress())) {
                                dob.setText(userItem.getDsDeliveryAddress() + "\n" + userItem.getDsPincode());
                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }


            }

            @Override
            public void onFailure(Call<List<UserItem>> call, Throwable t) {

                t.printStackTrace();
            }
        });
    }
}
