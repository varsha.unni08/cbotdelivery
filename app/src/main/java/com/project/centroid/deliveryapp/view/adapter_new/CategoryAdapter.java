package com.project.centroid.deliveryapp.view.adapter_new;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.data.remote.model.SubCategory;
import com.project.centroid.deliveryapp.view.MainHomeActivity;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryHolder> {

    Context context;
    List<SubCategory>subCategories;

    public CategoryAdapter(Context context, List<SubCategory>subCategories) {
        this.context = context;
        this.subCategories=subCategories;
    }

   // String arr_cat[]=new String[]{"Shirts","Cottons","TV","Computer","Vegetables","Meat"};


    public class CategoryHolder extends RecyclerView.ViewHolder{
        TextView txt;


        public CategoryHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ((MainHomeActivity)context).showData(subCategories.get(getAdapterPosition()));
                }
            });
        }
    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_maincategory_txt,viewGroup,false);



        return new CategoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryHolder categoryHolder, int i) {
        categoryHolder.txt.setText(subCategories.get(i).getDsSubcategory());
    }

    @Override
    public int getItemCount() {
        return subCategories.size();
    }
}
