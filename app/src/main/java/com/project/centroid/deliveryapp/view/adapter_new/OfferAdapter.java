package com.project.centroid.deliveryapp.view.adapter_new;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.project.centroid.deliveryapp.R;
import com.project.centroid.deliveryapp.data.remote.ApiConstants;
import com.project.centroid.deliveryapp.data.remote.modelnew.Offer;

import java.util.List;

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.OfferHolder> {

    Context context;
    List<Offer>offers;

    public OfferAdapter(Context context, List<Offer> offers) {
        this.context = context;
        this.offers = offers;
    }

    public class OfferHolder extends RecyclerView.ViewHolder{

        ImageView img;

        CardView cardV;

        TextView itemdetails,itemdetailsmrpprice,itemdetailsprice,itemofferdetails;

        public OfferHolder(@NonNull View itemView) {
            super(itemView);
            img=itemView.findViewById(R.id.img);
            itemdetails=itemView.findViewById(R.id.itemdetails);
            itemdetailsmrpprice=itemView.findViewById(R.id.itemdetailsmrpprice);
            itemdetailsprice=itemView.findViewById(R.id.itemdetailsprice);
            itemofferdetails=itemView.findViewById(R.id.itemofferdetails);
            cardV=itemView.findViewById(R.id.cardV);
        }
    }

    @NonNull
    @Override
    public OfferHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_offers,viewGroup,false);


        return new OfferHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OfferHolder offerHolder, int i) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((AppCompatActivity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        double w=width/1.7;

        offerHolder.cardV.setLayoutParams(new LinearLayout.LayoutParams(width, (int) w));
        Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder)).load(ApiConstants.imgbaseurl+offers.get(i).getDsSliderImage()).into(offerHolder.img);


        offerHolder.itemdetails.setText(offers.get(i).getDsOfferDesc());
      //  offerHolder.itemdetailsmrpprice.setText(offers.get(i).);
    }

    @Override
    public int getItemCount() {
        return offers.size();
    }
}
